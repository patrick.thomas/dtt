/* Version $Id$ */
/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef ROOT_TLGLBTree
#define ROOT_TLGLBTree

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGLBTreeContainer and TLGLBTreeEntry                                //
//                                                                      //
// A list tree is a widget that can contain a number of items           //
// arranged in a tree structure. The items are represented by small     //
// folder icons that can be either open or closed.                      //
//                                                                      //
// The TLGLBTreeContainer is user callable. The TLGLBTreeEntry is a srvc//
// class of the list tree.                                              //
//                                                                      //
// A list tree can generate the following events:                       //
// kC_LISTTREE, kCT_ITEMCLICK, which button, location (y<<16|x).        //
// kC_LISTTREE, kCT_ITEMDBLCLICK, which button, location (y<<16|x).     //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include <TGFrame.h>
#include <TGCanvas.h>
#include <TGWidget.h>
#include <TGClient.h>
#include <iostream> // FOR Debugging JCB

   class TGPicture;
   class TGViewPort;


namespace dttgui {

/** @name TLGLBTree
    This header exports a listbox which organizes its entries in a 
    tree.
   
    Selecting an item in the list tree box will generate the event:
    \begin{verbatim}
    kC_LISTTREE, kCT_ITEMCLICK, list tree box ID, user data   and
    kC_LISTTREE, kCT_DBLITEMCLICK, list tree box ID, user data
    \end{verbatim}

    @memo Tree listbox
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Tree listbox entry. For internal use only.
    @memo Tree listbox entry.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGLBTreeEntry {
   
      friend class TLGLBTreeContainer;
   
   private:
      TGClient        *fClient;       // pointer to TGClient
      TLGLBTreeEntry   *fParent;       // pointer to parent
      TLGLBTreeEntry   *fFirstchild;   // pointer to first child item
      TLGLBTreeEntry   *fLastchild;   // pointer to last child item
      TLGLBTreeEntry   *fPrevsibling;  // pointer to previous sibling
      TLGLBTreeEntry   *fNextsibling;  // pointer to next sibling
      Bool_t           fOpen;         // true if item is open
      Bool_t           fActive;       // true if item is active
      char            *fText;         // item text
      Int_t            fLength;       // length of item text
      char            *fFullname;     // full name of item
      Int_t            fFullLength;   // length of full name
      Int_t            fY;            // y position of item
      Int_t            fXtext;        // x position of item text
      Int_t            fYtext;        // y position of item text
      UInt_t           fHeight;       // item height
      UInt_t           fPicWidth;     // width of item icon
      const TGPicture *fOpenPic;      // icon for open state
      const TGPicture *fClosedPic;    // icon for closed state
      void            *fUserData;     // pointer to user data structure
   
   public:
      TLGLBTreeEntry(TGClient *fClient, const char *name,
                    const char* fullname,
                    const TGPicture *opened, const TGPicture *closed);
      virtual ~TLGLBTreeEntry();
   
      void AddChild(TLGLBTreeEntry* ent);
      void Rename(const char *new_name);
   
      TLGLBTreeEntry *GetParent() const { 
         return fParent; }
      TLGLBTreeEntry *GetFirstChild() const { 
         return fFirstchild; }
      TLGLBTreeEntry *GetPrevSibling() const { 
         return fPrevsibling; }
      TLGLBTreeEntry *GetNextSibling() const { 
         return fNextsibling; }
      Bool_t IsActive() const { 
         return fActive; }
      Bool_t IsOpen() const { 
         return fOpen; }
      const char* GetText() const { 
         return fText; }
      const char* GetFullname() const { 
         return fFullname; }
      void SetUserData(void *userData) { 
	 //std::cerr << "     TLGLBTreeEntry::SetUserData(" << (long) userData << ")" << std::endl ;
         fUserData = userData; }
      void* GetUserData() const { 
	 //std::cerr << "TLGLBTreeEntry::GetUserData() - return " << (long) fUserData << std::endl ;
         return fUserData; }
      void SetPictures(const TGPicture *opened, const TGPicture *closed);
   
      //ClassDef(TLGLBTreeEntry,0)  //Item that goes into a TLGLBTreeContainer container
   };


/** Tree listbox container. For internal use only.
    @memo Tree listbox container.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGLBTreeContainer : public TGFrame {
   
      //friend class TGClient;
      friend class TLGLBTree;
   
   protected:
      TLGLBTreeEntry   *fFirst;          // pointer to first item in list
      TLGLBTreeEntry   *fSelected;       // pointer to selected item in list
      Int_t            fHspacing;       // horizontal spacing between items
      Int_t            fVspacing;       // vertical spacing between items
      Int_t            fIndent;         // number of pixels indentation
      Int_t            fMargin;         // number of pixels margin from left side
      ULong_t          fGrayPixel;      // gray draw color
      GContext_t       fDrawGC;         // icon drawing context
      GContext_t       fLineGC;         // dashed line drawing context
      GContext_t       fHighlightGC;    // highlighted icon drawing context
      FontStruct_t     fFont;           // font used to draw item text
      UInt_t           fDefw;           // default list width
      UInt_t           fDefh;           // default list height
      Int_t            fExposeTop;      // top y postion of visible region
      Int_t            fExposeBottom;   // bottom y position of visible region
      const TGWindow  *fMsgWindow;      // pointer to window handling list messages
      const TGPicture *fDefOpenPic;      // icon for open state
      const TGPicture *fDefClosedPic;    // icon for closed state
   
      static FontStruct_t   fgDefaultFontStruct;
   
      virtual void DoRedraw();
   
      void  Draw(Int_t yevent, Int_t hevent);
      void  Draw(Option_t * ="") { 
         MayNotUse("Draw(Option_t*)"); }
      Int_t DrawChildren(TLGLBTreeEntry *item, Int_t x, Int_t y, Int_t xroot);
      void  DrawItem(TLGLBTreeEntry *item, Int_t x, Int_t y, Int_t *xroot,
                    UInt_t *retwidth, UInt_t *retheight);
      void  DrawItemName(TLGLBTreeEntry *item);
      void  DrawNode(TLGLBTreeEntry *item, Int_t x, Int_t y);
   
      void  HighlightItem(TLGLBTreeEntry *item, Bool_t state, Bool_t draw);
      void  HighlightChildren(TLGLBTreeEntry *item, Bool_t state, Bool_t draw);
      void  UnselectAll(Bool_t draw);
   
      void  RemoveReference(TLGLBTreeEntry *item);
      void  PDeleteChildren(TLGLBTreeEntry *item);
      void  InsertChild(TLGLBTreeEntry *parent, TLGLBTreeEntry *item);
      void  InsertChildren(TLGLBTreeEntry *parent, TLGLBTreeEntry *item);
      Int_t SearchChildren(TLGLBTreeEntry *item, Int_t y, Int_t findy,
                        TLGLBTreeEntry **finditem);
      TLGLBTreeEntry *FindItem(Int_t findy);
      UInt_t GetChildrenSize (TLGLBTreeEntry* item, UInt_t x, UInt_t y) const;
      void GetItemSize (TLGLBTreeEntry* item, UInt_t* retwidth, 
                       UInt_t* retheight) const;
   
   public:
#if ROOT_VERSION_CODE > 197893
      TLGLBTreeContainer(TGWindow *p, UInt_t w, UInt_t h,
                        UInt_t options, ULong_t back = GetWhitePixel());
#else
      TLGLBTreeContainer(TGWindow *p, UInt_t w, UInt_t h,
                        UInt_t options, ULong_t back = fgWhitePixel);
#endif
      virtual ~TLGLBTreeContainer();
   
      virtual Bool_t HandleButton(Event_t *event);
      virtual Bool_t HandleDoubleClick(Event_t *event);
      virtual Bool_t HandleExpose(Event_t *event);
   
      virtual void Associate(const TGWindow *w) { 
         fMsgWindow = w; }
   
      virtual void CaculateDefaultSize() const;
      virtual TGDimension GetDefaultSize() const;      
      virtual UInt_t GetDefaultHeight() const;
      virtual UInt_t GetDefaultWidth() const;
   
      TLGLBTreeEntry *AddItem(TLGLBTreeEntry *parent, const char *string,
                        const TGPicture *open = 0,
                        const TGPicture *closed = 0);
      TLGLBTreeEntry *AddItem(TLGLBTreeEntry *parent, const char *string,
                        void *userData, const TGPicture *open = 0,
                        const TGPicture *closed = 0);
      TLGLBTreeEntry *AddItem(TLGLBTreeEntry *parent, const char *string,
                        const char* fullname, const TGPicture *open = 0,
                        const TGPicture *closed = 0);
      TLGLBTreeEntry *AddItem(TLGLBTreeEntry *parent, const char *string,
                        const char* fullname, void *userData, 
                        const TGPicture *open = 0, 
                        const TGPicture *closed = 0);
      void  RenameItem(TLGLBTreeEntry *item, const char *string);
      Int_t DeleteItem(TLGLBTreeEntry *item);
      Int_t RecursiveDeleteItem(TLGLBTreeEntry *item, void *userData);
      Int_t DeleteChildren(TLGLBTreeEntry *item);
      Int_t Reparent(TLGLBTreeEntry *item, TLGLBTreeEntry *newparent);
      Int_t ReparentChildren(TLGLBTreeEntry *item, TLGLBTreeEntry *newparent);
   
      Int_t Sort(TLGLBTreeEntry *item);
      Int_t SortSiblings(TLGLBTreeEntry *item);
      Int_t SortChildren(TLGLBTreeEntry *item);
      void  HighlightItem(TLGLBTreeEntry *item);
      void  ClearHighlighted();
   
      TLGLBTreeEntry *GetFirstItem() const { 
         return fFirst; }
      TLGLBTreeEntry *GetSelected() const { 
	 //std::cerr << "TLGLBTreeContainer::GetSelected()" << std::endl ;
         return fSelected; }

      void *GetSelectedUserData() {
	 if (fSelected)
	    return fSelected->GetUserData(); 
	 else {
	    //std::cerr << "TLGLBTreeContainer::GetSelectedUserData() - fSelected is NULL" << std::endl ;
	    return 0 ;
	 }
      }

      void SetSelectedUserData(void *userdata) {
	 //std::cerr << "    TLGLBTreeContainer::SetSelectedUserData(" << (long) userdata << ")" << std::endl ;
	 if (fSelected)
	    fSelected->SetUserData(userdata) ; // Calls TLGLBTreeEntry::SetUserData()
      }

      TLGLBTreeEntry *FindByData (TLGLBTreeEntry* item, void *userData);
      TLGLBTreeEntry *FindByFullname (TLGLBTreeEntry* item, const char* fullname, const int rate = -1);
      TLGLBTreeEntry *FindSiblingByName(TLGLBTreeEntry *item, const char *name);
      TLGLBTreeEntry *FindSiblingByData(TLGLBTreeEntry *item, void *userData);
      TLGLBTreeEntry *FindChildByName(TLGLBTreeEntry *item, const char *name);
      TLGLBTreeEntry *FindChildByData(TLGLBTreeEntry *item, void *userData);
   
      //ClassDef(TLGLBTreeContainer,0)  //Show items in a tree structured list
   };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGLBTree                                                            //
//                                                                      //
// A TLGLBTree widget.                                                  //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

/** Tree listbox. This listbox variant presents a tree selection
    choice.
    @memo Tree listbox.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGLBTree : public TGCompositeFrame {
   
   protected:
      /// listbox widget id
      Int_t            fListBoxId;       
      /// maximum height of single entry
      UInt_t           fItemVsize;       
      /// true if height should be multiple of fItemVsize
      Bool_t           fIntegralHeight;  
      /// listbox container
      TLGLBTreeContainer *fLbc;          
      /// listbox viewport (see TGCanvas.h) 
      TGViewPort      *fVport;           
      /// vertical scrollbar
      TGVScrollBar    *fVScrollbar;      
      /// last position of scrollbar
      int             fSBLastpos;        
      /// window handling listbox messages
      const TGWindow  *fMsgWindow;       
   
      /// Set the container
      void SetContainer(TGFrame *f) { 
         fVport->SetContainer(f); }
   
      /// Initialize the list box
      virtual void InitListBox();
   
   public:
      /// Create the list box
#if ROOT_VERSION_CODE > 197893
      TLGLBTree(const TGWindow *p, Int_t id,
               UInt_t options = kSunkenFrame | kDoubleBorder,
               ULong_t back = GetWhitePixel());
#else
      TLGLBTree(const TGWindow *p, Int_t id,
               UInt_t options = kSunkenFrame | kDoubleBorder,
               ULong_t back = fgWhitePixel);
#endif
      /// Destroy the list box
      virtual ~TLGLBTree();
   
      /// Get the container
      TGFrame *GetContainer() const { 
         return fLbc; }
      /// Get the viewport
      TGFrame *GetViewPort() const { 
         return fVport; }
      /// Draw border
      virtual void DrawBorder();
      /// Resize
      virtual void Resize(UInt_t w, UInt_t h);
      /// Resize
      virtual void Resize(TGDimension size) { 
         Resize(size.fWidth, size.fHeight); }
      /// Move and resize
      virtual void MoveResize(Int_t x, Int_t y, UInt_t w, UInt_t h);
      /// Layout
      virtual void Layout();
      /// Integral height?
      virtual void IntegralHeight(Bool_t mode) { 
         fIntegralHeight = mode; }
      /// Get default size
      virtual TGDimension GetDefaultSize() const;
   
      /// Process messages
      virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2);
      /// Associate
      virtual void Associate(const TGWindow *w) { 
         fMsgWindow = w; }
   
      /// Add item
      TLGLBTreeEntry *AddItem(TLGLBTreeEntry *parent, const char *string,
                        const TGPicture *open = 0,
                        const TGPicture *closed = 0) {
         return fLbc->AddItem (parent, string, open, closed); }
      /// Add item
      TLGLBTreeEntry *AddItem(TLGLBTreeEntry *parent, const char *string,
                        void *userData, const TGPicture *open = 0,
                        const TGPicture *closed = 0) {
         return fLbc->AddItem (parent, string, userData, open, closed); }
      /// Add item
      TLGLBTreeEntry *AddItem(TLGLBTreeEntry *parent, const char *string,
                        const char* fullname, const TGPicture *open = 0,
                        const TGPicture *closed = 0) {
         return fLbc->AddItem (parent, string, fullname, open, closed); }
      /// Add item
      TLGLBTreeEntry *AddItem(TLGLBTreeEntry *parent, const char *string,
                        const char* fullname, void *userData, 
                        const TGPicture *open = 0, 
                        const TGPicture *closed = 0) {
         return fLbc->AddItem (parent, string, fullname, 
                              userData, open, closed); }
      /// Rename item
      void  RenameItem(TLGLBTreeEntry *item, const char *string) {
         fLbc->RenameItem (item, string); }
      ///  Delete item
      Int_t DeleteItem(TLGLBTreeEntry *item) {
         return fLbc->DeleteItem (item); }
      /// Delete items recursivly
      Int_t RecursiveDeleteItem(TLGLBTreeEntry *item, void *userData) {
         return fLbc->RecursiveDeleteItem(item, userData); }
      /// Delete all children
      Int_t DeleteChildren(TLGLBTreeEntry *item) {
         return fLbc->DeleteChildren (item); }
      /// Reparanet
      Int_t Reparent(TLGLBTreeEntry *item, TLGLBTreeEntry *newparent) {
         return fLbc->Reparent (item, newparent); }
      /// Reparent children
      Int_t ReparentChildren(TLGLBTreeEntry *item, TLGLBTreeEntry *newparent) {
         return fLbc->ReparentChildren (item, newparent); }
   
      /// Sort items
      Int_t Sort(TLGLBTreeEntry *item) {
         return fLbc->Sort (item); }
      /// Sort siblings
      Int_t SortSiblings(TLGLBTreeEntry *item) {
         return fLbc->SortSiblings (item); }
      /// Sort children
      Int_t SortChildren(TLGLBTreeEntry *item) {
         return fLbc->SortChildren (item); }
   
      /// Get first item
      TLGLBTreeEntry *GetFirstItem() const { 
         return fLbc->GetFirstItem(); }
      /// Select item
      virtual TLGLBTreeEntry *Select (void *userData, Bool_t sel = kTRUE);
      /// Select item
      virtual TLGLBTreeEntry *SelectByName (const char* fullname, Bool_t sel = kTRUE, const int rate = -1);
      /// Get selected item
      virtual void* GetSelected() const; 
      /// Get selected item
      virtual const char* GetSelectedName() const;
      /// Is selected?
      virtual Bool_t GetSelection (void *userData) const;
      /// Is selected
      virtual Bool_t GetSelectionByName (const char* fullname) const;
      /// Get selected entry
      virtual TLGLBTreeEntry *GetSelectedEntry() const { 
	 //std::cerr << "TLGLBTree::GetSelectedEntry()" << std::endl ;
         return fLbc->GetSelected(); }

      virtual void *GetSelectedUserData() {
	 if (fLbc)
	    return fLbc->GetSelectedUserData(); 
	 return (void *) NULL ;
      }
      virtual void SetSelectedUserData(void *userdata) {
	 //std::cerr << "  TLGLBTree::SetSelectedUserData(" << (long) userdata << std::endl ;
	 if (fLbc)
	    fLbc->SetSelectedUserData(userdata) ; // Calls TLGLBTreeContainer::SetSelectedUserData()
      }
   
   // ClassDef(TLGLBTree,0)  // Listbox tree widget
   };


//@}
}

#endif
