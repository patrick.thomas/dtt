/* Version $Id$ */
#ifndef ROOT_TLGChannelBox
#define ROOT_TLGChannelBox
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Module Name:  TLGChannelBox						     */
/*                                                                           */
/* Module Description: ROOT GUI widget for handling LIGO channel names       */
/*                                                                           */
/*---------------------------------------------------------------------------*/

#include <TGClient.h>
#include "TLGLBTree.hh"
#include "TLGComboTree.hh"
#include <string>

#define TEST_LOCALRATE 1

namespace dttgui {


/** @name TLGChannelBox
    This header exports a tree listbox and a tree combobox for 
    selecting LIGO channel names. Channel names are organized in a 
    tree with site/ifo number and the system name as branch nodes.
    Branch nodes can be opened and closed like folders to expand or
    collapse the channel names asscciated with this node.

    The channel selection boxes will not obtain the channel names
    by themselves, but rather the user has to provide a list of them
    when creating the box.
   
    @memo Channel name selection combobox
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// ChannelTree                                                          //
//                                                                      //
// A list of channel names                                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


   // This enum is assigned to an int type.
   /// channel tree type
   enum channetreetype {
   /// Standard
   kChannelTreeNormal = 0,
   /// Show rate
   kChannelTreeShowRate = 1,
   /// Show source
   kChannelTreeShowSource = 2,
   /// Separate slow channels
   kChannelTreeSeparateSlow = 4,
   /// Make 3 levels
   kChannelTreeLevel3 = 8
   // Add more entries for additional levels of channel hierarchy - JCB 
   ,kChannelTreeLevel6 = 16 
   // End additional channel hierarchy - JCB
   };

#define SEPARATE_SLOW_LIMIT 100
#define TREE_LEVEL_3_LIMIT 1000
#define TREE_LEVEL_6_LIMIT 5000

/** A helper class which is used by ChannelTree which represents a
    channel name with associate parameters.
   
    @memo Channel entry.
    @author Written November 1999 by Daniel Sigg
 ************************************************************************/
   class ChannelEntry {
   protected:
      /// name
      TString	fName;
      /// rate
      Float_t	fRate;
      /// source
      TString	fUDN;
   
   public:
      /// Constructor
      ChannelEntry() : fRate (0) {
      }
      /// get name
      const char* Name() const {
         return (const char*)fName; }
      /// set name
      void SetName (const char* name) {
         fName = name ? name : ""; }
      /// get rate
      float Rate() const {
         return fRate; }
      /// set rate
      void SetRate (float r) {
         fRate = r; }
      /// get UDN
      const char* UDN() const {
         return (const char*) fUDN; }
      /// Set UDN
      void SetUDN (const char* udn) {
         fUDN = udn ? udn : ""; }
   };


/** A helper class which is uset by TLGChannelListbox and 
    TLGChannelCombobox to manage channel names and organize them in a
    tree.
   
    @memo Channel name tree.
    @author Written November 1999 by Daniel Sigg
 ************************************************************************/
   class ChannelTree {
   
   public:
      /// Create a channel name tree (list of channel names)
      ChannelTree (const ChannelEntry* chns, UInt_t chnnum, Bool_t copy=kTRUE,
                  Int_t type = kChannelTreeNormal);
      /// Create a channel name tree (list of channel names)
      ChannelTree (const char* const* chnnames, UInt_t chnnum,
                  Int_t type = kChannelTreeNormal);
      /// Create a channel name tree (space separated list of names)
      explicit ChannelTree (const char* chnnames,
                        Int_t type = kChannelTreeNormal);
      /// Destroys the channel name tree
      virtual ~ChannelTree();

      /// Sets a new channel name tree (list of channel names)
      Bool_t ReSize(UInt_t chnnum);

      /// Sets a new channel name tree (list of channel names)
      virtual Bool_t SetChannels(const ChannelEntry* chns, UInt_t chnnum,
				 Bool_t copy=kTRUE);
      /// Sets a new channel name tree (list of channel names)
      virtual Bool_t SetChannels (const char* const* chnnames, 
				  UInt_t chnnum);
      /// Sets a new channel name tree (space separated list of names)
      virtual Bool_t SetChannels (const char* chnnames);

      /// Sets a new channel name tree (list of channel names)
      static Bool_t MakeChannelList(const char* chnnames, ChannelEntry*& chns, 
				    UInt_t& chnnum, 
				    Int_t tree_type = kChannelTreeNormal);

      /// Sets a new channel name tree (list of channel names)
      static Bool_t SortChannelList(ChannelEntry chns[], UInt_t chnnum, 
				    Int_t tree_type = kChannelTreeNormal);
      /// Parses a channel name
      static Bool_t GetIfoSub (const char* chn, char* ifo, char* sub, 
                        char* rest);
      /// Parses a channel name
      static Bool_t GetIfoSubLoc (const char* chn, char* ifo, char* sub, 
                        char* loc, char* rest);
#if 1
      /// Parse the remainder of the string for another section
      // For additional hierarchy in names - JCB
      static Int_t GetLocFromRest(char *rest, char *loc) ;
#endif

      /// Builds the channel tree for the GUI wdigets
      virtual Bool_t BuildChannelTree ();
   
      /// Get the listbox ID number associated with a channel name
      virtual Int_t GetChannelId (const char* channelname) const;
      /// Get the channel name associated with list box ID
      virtual const char* GetChannelName (Int_t id) const;
      /// Get the channel tree type
      virtual Int_t GetChannelTreeType () const {
         return fChannelTreeType; }
      /// Set the channel tree type
      virtual void SetChannelTreeType (Int_t ctype) {
         fChannelTreeType = ctype; }
   
   protected:
     /// Set the pointer to the TGClient
     void SetChannelClient(TGClient* tc);
   
   protected:
      /// connection to display server
      TGClient* 	fChnClient;
      /// la picture
      const TGPicture* fLeftArrow;
      /// List of channel names
      ChannelEntry*	fChannels;
      /// Number of channels names in list
      UInt_t    	fChnNum;
      ///  This class owns the channel list.
      Bool_t            fOwned;
      /// Channel tree type
      Int_t		fChannelTreeType;
   
      /** AddChannel must be overwritten by descendents
          Used to add/delete channel names to/from a list tree */
      virtual TLGLBTreeEntry* AddChannel (TLGLBTreeEntry *parent, 
                        const char *string, const char* fullname = 0,
                        long userData = -1, const TGPicture *open = 0,
                        const TGPicture *closed = 0) {
         return 0; }
      /** DeleteChannels must be overwritten by descendents
          Used to add/delete channel names to/from a list tree */
      virtual void DeleteChannels() {
      }
   
   };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGChannelListbox                                                    //
//                                                                      //
// A tree listbox to select LIGO channel names                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

/** A list box widget which lets the user select channel names by 
    presenting a tree like selection list.
   
    @memo Channel tree listbox.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGChannelListbox : public TLGLBTree, public ChannelTree {
   public:
      /** Create a channel tree listbox.
          @memo Constructor.
          @param p Parent window
   	  @param id Widget id number
   	  @param chnnames List of channel names
          @param chnnum Number of channel names in list
     	  @param option listbox options
   	  @param back listbox background color
       ******************************************************************/
#if ROOT_VERSION_CODE > 197893
      TLGChannelListbox (TGWindow* p, Int_t id,
                        ChannelEntry* chnnames, UInt_t chnnum,
                        Int_t type = kChannelTreeNormal,
                        UInt_t option = kSunkenFrame | kDoubleBorder, 
                        ULong_t back = GetWhitePixel());
#else
      TLGChannelListbox (TGWindow* p, Int_t id,
                        ChannelEntry* chnnames, UInt_t chnnum,
                        Int_t type = kChannelTreeNormal,
                        UInt_t option = kSunkenFrame | kDoubleBorder, 
                        ULong_t back = fgWhitePixel);
#endif
      /** Create a channel tree listbox.
          @memo Constructor.
          @param p Parent window
   	  @param id Widget id number
   	  @param chnnames Space separated list of channel names
     	  @param option listbox options
   	  @param back listbox background color
       ******************************************************************/
#if ROOT_VERSION_CODE > 197893
      TLGChannelListbox (TGWindow* p, Int_t id, const char* chnnames = 0, 
                        Int_t type = kChannelTreeNormal,
                        UInt_t option = kSunkenFrame | kDoubleBorder, 
                        ULong_t back = GetWhitePixel());
#else
      TLGChannelListbox (TGWindow* p, Int_t id, const char* chnnames = 0, 
                        Int_t type = kChannelTreeNormal,
                        UInt_t option = kSunkenFrame | kDoubleBorder, 
                        ULong_t back = fgWhitePixel);
#endif
      /** Destructs the channel tree listbox.
          @memo Destructor.
          @return void
       ******************************************************************/
      virtual ~TLGChannelListbox ();
   
      /** Select a channel name.
          @memo Select a channel name.
   	  @param channelname name of channel
   	  @@return true if successful
       ******************************************************************/
      virtual Bool_t SelectChannel (const char* channelname) {
         return SelectByName (channelname) != 0; }
      /** Returns true if specified channel is selected.
          @memo Is channel name selected.
   	  @param channelname name of channel
   	  @return true if selected
       ******************************************************************/
      virtual Bool_t GetChannelSelection (const char* channelname) const {
         return GetSelectionByName (channelname); }
      /** Get the selected channel name.
          @memo Get selected channel name.
   	  @return  name of channel
       ******************************************************************/
      virtual const char* GetSelectedChannel () const {
         return GetSelectedName(); }
   
   protected:
   
      /// Add a channel to listbox
      virtual TLGLBTreeEntry* AddChannel (TLGLBTreeEntry *parent, 
                        const char *string, const char* fullname = 0,
                        long userData = -1, const TGPicture *open = 0,
                        const TGPicture *closed = 0) {
         return AddItem (parent, string, fullname, (void*) userData,
                        open, closed);
      }
      /// Delete all channels from listbox
      virtual void DeleteChannels() {
         DeleteItem (0);
      }
   };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGChannelCombobox                                                   //
//                                                                      //
// A tree combobox to select LIGO channel names                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

/** A combo box widget which lets the user select channel names by 
    presenting a tree like selection list.
   
    @memo Channel tree combobox.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGChannelCombobox : public TLGComboTree, public ChannelTree {
   public:
      /** Create a channel tree combobox.
          @memo Constructor.
          @param p Parent window
   	  @param id Widget id number
   	  @param chnnames List of channel names
          @param chnnum Number of channel names in list
	  @param copy Copy the channel list.
	  @param type type code
	  @param editable Editable window.
     	  @param option combobox options
   	  @param back combobox background color
       ******************************************************************/
#if ROOT_VERSION_CODE > 197893
      TLGChannelCombobox(TGWindow* p, Int_t id, 
			 ChannelEntry* chnnames, UInt_t chnnum,
			 Bool_t copy=kTRUE, Int_t type = kChannelTreeNormal,
			 Bool_t editable = kFALSE,
			 UInt_t option = kHorizontalFrame | kSunkenFrame | 
			 kDoubleBorder, ULong_t back = GetWhitePixel());
#else
      TLGChannelCombobox(TGWindow* p, Int_t id, 
			 ChannelEntry* chnnames, UInt_t chnnum, 
			 Bool_t copy=kTRUE, Int_t type = kChannelTreeNormal,
			 Bool_t editable = kFALSE,
			 UInt_t option = kHorizontalFrame | kSunkenFrame | 
			 kDoubleBorder, ULong_t back = fgWhitePixel);
#endif
      /** Create a channel tree combobox.
          @memo Constructor.
          @param p Parent window
   	  @param id Widget id number
   	  @param chnnames Space separated list of channel names
     	  @param option combobox options
   	  @param back combobox background color
       ******************************************************************/
#if ROOT_VERSION_CODE > 197893
      TLGChannelCombobox (TGWindow* p, Int_t id, const char* chnnames = 0, 
                        Int_t type = kChannelTreeNormal,
                        Bool_t editable = kFALSE,
                        UInt_t option = kHorizontalFrame | kSunkenFrame | 
                        kDoubleBorder, ULong_t back = GetWhitePixel());
#else
      TLGChannelCombobox (TGWindow* p, Int_t id, const char* chnnames = 0, 
                        Int_t type = kChannelTreeNormal,
                        Bool_t editable = kFALSE,
                        UInt_t option = kHorizontalFrame | kSunkenFrame | 
                        kDoubleBorder, ULong_t back = fgWhitePixel);
#endif
      /** Destructs the channel tree combobox.
          @memo Destructor.
          @return void
       ******************************************************************/
      virtual ~TLGChannelCombobox ();
   
      /** Select a channel name.
          @memo Select a channel name.
   	  @param channelname name of channel
   	  @return true if successful
       ******************************************************************/
      virtual Bool_t SelectChannel (const char* channelname, const int rate = -1) {
#ifdef TEST_LOCALRATE
	 SetRate(rate) ;
#endif
	 // The call is to TLGComboTree::SelectByName()
         return SelectByName (channelname, rate); }
      /** Set a channel name. This will force the channel name to be 
          displayed even if not in list.
          @memo Set a channel name.
   	  @param channelname name of channel
   	  @return true if channel in list
       ******************************************************************/
      virtual Bool_t SetChannel (const char* channelname, int rate = -1) {
#ifdef TEST_LOCALRATE
	 SetRate(rate) ;
#endif
         return SetByName (channelname, rate); }
      /** Returns true if specified channel is selected.
          @memo Is channel name selected.
   	  @param channelname name of channel
   	  @return true if selected
       ******************************************************************/
      virtual Bool_t GetChannelSelection (const char* channelname) const {
         return GetSelectionByName (channelname); }
      /** Get the selected channel name.
          @memo Get selected channel name.
   	  @return  name of channel
       ******************************************************************/
      virtual const char* GetSelectedChannel () const {
         return GetDisplayedName(); }

      /**
       * Get channel name stripped of any white space in case of user mis-entry.
       * @memo Get channel name stripped of any white space in case of user mis-entry.
       * @return name of channel with no leading or trailing white spaces.
       ******************************************************************/
      virtual std::string GetChannelNameStripped();

      /** Get the channel name currently displayed. This will return
          the displayed channel name even if it is not in the list.
          @memo Get channel name.
   	  @return name of channel
       ******************************************************************/
      virtual const char* GetChannel () const {
         return GetDisplayedName(); }

      // This gets the userData for the displayed name. The value was
      // added in AddItem() below.
      virtual void *GetChannelUserData() {
	 return GetSelectedUserData() ; }

      virtual void SetChannelUserData(void * userdata) {
	 //std::cerr << "TLGChannelCombobox::SetChannelUserData(" << (long) userdata << ")" << std::endl ;
	 SetSelectedUserData(userdata) ; // Through inheritance, calls TLGComboTree::SetSelectedUserData()
      }
#ifdef TEST_LOCALRATE
      long GetChannelRate() {
	 return GetRate() ;
      }

      void SetChannelRate(long rate) {
	 fRate = rate ;
      }
#endif
   
   protected:
   
      /// Add a channel to combobox
      virtual TLGLBTreeEntry* AddChannel (TLGLBTreeEntry *parent, 
                        const char *string, const char* fullname = 0,
                        long userData = -1, const TGPicture *open = 0,
                        const TGPicture *closed = 0) {
         return AddItem (parent, string, fullname, (void*) userData,
                        open, closed);
      }
      /// Delete all channels from combobox
      virtual void DeleteChannels() {
         DeleteItem (0);
      }
   };

//@}
}

#endif
