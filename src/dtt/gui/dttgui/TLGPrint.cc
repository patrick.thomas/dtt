

#include "PConfig.h"
#include "TLGPrint.hh"
#include "TLGEntry.hh"
#include "pipe_exec.hh"
#include <cstring>
#include <TStyle.h>
#include <TPad.h>
#include <TMath.h>
#include <TGFileDialog.h>
#include <TGMsgBox.h>
#include <TSystem.h>
#include <TVirtualX.h>
// federated naming service?
#ifdef __USE_FN_SERVICE
#include <cstdio>
#include <xfn/xfn.h>
#include <cstdlib>
#endif
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <cmath>
#include <strings.h>

namespace dttgui {
   using namespace std;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Constants			                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
#ifdef __USE_FN_SERVICE
   const char* const printservices = "thisorgunit/service/printer";
#endif
   const char* const kPrinterDefFile1 = "/etc/printers.conf";
   const char* const kPrinterDefFile2 = "/etc/printcap";
   const char* const kPrinterDefFile3 = "/etc/printcap.local";
   const char* const kPrinterDefs[] =
   {kPrinterDefFile1, kPrinterDefFile2, kPrinterDefFile3, 0};


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Print and conversion commands                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   // Print command
   const char* const kDefPrintCommand = "lp -c -d%Printer %File";
   // Postscript to PDF
   const char* const ps2pdf = "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite "
   "-sOutputFile=%s -r72 -c save pop %s -f %s";
   const char* const pdfPortrait = "";
   const char* const pdfLandscape = "\"<</Orientation 3>> setpagedevice\"";
   // Postscript to JPEG
   const char* const ps2jpeg = "gs -q -dNOPAUSE -dBATCH -sDEVICE=jpeg "
   "-sOutputFile=%s -f %s";
   // Postscript to AI
   const char* const ps2ai = "gs -q -dNOPAUSE -dBATCH -dNODISPLAY "
   "ps2ai.ps %s -c quit > %s";
   // Postscript to PNG
   const char* const ps2png = "gs -q -dNOPAUSE -dBATCH -sDEVICE=png16m "
   "-sOutputFile=%s -r72 -f %s";



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Print file types		                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static const char* const gPrintTypes[] = { 
   "Postscript", "*.ps",
   "PDF file", "*.pdf",
   "EPS file", "*.eps",
   "JPEG file", "*.jpg",
   "PNG file", "*.png",
   "Adobe Illustrator", "*.ai",
   "All files", "*.*",
   0, 0 };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Get list of printer names                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   Bool_t TLGPrintParam::GetPrinterList (char* list, int maxlen)
   {
      map<string, int> plist;
   
      // go through printer configuration files
      for (const char* const* p = kPrinterDefs; *p; ++p) {
         ifstream pconf (*p);
         while (pconf) {
            string line;
            getline (pconf, line);
            while (!line.empty() && isspace (line[0])) line.erase (0, 1);
            if (line.empty() || (line[0] == '#') ||
               !isalpha (line[0])) {
               continue;
            }
            string::size_type pos = line.find_first_of (":|");
            if ((pos == string::npos) || (pos == 0)) {
               continue;
            }
            line.erase (pos);
            plist[line] = 0;
         }
      }
   
      // use lpstat -a 
      pipe_exec lpstat ("sh -c 'lpstat -a; echo done'", "r");
      while (lpstat) {
         string line;
         getline (lpstat, line);
         while (!line.empty() && isspace (line[0])) line.erase (0, 1);
      //cout << "lpstat: '" << line << "'" << endl;
         string::size_type pos = line.find_first_of (" \t");
         if (pos != string::npos) line.erase (pos);
         if (line.empty()) {
            continue;
         }
         if (strncmp (line.c_str(), "done", 4) == 0) {
            break;
         }
         plist[line] = 0;
      }
   
      // Solaris < 10: Use federated naming service (what a pain!)
   #ifdef __USE_FN_SERVICE
      FN_ctx_t*			initial_context;
      FN_ctx_t*			printer_context;
      FN_composite_name_t*	printer_name_comp;
      FN_status_t*		status = fn_status_create();
      FN_ref_t*			printer_ref;
      FN_string_t*		printer_string;
      FN_string_t*		empty_string;
      FN_composite_name_t*	empty_name;
      FN_namelist_t*		children;
      FN_string_t*		child;
      unsigned int 		statcode;
   
   /* Convert the printer name to a composite name */
      printer_string = fn_string_from_str ((unsigned char*)printservices);
      printer_name_comp = fn_composite_name_from_string (printer_string);
      fn_string_destroy(printer_string);
   
   /* Get the initial context */
      initial_context = fn_ctx_handle_from_initial (0, status);
   
   /* Check status for any error messages */
      if (!fn_status_is_success(status)){
         fn_composite_name_destroy (printer_name_comp);
         fn_ctx_handle_destroy (initial_context);
         fn_status_destroy (status);
         return kFALSE;
      }
   /* Perform a lookup for the printer name */
      printer_ref = fn_ctx_lookup (initial_context,
                           printer_name_comp, status);
      fn_composite_name_destroy (printer_name_comp);
      fn_ctx_handle_destroy (initial_context);
   
   /* Check status for any error messages */
      if (!fn_status_is_success (status)){
         fn_status_destroy (status);
         return kFALSE;
      }
   
   /* Create printer context */
      printer_context = fn_ctx_handle_from_ref (printer_ref, 0, status);
      fn_ref_destroy (printer_ref);
   
   /* Check status for any error messages */
      if (!fn_status_is_success (status)){
         fn_status_destroy (status);
         return kFALSE;
      }
   
   /* Create an empty string */
      empty_string = fn_string_create();
      empty_name = fn_composite_name_from_string (empty_string);
      fn_string_destroy (empty_string);
   
   /* list all names in th eprinter context */
      children = fn_ctx_list_names (printer_context, empty_name, status);
      fn_composite_name_destroy (empty_name);
   
   /* check if any entries */
      if (children == NULL) {
         fn_ctx_handle_destroy (printer_context);
         fn_status_destroy (status);
         return kFALSE;
      }
   
   /* loop through entries */
      while ((child = fn_namelist_next (children, status)) != NULL) {
         plist[string ((const char*)fn_string_str (child, &statcode))] = 0;
   //       if (size + (int)strlen ((const char*) 
   //                         fn_string_str (child, &statcode)) + 2 < maxlen) {
   //          sprintf (p, "%s\n", );
   //          size += strlen (p);
   //          p += strlen (p);
   //       }
         fn_string_destroy (child);
      }
      fn_namelist_destroy (children);
      fn_ctx_handle_destroy (printer_context);
   
      fn_status_destroy (status);
   //    return kTRUE;
   #endif
   
      if (plist.empty()) {
         strcpy (list, "_default");
         return kFALSE;
      }
      else {
         char*			p;
         int			size;
         strcpy (list, "");
         p = list;
         size = 0;
	 cerr << "TLGPrintParam::GetPrinterList() printer list" << endl ;
         for (map<string,int>::iterator i = plist.begin();
             i != plist.end(); ++i) {
	    cerr <<  "  " << i->first << endl ;
            if (size + (int)i->first.length() + 2 < maxlen) {
               sprintf (p, "%s\n", i->first.c_str());
               size += strlen (p);
               p += strlen (p);
            }
         }
	 cerr << "TLGPrintParam::GetPrinterList() end" << endl ;
         return kTRUE;
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Postscript class 	                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   void TLGPostScript::Open (const char* fname, Int_t wtype)
   {
      cerr << "TLGPostScript::Open(" << fname << ", "<< wtype << ")" << endl ;;
      if (fStream) {
         Warning("Open", "postscript file already open");
         return;
      }
   
      fMarkerSizeCur = 0;
      fCurrentColor  = 0;
      fRed         = -1;
      fGreen       = -1;
      fBlue        = -1;
      fLenBuffer   = 0;
      fClip        = 0;
      fType        = abs(wtype);
      fClear       = kTRUE;
      fZone        = kFALSE;
      fSave        = 0;
      SetLineScale(gStyle->GetLineScalePS());
      gStyle->GetPaperSize(fXsize, fYsize);
      cerr << "  gStyle Paper size x=" << fXsize << ", y=" << fYsize << endl ;
      fMode        = fType%10;
      cerr << "  fMode = " << fMode << endl;
      Float_t xrange, yrange;
      if (gPad) {
         Float_t ww    = gPad->GetWw();
         Float_t wh    = gPad->GetWh();
         if (fMode == 3) {
	    // Encapsulated Postscript
            ww *= gPad->GetWNDC();
            wh *= gPad->GetHNDC();
         }
         Float_t ratio = wh/ww;
	 cerr << "  Pad size w=" << ww << ", h=" << wh << ", ratio = " << ratio << endl ;
// If gStyle->GetPaperSize(fXsize, fYsize) returns the long dimension in fXsize when
// landscape is selected, and the short dimension when portrait is selected, there is 
// no need to change the size calculations based on landscape/portrait.
#if 1
         xrange = fXsize;
         yrange = fXsize*ratio;
         if (yrange > fYsize) { yrange = fYsize; xrange = yrange/ratio;}
#else
         if (fMode == 2) {
	    // Landscape mode
            xrange = fYsize;
            yrange = xrange*ratio;
            if (yrange > fXsize) { yrange = fXsize; xrange = yrange/ratio;}
         } 
         else {
	    // Portrait or Encapsulated Postscript. 
            xrange = fXsize;
            yrange = fXsize*ratio;
            if (yrange > fYsize) { yrange = fYsize; xrange = yrange/ratio;}
         }   
#endif
         fXsize = xrange; fYsize = yrange;
	 cerr << "  final size x=" << fXsize << ", y=" << fYsize << endl ;
      }
   
   //*-*- open OS file
      fStream = new ofstream(fname, ios::out);
      if ((fStream == 0) || !*fStream) {
         printf("ERROR in TPostScript::Open: Cannot open file:%sn",fname);
         return;
      }
      gVirtualPS = this;
   
   #if ROOT_VERSION_CODE >= ROOT_VERSION(4,0,0)
      for (Int_t i=0;i<fSizBuffer;i++) fBuffer[i] = ' ';
   #else
      for (Int_t i=0;i<512;i++) fBuffer[i] = ' ';
   #endif
      if( fMode == 3) {
         fBoundingBox = kFALSE;
         PrintStr("%!PS-Adobe-2.0 EPSF-2.0@");
      }
      else {
         fBoundingBox = kTRUE;
         PrintStr("%!PS-Adobe-2.0@");
         Initialize();
      }
   
      fClipStatus  = kFALSE;
      fRange       = kFALSE;
      fType = 110 + fType % 10;
   
   //*-*- Set a default range
      cerr << "  calling Range(" << fXsize << ", " << fYsize << ")" << endl ;
      Range(fXsize, fYsize);
   
      fPrinted     = kFALSE;
      //if (fType % 10 == 3) NewPage();   
      cerr << "TLGPostScript::Open() - end" << endl;
   }

//______________________________________________________________________________
   void TLGPostScript::NewPage()
   {
      cerr << "TLGPostScript::NewPage()" << endl;
      Float_t xrange, yrange;
      if (gPad) {
         gStyle->GetPaperSize(fXsize, fYsize);
	 cerr << "  gStyle Paper size x=" << fXsize << ", y=" << fYsize << endl ;
         Float_t rpxmin, rpymin;
         if (fXsize < fYsize) {
            rpxmin = 0.7;
            rpymin = TMath::Sqrt(2.)*rpxmin;
         }
         else {
            rpymin = 0.7;
            rpxmin = TMath::Sqrt(2.)*rpymin;
         }
         Int_t xzone = (fType % 10 == 3) ? 1 : fNXzone;
         Int_t yzone = (fType % 10 == 3) ? 1 : fNYzone;
         fXsize = (fXsize - 1.*rpxmin) / xzone; 
         fYsize = (fYsize - 1.*rpymin) / yzone;
         Float_t ww    = gPad->GetWw();
         Float_t wh    = gPad->GetWh();
         if (fType % 10 == 3) {
            ww *= gPad->GetWNDC();
            wh *= gPad->GetHNDC();
         }
         Float_t ratio = wh/ww;
	 cerr << "  Pad size w=" << ww << ", h=" << wh << ", ratio = " << ratio << endl ;
         xrange = fXsize;
         yrange = fXsize*ratio;
         if (yrange > fYsize) { yrange = fYsize; xrange = yrange/ratio;}
         fXsize = xrange; fYsize = yrange;
         fRange       = kFALSE;
	 cerr << "  calling Range(" << fXsize << ", " << fYsize << ")" << endl ;
         Range(fXsize, fYsize);
      }
      else fYsize = 27;
   
      if (fType % 10 == 3 && !fBoundingBox) {
         // EPS are half size only
         fXsize /= 2;
         fYsize /= 2;
         Bool_t psave = fPrinted;
         PrintStr("@%%BoundingBox: ");
         WriteInteger(CMtoPS(0));
         WriteInteger(CMtoPS(0));
         WriteInteger(CMtoPS(fXsize));
         WriteInteger(CMtoPS(fYsize));
         PrintStr("@");
         Initialize();
         fBoundingBox  = kTRUE;
         fPrinted      = psave;
         SaveRestore (1);
      }
      //if (fPrinted) {
      if (fSave) SaveRestore(-1);
      fPrinted  = kFALSE;
      fClear    = kTRUE;
      //}
      Zone();     
      cerr << "TLGPostScript::NewPage() end" << endl;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGPrintDialog   		                                        //
//                                                                      //
// Print dialog box for TLGMultiPad                                     //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGPrintDialog::TLGPrintDialog (const TGWindow *p, 
                     const TGWindow *main, TLGPrintParam& pdlg, 
                     Bool_t& ret, EPrintDialog pd)
   : TLGTransientFrame (p, main, 10, 10), fPDlg (&pdlg), fOk (&ret), 
   fPrintDlgType (pd)
   {
      // group frames
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 4, 4, 4, 4);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop, 0, 0, 0, 0);
      fL[5] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop, 0, 0, 2, -6);      
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 4, 2, 2);
      fL[3] = new TGLayoutHints (kLHintsRight | kLHintsCenterY, 6, 4, 2, 2);
      fL[4] = new TGLayoutHints (kLHintsRight | kLHintsCenterY, 4, 0, 2, 2);
      fGroupPrinter = new TGGroupFrame (this, "Printer");
      AddFrame (fGroupPrinter, fL[0]);
      fGroupLayout = new TGGroupFrame (this, "Page Layout");
      AddFrame (fGroupLayout, fL[0]);
      fGroupOrientation = new TGGroupFrame (this, "Page Orientation");
      AddFrame (fGroupOrientation, fL[0]);
      fGroupSelection = new TGGroupFrame (this, "Plot Selection");
      AddFrame (fGroupSelection, fL[0]);
      fButtonFrame = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fButtonFrame, fL[0]);
   
      // printer frame
      fFrame[0] = new TGHorizontalFrame (fGroupPrinter, 10, 10);
      fGroupPrinter->AddFrame (fFrame[0], fL[1]);
      fFrame[1] = new TGHorizontalFrame (fGroupPrinter, 10, 10);
      fGroupPrinter->AddFrame (fFrame[1], fL[1]);
      fFrame[6] = new TGHorizontalFrame (fGroupPrinter, 10, 10);
      fGroupPrinter->AddFrame (fFrame[6], fL[1]);
      fFrame[2] = new TGHorizontalFrame (fGroupPrinter, 10, 10);
      fGroupPrinter->AddFrame (fFrame[2], fL[5]);
      fLPrinter = new TGLabel (fFrame[0], "Printer:      ");
      fFrame[0]->AddFrame (fLPrinter, fL[2]);
      // Printer selection combo box. - id = 1
      fPrinter = new TGComboBox (fFrame[0], 1);
      fPrinter->Associate (this);
      fPrinter->Resize (220, 23);
      fFrame[0]->AddFrame (fPrinter, fL[4]);
      fLPaperSize = new TGLabel (fFrame[1], "Page size: ");
      fFrame[1]->AddFrame (fLPaperSize, fL[2]);
      // Page size combo box. - id = 2
      fPaperSize = new TGComboBox (fFrame[1], 2);
      fPaperSize->Associate (this);
      fPaperSize->Resize (220, 23);
      fFrame[1]->AddFrame (fPaperSize, fL[4]);
      fLPrintCommand = new TGLabel (fFrame[6], "Print command:");
      fFrame[6]->AddFrame (fLPrintCommand, fL[2]);
      // Print command text box, editable.  id = 16, initial text in fPrintCommand.
      fPrintCommand = new TGTextEntry (fFrame[6], fPDlg->fPrintCommand, 16);
      fPrintCommand->Associate (this);
      fPrintCommand->Resize (220, 23);
      fFrame[6]->AddFrame (fPrintCommand, fL[4]);
      // Print to File checkbox.  id = 3.
      fPrintToFile = new TGCheckButton (fFrame[2], "Print to file:", 3);
      fPrintToFile->Associate (this);
      fFrame[2]->AddFrame (fPrintToFile, fL[2]);
      // Print file format combo box, id = 2
      fFileFormat = new TGComboBox (fFrame[2], 2);
      fFileFormat->Associate (this);
      fFileFormat->Resize (220, 23);
      fFrame[2]->AddFrame (fFileFormat, fL[4]);
   
      // Page Layout frame
      fFrame[3] = new TGHorizontalFrame (fGroupLayout, 10, 10);
      fGroupLayout->AddFrame (fFrame[3], fL[5]);
      // Radio buttons, id = 5, 6, 7
      fLayout[0] = new TGRadioButton (fFrame[3], "1 up     ", 5);
      fLayout[0]->Associate (this);
      fFrame[3]->AddFrame (fLayout[0], fL[2]);
      fLayout[1] = new TGRadioButton (fFrame[3], "2 up     ", 6);
      fLayout[1]->Associate (this);
      fFrame[3]->AddFrame (fLayout[1], fL[2]);
      fLayout[2] = new TGRadioButton (fFrame[3], "4 up", 7);
      fLayout[2]->Associate (this);
      fFrame[3]->AddFrame (fLayout[2], fL[2]);
   
      // Page Orientation frame
      fFrame[4] = new TGHorizontalFrame (fGroupOrientation, 10, 10);
      fGroupOrientation->AddFrame (fFrame[4], fL[5]);
      // Radio buttons, id = 8, 9
      fOrientation[0] = new TGRadioButton (fFrame[4],
                           "Portrait     ", 8);
      fOrientation[0]->Associate (this);
      fFrame[4]->AddFrame (fOrientation[0], fL[2]);
      fOrientation[1] = new TGRadioButton (fFrame[4],
                           "Landscape", 9);
      fOrientation[1]->Associate (this);
      fFrame[4]->AddFrame (fOrientation[1], fL[2]);
   
      // Plot Selection frame
      fFrame[5] = new TGHorizontalFrame (fGroupSelection, 10, 10);
      fGroupSelection->AddFrame (fFrame[5], fL[5]);
      // Radio buttons, id = 10, 11, 12, 13
      fSelection[0] = new TGRadioButton (fFrame[5], "Current   ", 10);
      fSelection[0]->Associate (this);
      fFrame[5]->AddFrame (fSelection[0], fL[2]);
      fSelection[1] = new TGRadioButton (fFrame[5], "First     ", 11);
      fSelection[1]->Associate (this);
      fFrame[5]->AddFrame (fSelection[1], fL[2]);
      fSelection[2] = new TGRadioButton (fFrame[5], "Second    ", 12);
      fSelection[2]->Associate (this);
      fFrame[5]->AddFrame (fSelection[2], fL[2]);
      fSelection[3] = new TGRadioButton (fFrame[5], "All", 13);
      fSelection[3]->Associate (this);
      fFrame[5]->AddFrame (fSelection[3], fL[2]);
   
      // setup buttons
      // Text buttons for OK, Cancel.  OK id = 14, Cancel id = 15
      fCancelButton = new TGTextButton (fButtonFrame, 
                           new TGHotString ("   &Cancel   "), 15);
      fCancelButton->Associate (this);
      fButtonFrame->AddFrame (fCancelButton, fL[3]);
      fOkButton = new TGTextButton (fButtonFrame, 
                           new TGHotString ("      &Ok      "), 14);
      fOkButton->Associate (this);
      fButtonFrame->AddFrame (fOkButton, fL[3]);
   
      // set printer selection
      fPrinter->AddEntry ("[Default printer]", 0);
      Int_t def = 0;
      int	indx = 1;
      char	buf[2048];
      if (TLGPrintParam::GetPrinterList (buf, sizeof (buf))) {
         char* p = buf;
         char* end;
         while ((strlen (p) != 0) &&
               ((end = strchr (p, '\n')) != 0)) {
            *end = 0;
            if (strcasecmp (p, "_default") != 0) {
               if (strcasecmp (p, fPDlg->fPrinter) == 0) {
                  def = indx;
               }
               fPrinter->AddEntry (p, indx++);
            }
            p = end + 1;
         }
      }
      fPrinter->Select(def);
      // set paper size selection
      fPaperSize->AddEntry ("Letter (215.9mm x 279.4mm)", 0);
      fPaperSize->AddEntry ("11x17 (279.4mm x 431.8mm)", 1);
      fPaperSize->AddEntry ("A4 (209.9mm x 297.0mm)", 2);
      fPaperSize->AddEntry ("A3 (297.0mm x 420.2mm)", 3);
      if (fabs (fPDlg->fPaperSizeWidth - 27.94) < 1E-3) {
         fPaperSize->Select(1);
      }
      else if (fabs (fPDlg->fPaperSizeWidth - 20.99) < 1E-3) {
         fPaperSize->Select(2);
      }
      else if (fabs (fPDlg->fPaperSizeWidth - 297.0) < 1E-3) {
         fPaperSize->Select(3);
      }
      else {
         fPaperSize->Select(0);
      }
      // set file formats
      if (fPDlg->fPrintToFile) {
         fPrintToFile->SetState (kButtonDown);
      }
      fFileFormat->AddEntry ("Postscript", 0);
      fFileFormat->AddEntry ("PDF", 1);
      fFileFormat->AddEntry ("EPS", 2);
      fFileFormat->AddEntry ("JPEG", 4);
      fFileFormat->AddEntry ("PNG", 5) ;
      fFileFormat->AddEntry ("Adobe Illustrator", 6);
      fFileFormat->Select (fPDlg->fFileFormat);
      // set radio buttons
      def = 0;
      if ((fPDlg->fPageLayout >= 0) && (fPDlg->fPageLayout < 3)) {
         def = fPDlg->fPageLayout;
      }
      for (int i = 0; i < 3; i++) {
         fLayout[i]->SetState (i == def ? kButtonDown : kButtonUp);
      }
      def = 0;
      if ((fPDlg->fPageOrientation >= 0) && (fPDlg->fPageOrientation < 2)) {
         def = fPDlg->fPageOrientation;
      }
      for (int i = 0; i < 2; i++) {
         fOrientation[i]->SetState (i == def ? kButtonDown : kButtonUp);
      }
      def = 0;
      if ((fPDlg->fPlotSelection >= 0) && (fPDlg->fPlotSelection < 4)) {
         def = fPDlg->fPlotSelection;
      }
      for (int i = 0; i < 4; i++) {
         fSelection[i]->SetState (i == def ? kButtonDown : kButtonUp);
      }
   
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
   
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
   
      // set dialog box title
      TString name = 
         (pd == TLGPrintParam::kPrint) ? "Print" : "Print setup";
      SetWindowName (name);
      SetIconName (name);
      SetClassHints ("PrintDlg", "PrintDlg");
   
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
      //SetMWMHints (kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGPrintDialog::~TLGPrintDialog()
   {
      delete fOkButton;
      delete fCancelButton;
      delete fButtonFrame;
      delete fSelection[0];
      delete fSelection[1];
      delete fSelection[2];
      delete fSelection[3];
      delete fOrientation[1];
      delete fOrientation[0];
      delete fLayout[0];
      delete fLayout[1];
      delete fLayout[2];
      delete fFileFormat;
      delete fPrintToFile;
      delete fPrintCommand;
      delete fLPrintCommand;
      delete fPaperSize;
      delete fLPaperSize;
      delete fPrinter;
      delete fLPrinter;
      for (int i = 0; i < 7; i++) {
         delete fFrame[i];
      }
      delete fGroupPrinter;
      delete fGroupLayout;
      delete fGroupOrientation;
      delete fGroupSelection;
      for (int i = 0; i < 6; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   void TLGPrintDialog::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
   #if ROOT_VERSION_CODE >= ROOT_VERSION(3,4,2)
      DeleteWindow();
   #else
      delete this;
   #endif
   }

//______________________________________________________________________________
   Bool_t TLGPrintDialog::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // OK button
            case 14:
               {
                  // get data
                  fPDlg->fPrinter = "";
                  if (fPrinter->GetSelected() != 0) {
                     TGTextLBEntry* e = (TGTextLBEntry*)
                        fPrinter->GetSelectedEntry();
                     if (e != 0) {
                        fPDlg->fPrinter = e->GetText()->GetString();
                     }
                  }
                  fPDlg->fPrintCommand = fPrintCommand->GetText();
                  if (fPDlg->fPrintCommand.Length() == 0) {
                     fPDlg->fPrintCommand = kDefPrintCommand;
                  }
                  switch (fPaperSize->GetSelected()) {
		     // Letter size paper
                     case 0:
                     default:
                        {
                           fPDlg->fPaperSizeWidth = 21.59; //units are cm.
                           fPDlg->fPaperSizeHeight = 27.94;
                           break;
                        }
		     // 11 x 17 paper
                     case 1:
                        {
                           fPDlg->fPaperSizeWidth = 27.94;
                           fPDlg->fPaperSizeHeight = 43.18;
                           break;
                        }
		     // A4 paper
                     case 2:
                        {
                           fPDlg->fPaperSizeWidth = 20.99;
                           fPDlg->fPaperSizeHeight = 29.70;
                           break;
                        }
		     // A3 paper
                     case 3:
                        {
                           fPDlg->fPaperSizeWidth = 29.70;
                           fPDlg->fPaperSizeHeight = 42.02;
                           break;
                        }
                  }
                  fPDlg->fPrintToFile = 
                     (fPrintToFile->GetState() == kButtonDown);
                  fPDlg->fFileFormat = (TLGPrintParam::EPFileFormat) 
                     fFileFormat->GetSelected();
                  for (int i = 0; i < 3; i++) {
                     if (fLayout[i]->GetState () == kButtonDown) {
                        fPDlg->fPageLayout = i;
                        break;
                     }
                  }
                  for (int i = 0; i < 2; i++) {
                     if (fOrientation[i]->GetState () == kButtonDown) {
                        fPDlg->fPageOrientation = i;
                        break;
                     }
                  }
                  for (int i = 0; i < 4; i++) {
                     if (fSelection[i]->GetState () == kButtonDown) {
                        fPDlg->fPlotSelection = i;
                        break;
                     }
                  }
               
                  // if print to file, ask for filename
                  fPDlg->fFilename = "";
                  if ((fPrintDlgType == TLGPrintParam::kPrint) && 
                     fPDlg->fPrintToFile) {
                     // file save as dialog
                     TGFileInfo	info;
                     info.fFilename = 0;
                     info.fIniDir = 0;
                  #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
                     info.fFileTypes = const_cast<const char**>(gPrintTypes);
                  #else
                     info.fFileTypes = const_cast<char**>(gPrintTypes);
                  #endif
                     TString defext;
		     // Note - When setting the fFileTypeIdx, the values are double
		     // what you might expect.  Each entry in the file filter combo box
		     // consists of 2 strings (see gPrintTypes) and when they are added
		     // to the combo box by the TGFileDialog constructor, the id for each
		     // entry is the index of the first string for the entry.  See the
		     // ROOT source code for the constructor.
                     switch (fPDlg->fFileFormat) {
                        case TLGPrintParam::kEPS:
                           defext = ".eps";	   // Set the default file extension.
			   info.fFileTypeIdx = 4 ; // Set the initial file filter type.
                           break;
                        case TLGPrintParam::kPDF:
                           defext = ".pdf";
			   info.fFileTypeIdx = 2 ;
                           break;
                        case TLGPrintParam::kJPEG:
                           defext = ".jpg";
			   info.fFileTypeIdx = 6 ;
                           break;
			case TLGPrintParam::kPNG:
			   defext = ".png";
			   info.fFileTypeIdx = 8 ;
			   break ; // Bugzilla 755
                        case TLGPrintParam::kAI:
                           defext = ".ai";
			   info.fFileTypeIdx = 10 ;
                           break;
                        case TLGPrintParam::kPostscript:
                        default:
                           defext = ".ps";
			   info.fFileTypeIdx = 0 ;
                           break;
                     }
		  #if 1
		     {
			new TLGFileDialog(this, &info, kFDSave) ;
		     }
		     if (!info.fFilename)
		  #else
                     if (!TLGFileDialog (this, info, kFDSave, defext))
		  #endif
		     {
                     #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
                        delete [] info.fFilename;
                     #endif
                        return kTRUE;
                     }
                     fPDlg->fFilename = info.fFilename;
                  #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
                     delete [] info.fFilename;
                  #endif
                  }
               
                  if (fOk) *fOk = kTRUE;
                  DeleteWindow();
                  break;
               }
            // Cancel button
            case 15:
               {
                  if (fOk) *fOk = kFALSE;
                  DeleteWindow();
                  break;
               }
         }
      }
      // Radio buttons
      else if ((GET_MSG (msg) == kC_COMMAND) &&
              (GET_SUBMSG (msg) == kCM_RADIOBUTTON)) {
         switch (parm1) {
	    // Page Layout
            case 5: // 1 up
            case 6: // 2 up
            case 7: // 4 up
               {
                  for (int i = 0; i < 3; i++) {
                     fLayout[i]->SetState (i == parm1 - 5 ? 
                                          kButtonDown : kButtonUp);
                  }
                  break;
               }
	    // Page Orientation
            case 8: // Portrait
            case 9: // Landscape
               {
                  for (int i = 0; i < 2; i++) {
                     fOrientation[i]->SetState (i == parm1 - 8 ? 
                                          kButtonDown : kButtonUp);
                  }
                  break;
               }
	    // Plot Selection
            case 10: // Current
            case 11: // First
            case 12: // Second
            case 13: // All
               {
                  for (int i = 0; i < 4; i++) {
                     fSelection[i]->SetState (i == parm1 - 10 ? 
                                          kButtonDown : kButtonUp);
                  }
                  break;
               }
         }
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Printer dialog parameters                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGPrintParam::TLGPrintParam ()
   {
      init();
   }

//______________________________________________________________________________
   TLGPrintParam::TLGPrintParam (const char* format)
   {
      init();
      if (format) {
         string s (format);
         string::size_type pos = s.find ('#');
         string format;
         string file;
         if (pos != string::npos) {
            format = s.substr (0, pos);
            file = s.substr (pos + 1, string::npos);
         }
         else {
            file = s;
         }
         fPageOrientation = 1;
         if (format.find ('P') != string::npos) {
            fPageOrientation = 0;
         }
         fPlotSelection = 1;
         if (format.find ('S') != string::npos) {
            fPlotSelection = 2;
         }
         if (format.find ('A') != string::npos) {
            fPlotSelection = 3;
         }
         fPageLayout = 0;
         for (int i = 0; i < (int)format.size(); ++i) {
            if (isdigit (format[i])) {
               switch (format[i] - '0') {
                  case 2:
                     fPageLayout = 1;
                     break;
                  case 4:
                     fPageLayout = 2;
                     break;
                  case 1:
                  default:
                     fPageLayout = 0;
                     break;
               }
            }
         }
         fPrinter = "";
         fFilename = "";
         pos = file.find_last_of ('.');
         if (pos != string::npos) {
            fFilename = file.c_str();
            fPrintToFile = kTRUE;
            if (strcasecmp (file.c_str() + pos + 1, "ps") == 0) {
               fFileFormat = kPostscript;
            }
            else if (strcasecmp (file.c_str() + pos + 1, "pdf") == 0) {
               fFileFormat = kPDF;
            }
            else if (strcasecmp (file.c_str() + pos + 1, "eps") == 0) {
               fFileFormat = kEPS;
            }
            else if ((strcasecmp (file.c_str() + pos + 1, "jpg") == 0) ||
                    (strcasecmp (file.c_str() + pos + 1, "jpeg") == 0)) {
               fFileFormat = kJPEG;
            }
	    else if (strcasecmp (file.c_str() + pos + 1, "png") == 0) {
	       fFileFormat = kPNG ;
	    }
            else if (strcasecmp (file.c_str() + pos + 1, "ai") == 0) {
               fFileFormat = kAI;
            }
            else {
               fFileFormat = kPDF;
            }
         }
         else {
            fPrinter = file.c_str();
            fPrintToFile = kFALSE;
         }
      }
   }

//______________________________________________________________________________
   TLGPrintParam::~TLGPrintParam ()
   {
      Finish (kTRUE);
   }

//______________________________________________________________________________
   void TLGPrintParam::init ()
   {
      fPrinter = "";
      fPrintCommand = kDefPrintCommand;
      fFileFormat = kPostscript;
      fPrintToFile = kFALSE;
      fFilename = "";
      fPaperSizeWidth = 21.59;
      fPaperSizeHeight = 27.94;
      fPageLayout = 0;
      fPageOrientation = 0;
      fPlotSelection = 0;
      fErrorCode = 0;
      fOutFilename = "";
      fTempfileUse = kFALSE;
      fPrintCmd = "";
   }

//______________________________________________________________________________
   Bool_t TLGPrintParam::ShowDialog (const TGWindow* p, 
                     const TGWindow* main, EPrintDialog pd)
   {
      Bool_t ret = kFALSE;
      new TLGPrintDialog (p, main, *this, ret, pd);
      return ret;
   }

//______________________________________________________________________________
   Bool_t TLGPrintParam::IsFormatSupported()
   {
      // check if we have ghostscript
      static Bool_t haveGS = kFALSE;
      if (fPrintToFile && !haveGS &&
         ((fFileFormat == TLGPrintParam::kPDF) || 
         (fFileFormat == TLGPrintParam::kJPEG) || 
         (fFileFormat == TLGPrintParam::kAI) || 
	 (fFileFormat == TLGPrintParam::kPNG))) {
         haveGS = (gSystem->Exec ("gs -h > /dev/null 2>&1") == 0);
         if (!haveGS) {
            fErrorCode = -1;
	    cerr << "IsFormatSupported failed test 1" << endl ;
            return kFALSE;
         }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGPrintParam::Setup (TString& filename)
   {
      Finish (kTRUE);
      if (!IsFormatSupported()) {
         return kFALSE;
      }
      // always print to file!
      if (fPrintToFile && 
         ((fFileFormat == TLGPrintParam::kPostscript) || 
         (fFileFormat == TLGPrintParam::kEPS))) {
         fOutFilename = fFilename;
         fTempfileUse = kFALSE;
      }
      else {
	 //#define L_tmpnam 32
         char buf[32];
	 strcpy(buf, "/tmp/TLGPrint-XXXXXX");
	 int fdtmp = mkstemp(buf);
	 if ( fdtmp < 0 )
	 {
	    fErrorCode = -1;
	    cerr << "Setup failed to create temporary file; " << buf << endl ;
	    return kFALSE;
	 }
	 else
	 {
	   fOutFilename = buf;
	   fTempfileUse = kTRUE;
	 }
      }
      filename = fOutFilename;
      fErrorCode = 0;
   
      // construct print command
      TString pcmd = fPrintCommand;
      if (!fPrintToFile) {
         if (fPrinter.Length() == 0) {
            // remove %Printer for default printer
            Int_t i = pcmd.Index ("%printer", 8, 0, TString::kIgnoreCase);
            if (i != kNPOS) {
               pcmd.Remove (i, 8);
               while ((--i >= 0) && (pcmd[i] != ' ')) {
                  pcmd.Remove (i, 1);
               }
            }
         }
         else {
            // replace all apearences of %printer with the printer name
            Int_t i = 0;
            TString pr = TString (" ") + fPrinter;
            while ((i = pcmd.Index ("%printer", 8, i, 
                                   TString::kIgnoreCase)) != kNPOS) {
               pcmd.Replace (i, 8, pr, pr.Length());
               i += pr.Length();
            }
         }
         Int_t i = 0;
         // replace all apearences of %file with the file name
         while ((i = pcmd.Index ("%file", 5, i, 
                                TString::kIgnoreCase)) != kNPOS) {
            pcmd.Replace (i, 5, filename, filename.Length());
            i += filename.Length();
         }
      }
      fPrintCmd = pcmd;
      cout << "Print to " << filename << endl;
      cout << "Print command " << fPrintCmd << endl;
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGPrintParam::Finish (Bool_t cleanuponly)
   {
      Bool_t ret = kTRUE;
   
      if (!cleanuponly && !fPrintToFile) {
         // start print program
         ret = (gSystem->Exec (fPrintCmd) == 0);
         if (!ret) fErrorCode = -5;
      }
      // Convert to final file format using ghostscript
      if (!cleanuponly && fPrintToFile) {
         char convcmd[1024];
         switch (fFileFormat) {
            // ps2pdf
            case TLGPrintParam::kPDF:
               sprintf (convcmd, ps2pdf, (const char*) fFilename, 
                       (fPageOrientation == 1) ? pdfLandscape : pdfPortrait,
                       (const char*) fOutFilename);
               break;
            // ps2jpg
            case TLGPrintParam::kJPEG: 
               sprintf (convcmd, ps2jpeg, (const char*) fFilename, 
                       (const char*) fOutFilename);
               break;
	    // ps2png
	    case TLGPrintParam::kPNG:
	       sprintf (convcmd, ps2png, (const char*) fFilename,
		       (const char*) fOutFilename) ;
	       break ;
            // ps2ai
            case TLGPrintParam::kAI: 
               sprintf (convcmd, ps2ai, (const char*) fOutFilename, 
                       (const char*) fFilename);
               break;
            // nothing
            default:
               strcpy (convcmd, "");
               break;
         }
         cout << "Convert to " << convcmd << endl;
         if (strlen (convcmd) > 0) {
            ret = (gSystem->Exec (convcmd) == 0);
            if (!ret) 
	    {
	    
	       cerr << "TLGPrint: " << convcmd << " command failed, return code " << ret << endl ;
	       fErrorCode = -5;
	    }
         }
      }
   
      // cleanup temp file
      if (fTempfileUse && (fOutFilename.Length() > 0)) {
         remove (fOutFilename);
         fOutFilename = "";
         fTempfileUse = kFALSE;
         cout << "remove temp " << fOutFilename << endl;
      }
      return ret;
   }


#ifndef __NO_NAMESPACE
}
#endif
