/* -*- mode: c++; c-basic-offset: 3; -*- */

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGMultiTab, TLGMultiTabElement, TLGMultiTabLayout                   //
//                                                                      //
// A tab widget contains a set of composite frames each with a little   //
// tab with a name (like a set of folders with tabs). This multi tab    //
// suuport multi line tabs				                //
//                                                                      //
// The TLGMultiTab is user callable. The TLGMultiTabElement and 	//
// TLGMultiTabLayout are a service classes of the tab widget.           //
//                                                                      //
// Clicking on a tab will bring the associated composite frame to the   //
// front and generate the following event:                              //
// kC_COMMAND, kCM_TAB, tab id, 0.                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


#include <time.h>
#include <iostream>
#include "TLGMultiTab.hh"
#include <TList.h>
#include <TMath.h>

namespace dttgui {
   using namespace std;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGMultiTabElement                                                   //
//                                                                      //
// Tab element for multi line tab widget                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGMultiTabElement::TLGMultiTabElement (const TGWindow *p, 
                     TGString *text, UInt_t w, UInt_t h,
                     GContext_t norm, FontStruct_t font,
                     UInt_t options, ULong_t back) :
   TGTabElement (p, text, w, h, norm, font, options, back)
   {
      fWAdjust = 0;
   }

//______________________________________________________________________________
   TLGMultiTabElement::~TLGMultiTabElement()
   {
   }

//______________________________________________________________________________
   Int_t TLGMultiTabElement::GetPreferredWidth() const
   {
      return TMath::Max(fTWidth+12, (UInt_t)45);
   }

//______________________________________________________________________________
   TGDimension TLGMultiTabElement::GetDefaultSize() const
   {
      return TGDimension(GetPreferredWidth()+fWAdjust, fTHeight+6);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGMultiTabLayout                                                    //
//                                                                      //
// Layout manager for multi line tab widget                             //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGMultiTabLayout::TLGMultiTabLayout (TLGMultiTab *main)
   {
   // Create a tab layout manager.
   
      fMain = main;
      fList = fMain->GetList();
   }

//______________________________________________________________________________
   void TLGMultiTabLayout::Layout()
   {
   // Layout the tab widget.
   
      UInt_t tabh = fMain->GetTabHeight();
      UInt_t tabnum = fMain->GetNumberOfTabs();
      UInt_t lines = fMain->GetTabLines();
      if (lines <= 0) lines = 1;
      Int_t tabperline = tabnum / lines;
      Int_t tabrest = tabnum % lines;
      UInt_t bw = fMain->GetBorderWidth();
      Int_t w = fMain->GetWidth();
      Int_t h = fMain->GetHeight();
      Int_t lineofs = fMain->GetLineOfCurrentTab() % lines;
   
      fMain->GetContainer()->MoveResize (0, tabh * lines, 
                           w, h - tabh * lines);
      Int_t xtab = 2;
      Int_t ytab = ((lineofs + lines - 1) % lines) * tabh;
      Int_t linecur = 0;
      Int_t lineindex = 0;
      Int_t linemaxindex = tabperline + (linecur >= tabrest ? 0 : 1);
      Int_t tabw = fMain->GetWidthOfTabs (0, linemaxindex);
      Int_t adjust = TMath::Abs (w - 2 - tabw) / linemaxindex;
      Int_t adjustrest = TMath::Abs (w - 2 - tabw) % linemaxindex;

   // first frame is the container, so take next...
      TGFrameElement *el, *elnxt;
      TIter next(fList);
      Int_t i = 0;
      next();   // skip first
      while ((el = (TGFrameElement*) next())) {
         elnxt = (TGFrameElement*) next();
         // adjust width of tab
         Bool_t lasttab = (lineindex >= tabperline) || 
            ((lineindex+1 == tabperline) && (linecur >= tabrest));
         Int_t ad = adjust + (lineindex >= adjustrest ? 0 : 1);
         if (w < tabw) {
            ad *= -1;
         }
         ((TLGMultiTabElement*)(el->fFrame))->SetWidthAdjust(ad);
         UInt_t tw = el->fFrame->GetDefaultWidth();

         // redraw tabs
         if (i == fMain->GetCurrent()) {
            el->fFrame->MoveResize(xtab-2, ytab, tw + (lasttab ? 2 : 3), 
                                 tabh+1);
            elnxt->fFrame->RaiseWindow();
            el->fFrame->RaiseWindow();
         } 
         else {
            el->fFrame->MoveResize(xtab, ytab+2, tw, tabh-1);
            el->fFrame->LowerWindow();
         }
         elnxt->fFrame->MoveResize(bw, tabh * lines + bw, w - (bw << 1), 
                              h - tabh*lines - (bw << 1));
         elnxt->fFrame->Layout();
         // increas tab index and recalulate parameters if on new line
         xtab += (Int_t)tw;
         i++;
         lineindex++;
         if (lasttab) {
            linecur++;
            lineindex = 0;
            xtab = 2;
            ytab = ((lineofs + lines - linecur - 1) % lines) * tabh;
            linemaxindex = tabperline + (linecur >= tabrest ? 0 : 1);
            tabw = fMain->GetWidthOfTabs (i, linemaxindex);
            // cout << "lay new tab el line " << linemaxindex << endl;
            if (linemaxindex == 0) {
               adjust = 0;
               adjustrest = 0;
            }
            else {
               adjust = TMath::Abs (w - 2 - tabw) / linemaxindex;
               adjustrest = TMath::Abs (w - 2 - tabw) % linemaxindex;
            }
         }
      }
   }

//______________________________________________________________________________
   TGDimension TLGMultiTabLayout::GetDefaultSize() const
   {
   // Get default size of tab widget.
   
      TGDimension dsize, dsize_te;
      TGDimension size(0,0), size_te(0,0);
   
      TGFrameElement *el, *elnxt;
      TIter next(fList);
      next();   // skip first container
      while ((el = (TGFrameElement *)next())) {
         dsize_te = el->fFrame->GetDefaultSize();
         dsize_te.fWidth = 
            ((TLGMultiTabElement*)(el->fFrame))->GetPreferredWidth();
         size_te.fWidth += dsize_te.fWidth;
         elnxt = (TGFrameElement *) next();
         dsize = elnxt->fFrame->GetDefaultSize();
         if (size.fWidth < dsize.fWidth) size.fWidth = dsize.fWidth;
         if (size.fHeight < dsize.fHeight) size.fHeight = dsize.fHeight;
      }
      if (fMain->GetTabLines() > 1) {
         size_te.fWidth = (UInt_t) (1.2 * size_te.fWidth / fMain->GetTabLines());
      }
   
   // check if tab elements make a larger width than the containers
      if (size.fWidth < size_te.fWidth) {
         size.fWidth = size_te.fWidth;
      }
   
      size.fWidth += fMain->GetBorderWidth() << 1;
      size.fHeight += fMain->GetTabLines() * fMain->GetTabHeight() + 
         (fMain->GetBorderWidth() << 1);
   
      return size;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGMultiTab                                                          //
//                                                                      //
// Tab widget with multi line tabs	                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGMultiTab::TLGMultiTab(const TGWindow *p, UInt_t w, UInt_t h,
                     Int_t tablines, GContext_t norm, FontStruct_t font,
                     UInt_t options, ULong_t back) :
   TGTab (p, w, h, norm, font, options, back), fLines (tablines)
   {
      SetLayoutManager (new TLGMultiTabLayout (this));
   }

//______________________________________________________________________________
   TLGMultiTab::~TLGMultiTab()
   {
   }

//______________________________________________________________________________
   TGCompositeFrame* TLGMultiTab::AddTab (TGString* text)
   {
      TGCompositeFrame* cf;
   
      AddFrame (new TLGMultiTabElement (this, text, 50, 20, fNormGC, 
                                 fFontStruct), 0);
      cf = new TGCompositeFrame (this, fWidth, fHeight-fLines*20-1);
      AddFrame (cf, 0);
#if ROOT_VERSION_CODE >= ROOT_VERSION(5,12,0)
      cf->SetEditDisabled(kEditDisableResize);
#endif
      return cf; 
   }

//______________________________________________________________________________
   TGCompositeFrame* TLGMultiTab::AddTab (const char* text)
   {
      return AddTab (new TGString (text)); 
   }

#if ROOT_VERSION_CODE >= ROOT_VERSION(5,18,0)
//______________________________________________________________________________
  void TLGMultiTab::AddTab (const char* text, TGCompositeFrame* cf)
   {
      AddTab (new TGString (text), cf); 
   }

//______________________________________________________________________________
  void TLGMultiTab::AddTab (TGString* text, TGCompositeFrame* cf)
   {
      AddFrame (new TLGMultiTabElement (this, text, 50, 20, fNormGC, 
                                 fFontStruct), 0);
      AddFrame (cf, 0);
      cf->SetEditDisabled(kEditDisableResize);
   }
#endif

//______________________________________________________________________________
   void TLGMultiTab::ChangeTab (Int_t tabIndex, Bool_t emit)
   {
   // Make tabIdx the current tab. Utility method called by SetTab and
   // HandleButton().
   
      if (tabIndex != fCurrent) {
         fCurrent = tabIndex;
         UInt_t tabh = GetTabHeight();
	 UInt_t tabnum = GetNumberOfTabs();
         UInt_t lines = GetTabLines();
         if (lines <= 0) lines = 1;
         Int_t tabperline = tabnum / lines;
         Int_t tabrest = tabnum % lines;
         UInt_t bw = GetBorderWidth();
         Int_t w = GetWidth();
         Int_t h = GetHeight();
         Int_t lineofs = GetLineOfCurrentTab() % lines;
      
         GetContainer()->MoveResize (0, tabh * lines, 
				     w, h - tabh * lines);
         Int_t xtab = 2;
         Int_t ytab = ((lineofs + lines - 1) % lines) * tabh;
         Int_t linecur = 0;
         Int_t lineindex = 0;
         Int_t linemaxindex = tabperline + (linecur >= tabrest ? 0 : 1);
         Int_t tabw = GetWidthOfTabs (0, linemaxindex);
         Int_t adjust = TMath::Abs (w - 2 - tabw) / linemaxindex;
         Int_t adjustrest = TMath::Abs (w - 2 - tabw) % linemaxindex;
      
      // first frame is the container, so take next...
         TGFrameElement *el, *elnxt;
         TIter next(fList);
         Int_t i = 0;
         next();   // skip first
         while ((el = (TGFrameElement *) next())) {
            elnxt = (TGFrameElement *) next();
         // adjust width of tab
            Bool_t lasttab = (lineindex >= tabperline) || 
               ((lineindex+1 == tabperline) && (linecur >= tabrest));
            Int_t ad = adjust + (lineindex >= adjustrest ? 0 : 1);
            if (w < tabw) {
               ad *= -1;
            }
            ((TLGMultiTabElement*)el->fFrame)->SetWidthAdjust(ad);
            UInt_t tw = el->fFrame->GetDefaultWidth();
         // redraw tabs
            if (i == GetCurrent()) {
               el->fFrame->MoveResize(xtab-2, ytab, tw+ (lasttab ? 2 : 3), 
                                    tabh+1);
               elnxt->fFrame->RaiseWindow();
               el->fFrame->RaiseWindow();
            } 
            else {
               el->fFrame->MoveResize(xtab, ytab+2, tw, tabh-1);
               el->fFrame->LowerWindow();
            }
            elnxt->fFrame->MoveResize(bw, tabh * lines + bw, w - (bw << 1), 
                                 h - tabh*lines - (bw << 1));
            elnxt->fFrame->Layout();
         // increas tab index and recalulate parameters if on new line
            xtab += (Int_t)tw;
            i++;
            lineindex++;
            if (lasttab) {
               linecur++;
               lineindex = 0;
               xtab = 2;
               ytab = ((lineofs + lines - linecur- 1) % lines) * tabh;
               linemaxindex = tabperline + (linecur >= tabrest ? 0 : 1);
               tabw = GetWidthOfTabs (i, linemaxindex);
               if (linemaxindex == 0) {
                  adjust = 0;
                  adjustrest = 0;
               }
               else {
                  adjust = TMath::Abs (w - 2 - tabw) / linemaxindex;
                  adjustrest = TMath::Abs (w - 2 - tabw) % linemaxindex;
               }
            }
         }
      
         // send message
	 if (emit) {
	   SendMessage(fMsgWindow, MK_MSG(kC_COMMAND, kCM_TAB), fCurrent, 0);
	   fClient->ProcessLine(fCommand, MK_MSG(kC_COMMAND, kCM_TAB), fCurrent, 0);
	   Selected(fCurrent);
	 }
      }
   }

//______________________________________________________________________________
#if ROOT_VERSION_CODE < ROOT_VERSION(5,14,0)       
   Bool_t TLGMultiTab::SetTab(Int_t tabIndex)
#else
     Bool_t TLGMultiTab::SetTab(Int_t tabIndex, Bool_t emit)
#endif
   {
   // ONLY here because ChangeTab is not virtual
   // Brings the composite frame with the index tabIndex to the
   // front and generate the following event if the front tab has
   // changed:
   // kC_COMMAND, kCM_TAB, tab id, 0.
   // Returns kFALSE if tabIndex is a not valid index
   
   // check if tabIndex is a valid index
      if (tabIndex < 0)
         return kFALSE;
   
   // count the tabs
      TIter next(fList);
      Int_t count = 0;
      while (next())
         count++;
   
      count = count / 2 - 1;
      if (tabIndex > count)
         return kFALSE;
   
   // change tab and generate event
#if ROOT_VERSION_CODE < ROOT_VERSION(5,14,0)       
      ChangeTab(tabIndex);
#else
      ChangeTab(tabIndex, emit);
#endif
   
      return kTRUE;
   }

//______________________________________________________________________________
#if ROOT_VERSION_CODE >= ROOT_VERSION(5,14,0)
Bool_t TLGMultiTab::SetTab(const char *name, Bool_t emit)
{
   // Brings the composite frame with the name to the
   // front and generate the following event if the front tab has changed:
   // kC_COMMAND, kCM_TAB, tab id, 0.
   // Returns kFALSE if tab with name does not exist.

   TGFrameElement *el;
   Int_t  count = 0;
   TGTabElement *tab = 0;

   TIter next(fList);
   next();           // skip first container

   while ((el = (TGFrameElement *) next())) {
      next();        // skip tab containter
      tab = (TGTabElement *)el->fFrame;

      if (*(tab->GetText()) == name) {
         // change tab and generate event
         ChangeTab(count, emit);
         return kTRUE;
      }
      count++;
   }

   return kFALSE;
}
#endif

//______________________________________________________________________________
#if ROOT_VERSION_CODE < ROOT_VERSION(4,4,0)
   Bool_t TLGMultiTab::HandleButton(Event_t *event)
   
   {
   // ONLY here because ChangeTab is not virtual
   // Handle button event in the tab widget. Basically we only handle
   // button events in the small tabs.
   
      if (event->fType == kButtonPress) {
         TGFrameElement *el;
         TIter next(fList);
      
         next();   // skip first container
      
         Int_t i = 0;
         Int_t c = fCurrent;
         while ((el = (TGFrameElement *) next())) {
            if (el->fFrame->GetId() == (Window_t)event->fUser[0])  // fUser[0] is child window
               c = i;
            next(); i++;
         }
      
      // change tab and generate event
         ChangeTab(c);
      }
      return kTRUE;
   }
#endif

//______________________________________________________________________________
   Int_t TLGMultiTab::GetWidthOfTabs (Int_t tabIndex, Int_t tabs) const
   {
   // Return width of tabs.
      Int_t w = 0;
      Int_t count = 0;
   
      TGFrameElement *el;
      TIter next(fList);
      next();           // skip first container
   
      while ((el = (TGFrameElement *) next())) {
         next();
         if ((count >= tabIndex) && (count < tabIndex + tabs)) {
            w += ((TLGMultiTabElement *) el->fFrame)->GetPreferredWidth();
         }
         count++;
      }
      return w;
   }

//______________________________________________________________________________
   Int_t TLGMultiTab::GetLineOfCurrentTab () const
   {
   // first frame is the container, so take next...
      TIter next(fList);
      Int_t i = 0;
      UInt_t lines = GetTabLines();
      if (lines <= 0) lines = 1;
      UInt_t tabnum = GetNumberOfTabs();
      Int_t tabperline = tabnum / lines;
      Int_t tabrest = tabnum % lines;
      Int_t linecur = 0;
      Int_t lineindex = 0;
   
      next();   // skip first
      while (next()) {
         next();
         if (i == GetCurrent()) {
            return linecur;
         } 
         i++;
         lineindex++;
         if ((lineindex > tabperline) || 
            ((lineindex == tabperline) && (linecur >= tabrest))) {
            linecur++;
            lineindex = 0;
         }
      
      }
   
      return 0;
   }


}
