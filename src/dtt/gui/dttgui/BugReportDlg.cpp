//
// Created by erik.vonreis on 11/25/19.
//
#include <memory>
#include "BugReportDlg.h"


namespace dttgui
{
    const int OPEN_IN_BROWSER_ID=1;
    const int CLOSE_ID=2;

    BugReportDlg::BugReportDlg(const TGWindow* p, const TGWindow* main, string report_url, string report_email) :
        TGTransientFrame(p, main, 10, 10),
        report_url(report_url),report_email(report_email)
    {
        Setup();

        MapSubwindows ();
        UInt_t width  = GetDefaultWidth();
        UInt_t height = GetDefaultHeight();
        Resize (width, height);
        Int_t ax;
        Int_t ay;
//        if (main) {
//            Window_t wdum;
//            gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
//                                             (((TGFrame*)main)->GetWidth() - fWidth) >> 1,
//                                             (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
//                                             ax, ay, wdum);
//        }
//        else {
//            UInt_t root_w, root_h;
//            gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay,
//                                      root_w, root_h);
//            ax = (root_w - fWidth) >> 1;
//            ay = (root_h - fHeight) >> 1;
//        }
//        Move (ax, ay);
//        SetWMPosition (ax, ay);

        // make the message box non-resizable
        SetWMSize(width, height);
        SetWMSizeHints(width, height, width, height, 0, 0);

        // set dialog box title
        SetWindowName ("Report a Bug");
        SetIconName ("Bug");
        SetClassHints ("BugReportDlg", "BugReportDlg");
        SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                    kMWMDecorMinimize | kMWMDecorMenu,
                    kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                    kMWMFuncMinimize,
                    kMWMInputModeless);

        MapWindow();
        fClient->WaitFor (this);
    }

    BugReportDlg::~BugReportDlg()
    {

    }

    void BugReportDlg::Setup()
    {
        //this->SetLayoutManager(new TGTableLayout(this, 9,2));
        //fMainGroup = unique_ptr<TGVerticalFrame>(new TGVerticalFrame(this));
        //AddFrame(fMainGroup.get(), new TGLayoutHints(kLHintsNormal, 2,2,2,2));
        fHeaderLabel = unique_ptr<TGLabel> (new TGLabel(this, "Please send us bug reports, ideas, or anything.  We love "
                                         "hearing from users.\n\nThe URL below will take you to a form that requires a "
                                         "LIGO.ORG account.\n\nTry the 'Open in browser' button, or copy and paste the "
                                         "URL into a browser"));
        this->AddFrame(fHeaderLabel.get(), new TGLayoutHints(kLHintsCenterX,5,5,5,5));


        fURLFrame = unique_ptr<TGHorizontalFrame>(new TGHorizontalFrame(this));


        fBugReportURL = unique_ptr<TGTextView>(new TGTextView(this, 500, 24, report_url.c_str()));
        this->AddFrame(fBugReportURL.get(), new TGLayoutHints(kLHintsCenterX,5,5,5,5));
        fOpenInBrowser = unique_ptr<TGTextButton>(new TGTextButton(this, new TGHotString("Open in browser"), OPEN_IN_BROWSER_ID));
        this->AddFrame(fOpenInBrowser.get(), new TGLayoutHints(kLHintsCenterX,5,5,5,5));

        fOpenBrowserStatus = unique_ptr<TGTextView>(new TGTextView(this, 500, 24, "Press 'Open in browser' button to open URL"));
        this->AddFrame(fOpenBrowserStatus.get(), new TGLayoutHints(kLHintsCenterX,5,5,5,5));

        fMiddleLabel = unique_ptr<TGLabel>(new TGLabel(this, "OR email your report the address shown below.\nJust copy and paste the address into your favorite "
                     "email program.\nNo LIGO.ORG account is required."));
        this->AddFrame(fMiddleLabel.get(), new TGLayoutHints(kLHintsCenterX,5,5,5,5));

        fEmailTextView = unique_ptr<TGTextView>(new TGTextView(this, 700, 24, report_email.c_str()));
        this->AddFrame(fEmailTextView.get(), new TGLayoutHints(kLHintsCenterX,5,5,5,5));

        fClose = unique_ptr<TGTextButton>(new TGTextButton(this, new TGHotString("Close"), CLOSE_ID));
        this->AddFrame(fClose.get(), new TGLayoutHints(kLHintsCenterX,5,5,5,5));

    }

    Bool_t BugReportDlg::ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2)
    {
        // Buttons
        if (    (GET_MSG (msg) == kC_COMMAND)
            &&  (GET_SUBMSG (msg) == kCM_BUTTON) )
        {
            switch (parm1)
            {
                case OPEN_IN_BROWSER_ID:
                    OpenInBrowser();
                    break;
                case CLOSE_ID:
                    this->CloseWindow();
                    break;
            }
        }
        return kTRUE;
    }

    void BugReportDlg::CloseWindow()
    {
        delete this;
    }

    void BugReportDlg::OpenInBrowser()
    {
        bool browser_opened = false;
#if __linux__
        string buf = string("sensible-browser") + " " + report_url;
        int exit_code = system(buf.c_str());
        if(!exit_code)
        {
            browser_opened = true;
        }
#else //system type.
        //just fail automatically.
#endif //system type
        if(browser_opened)
        {
            fOpenBrowserStatus->SetText(new TGText("Browser opened to new issue form."));
            fOpenBrowserStatus->SetForegroundColor(0x000000);
        }
        else
        {
            fOpenBrowserStatus->SetText(new TGText("Cannot automatically open a web browser on this system."));
            fOpenBrowserStatus->SetForegroundColor(0xff0000);
        }
        fOpenBrowserStatus->Update();
    }
}