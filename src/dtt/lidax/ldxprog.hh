/* version $Id: ldxprog.hh 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: ldxprog							*/
/*                                                         		*/
/* Module Description: Lidax progress dialogbox				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: lidax.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_LDXPROG_H
#define _LIGO_LDXPROG_H

#include "gmutex.hh"
#include "TLGFrame.hh"
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGButton.h>
#include <string>


namespace dttgui {
   class TLGProgressBar;
}

namespace lidax {


/** @name Lidax progress dialogbox 

    @memo Lidax progress dialogbox
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Lidax progress parameters.
    @memo Lidax progress parameters.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class LidaxProgress {
   protected:
      /// Mutex
      mutable thread::mutex	fMux;
      /// Completion (%)
      float		fCompletion;
      /// completion message
      std::string	fMsg;
      /// Cancel set (access is atomic!)
      bool		fCancel;
   
   public:
   
      /// Constructor
      LidaxProgress () : fCompletion (0.), fCancel (false) {
      }
      /// Set completion
      void SetCompletion (float c) {
         thread::semlock sem (fMux); fCompletion = c; }
      /// Get comlpetion
      float GetCompletion() const {
         thread::semlock sem (fMux); 
         return fCompletion; }
      /// Set completion message
      void SetMsg (const char* s) {
         thread::semlock sem (fMux); fMsg = s; }
      /// Get comlpetion message
      std::string GetMsg() const {
         thread::semlock sem (fMux); 
         return fMsg; }
   
      /// Get Cancel
      bool IsCancel() const {
         return fCancel; }
      /// Pointer to Cancel
      bool* Cancel() {
         return &fCancel; }
      /// Set Cancel
      void SetCancel (bool c = true) {
         fCancel = c; }
   };


/** Lidax progress dialog box.
    @memo Lidax progress dialog box.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGLidaxProgress : public dttgui::TLGTransientFrame {
   protected:
      /// Progress data
      LidaxProgress*	fProg;
      /// Old value
      float		fCompletion;
      /// Old message
      std::string	fMsg;
      /// Update timer
      TTimer*		fTimer;
   
      /// Layout hints
      TGLayoutHints*	fL[5];
      /// Frames
      TGCompositeFrame*	fF[2];
      /// Progress bar
      dttgui::TLGProgressBar*	fBar;
      /// Status message
      TGLabel*		fStatus;
      /// Abort button
      TGButton*		fAbortButton;
   
      /// Handle timer
      virtual Bool_t HandleTimer (TTimer* timer);
   
   public:
      /// Constructor
      TLGLidaxProgress (const TGWindow* p, const TGWindow* main,
                       LidaxProgress& prog);
      /// Destructor
      virtual ~TLGLidaxProgress ();
      /// Close window
      virtual void CloseWindow();
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };


//@}

}

#endif // _LIGO_LDXPROG_H



