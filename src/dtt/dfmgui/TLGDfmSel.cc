/* -*- mode: c++; c-basic-offset: 3; -*- */
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmSel							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TLGDfmSel.hh"
#include "TLGDfmServer.hh"
#include "TLGDfmUdn.hh"
#include "TLGDfmChannels.hh"
#include "TLGDfmTimes.hh"
#include "TLGEntry.hh"
#include <TGMsgBox.h>
#include <TVirtualX.h>
#include <time.h>
#include <algorithm>
#include <iostream>

static int my_debug = 0 ;

namespace dfm {
   using namespace std;
   using namespace dttgui;
   using namespace fantom;

   const int kMaxChnLength = 1024 * 1024;

   Cursor_t TLGDfmSelection::fWaitCursor = (Cursor_t)-1;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmLogin							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class TLGDfmLogin : public TLGTransientFrame {
   private:
      /// Return value
      Bool_t*		fOk;
      /// Return user name
      std::string*	fUserName;
      /// Return password
      std::string*	fPassword;
      /// Group frame
      TGCompositeFrame*	fG[1];
      /// lines
      TGCompositeFrame* fF[3];
      /// labels
      TGLabel*		fLabel[2];
      /// username/password
      TGTextEntry*	fEntry[2];
      /// buttons
      TGButton*		fButton[2];
      /// layout hints
      TGLayoutHints*	fL[5];
   
   public:
      /// Constructor
      TLGDfmLogin (const TGWindow* p, const TGWindow* main, 
                  const char* prompt, std::string& username, 
                  std::string& passwd, Bool_t& ret);
      /// Destructor
      virtual ~TLGDfmLogin();
      /// Close window
      virtual void CloseWindow();
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };

//______________________________________________________________________________
   TLGDfmLogin::TLGDfmLogin (const TGWindow* p, const TGWindow* main, 
                     const char* prompt, std::string& username, 
                     std::string& passwd, Bool_t& ret)
   : TLGTransientFrame (p, main, 10, 10), fOk (&ret),
   fUserName (&username), fPassword (&passwd)
   {
      // layout hints
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 8, 8, 2, 2);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 
                           2, 2, 2, 2);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsCenterY, 2, 2, 2, 2);
      fL[3] = new TGLayoutHints (kLHintsLeft | kLHintsCenterX |
                           kLHintsTop, 6, 6, 12, 4);
      fL[4] = new TGLayoutHints (kLHintsRight | kLHintsCenterY, 
                           2, 2, 2, 2);
      // frames
      fG[0] = new TGGroupFrame (this, "Credentials");
      AddFrame (fG[0], fL[0]);
      for (int i = 0; i < 2; i++) {
         fF[i] = new TGHorizontalFrame (fG[0], 10, 10);
         fG[0]->AddFrame (fF[i], fL[0]);
      }
      fF[2] = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fF[2], fL[0]);
      // line 1
      fLabel[0] = new TGLabel (fF[0], "User name:");
      fF[0]->AddFrame (fLabel[0], fL[1]);
      fEntry[0] = new TLGTextEntry (fF[0], username.c_str(), 11);
      fEntry[0]->Associate (this);
      fEntry[0]->Resize (150, fEntry[0]->GetHeight());
      fF[0]->AddFrame (fEntry[0], fL[4]);
      // line 2
      fLabel[1] = new TGLabel (fF[1], "Password:");
      fF[1]->AddFrame (fLabel[1], fL[1]);
      fEntry[1] = new TLGTextEntry (fF[1], "", 21);
      fEntry[1]->SetEchoMode (TGTextEntry::kPassword);
      fEntry[1]->Associate (this);
      fEntry[1]->Resize (150, fEntry[1]->GetHeight());
      fF[1]->AddFrame (fEntry[1], fL[4]);
      // Buttons
      fButton[0] = new TGTextButton (fF[2],
                           new TGHotString ("        &Ok        "), 1);
      fButton[0]->Associate (this);
      fF[2]->AddFrame (fButton[0], fL[3]);
      fButton[1] = new TGTextButton (fF[2], 
                           new TGHotString ("     &Cancel     "), 0);
      fButton[1]->Associate (this);
      fF[2]->AddFrame (fButton[1], fL[3]);
   
      // set dialog box title
      SetWindowName (prompt ? prompt : "Login");
      SetIconName (prompt ? prompt : "Login");
      SetClassHints ("DfmLoginDlg", "DfmLoginDlg");
   
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates 
            (main->GetId(), GetParent()->GetId(),
            (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
            (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
            ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGDfmLogin::~TLGDfmLogin()
   {
      for (int i = 0; i < 2; i++) {
         delete fLabel[i];
      }
      delete fEntry[0];
      delete fEntry[1];
      for (int i = 0; i < 2; i++) {
         delete fButton[i];
      }
      for (int i = 0; i < 3; i++) {
         delete fF[i];
      }
      delete fG[0];
      for (int i = 0; i < 5; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   void TLGDfmLogin::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGDfmLogin::ProcessMessage (Long_t msg, Long_t parm1, Long_t)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // Ok
            case 1:
               {
                  *fUserName = fEntry[0]->GetText();
                  *fPassword = fEntry[1]->GetText();
                  if (fOk) *fOk = kTRUE;
                  DeleteWindow();
                  break;
               }
            // Cancel
            case 0:
               {
                  if (fOk) *fOk = kFALSE;
                  DeleteWindow();
                  break;
               }
         }
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmLayout							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class TLGDfmLayout : public TGLayoutManager {
   public:
      TLGDfmLayout (TGCompositeFrame* p, Int_t lines, 
                   bool alternate = false) 
      : fMain (p), fLines (lines), fAlternate (alternate) {
         fList = fMain->GetList(); }
      virtual void Layout ();
      virtual TGDimension GetDefaultSize() const {
         if (fAlternate) {
            return TGDimension (2 * 545 + 20, 30 + 25 * fLines / 2); 
         }
         else {
            return TGDimension (545, 38 + 25 * fLines - 
                               (fLines == 2 ? 5 : 0)); 
         } }
   
   protected:
      TGCompositeFrame*	fMain;
      TList*		fList;
      Int_t 		fLines;
      Bool_t		fAlternate;
   };

//______________________________________________________________________________
   void TLGDfmLayout::Layout ()
   {
      if (fAlternate) {
         const Int_t x[] = {10, 85, 435};
         const Int_t w[] = {65, 330, 80};
         const Int_t y[] = {0, 25, 60, 85, 110, 135, 160};
         TGFrameElement* ptr;
         Int_t col = 0;
         Int_t row = 0;
         TIter next (fList);
         while ((ptr = (TGFrameElement*) next())) {
            Int_t ww = w[col];
            if (col == 0) {
               ww = ptr->fFrame->GetWidth();
            }
            if (row < 2) {
               ptr->fFrame->MoveResize (x[col], y[row]+20, ww, 22);
            }
            else {
               ptr->fFrame->MoveResize (550 + x[col], y[row-2]+20, ww, 22);
            }
            if (++col >= 3) {
               row++; col = 0;
            }
         }
      }
      else {
         const Int_t x[] = {10, 100, 450};
         const Int_t w[] = {80, 330, 80};
         const Int_t y[] = {0, 25, 60, 85, 110, 135, 160};
         TGFrameElement* ptr;
         Int_t col = 0;
         Int_t row = 0;
         TIter next (fList);
         while ((ptr = (TGFrameElement*) next())) {
            Int_t ww = w[col];
            if (col == 0) {
               ww = ptr->fFrame->GetWidth();
            }
            ptr->fFrame->MoveResize (x[col], y[row]+20, ww, 22);
            if (++col >= 3) {
               row++; col = 0;
            }
         }
      }
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmSelection						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGDfmSelection::TLGDfmSelection (const TGWindow* p, dataaccess& dacc, 
                     Bool_t sourcesel, const char* group, Int_t id,
                     Bool_t channelsel, Bool_t timeformatsel, 
                     Bool_t stagingsel)
   : TGGroupFrame (p, group ? group : 
                  (sourcesel ? "Source" : "Destination")), 
   TGWidget (id), fSourceSel (sourcesel), fOther (0), fDacc (&dacc), 
   fChannelsel (channelsel), fTimeFormatsel (timeformatsel), 
   fStagingsel (stagingsel)
   {
      // init
      fMultiServer = 0;
      fServer = 0;
      fSearchServer = 0;
      fUDN = 0;
      fUDNMore = 0;
      fChannels = 0;
      fChnSel = 0;
      fTimeGPS = 0;
      fTimeGPSN = 0;
      fTimeDate = 0;
      fTimeTime = 0;
      fTimeLookup = 0;
      fTimeNow = 0;
      fDuration = 0;
      fFLen = 0;
      fFNum = 0;
      fCType = 0;
      fFVers = 0;
      fKeep = 0;
      fStaging = 0;
      for (int i = 0; i < 15; i++) fLabel[i] = 0;
      for (int i = 0; i < 5; i++) fF[i] = 0;
      for (int i = 0; i < 4; i++) fL[i] = 0;
      if (fWaitCursor == (Cursor_t)-1) {
         fWaitCursor = gVirtualX->CreateCursor (kWatch);
      }
      if (!fSourceSel) fStagingsel = false;
      // layout manager
      int lines = 2;
      if (fChannelsel) lines++;
      if (fTimeFormatsel) lines += fSourceSel ? 3 : 1;
      if (fStagingsel) lines++;
      SetLayoutManager (new TLGDfmLayout (this, lines,
                                         group && fSourceSel && 
                                         !fTimeFormatsel && (lines == 4)));
      // layout hints
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsCenterY, 4, 4, 4, 4);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 
                           0, 0, 0, 0);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 
                           0, 4, 2, 2);
      fL[3] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsCenterY, 5, 0, 0, 0);
      // first line
      fLabel[0] = new TGLabel (this, 
                           fSourceSel ? "Server: " : "Client: ");
      AddFrame (fLabel[0], fL[1]);
      fF[4] = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fF[4], fL[1]);
      fMultiServer = new TGComboBox (fF[4], kDfmMultiServer);
      fMultiServer->Associate (this);
      fMultiServer->Resize (70, 23);
      fMultiServer->AddEntry ("single", 0);
      fMultiServer->AddEntry ("muliple", 1);
      fF[4]->AddFrame (fMultiServer, fL[1]);
      fServer = new TGComboBox (fF[4], kDfmServer);
      fServer->Associate (this);
      fServer->Resize (185, 23);
      fF[4]->AddFrame (fServer, fL[3]);
      fSearchServer = new TGTextButton (this, new TGHotString 
                           (" &Add... "), kDfmServerSearch);
      fSearchServer->Associate (this);
      AddFrame (fSearchServer, fL[1]);
      // second line
      fLabel[1] = new TGLabel (this, "UDN: ");
      AddFrame (fLabel[1], fL[1]);
      fUDN = new TGComboBox (this, kDfmUDN);
      fUDN->Associate (this);
      fUDN->Resize (260, 23);
      AddFrame (fUDN, fL[1]);
      fUDNMore = new TGTextButton (this, new TGHotString 
                           (" &More... "), kDfmUDNMore);
      fUDNMore->Associate (this);
      AddFrame (fUDNMore, fL[1]);
      // third line
      if (fChannelsel) {
         fLabel[2] = new TGLabel (this, "Channels: ");
         AddFrame (fLabel[2], fL[1]);
         fChannels = new TLGTextEntry (this, "", kDfmChannels);
         fChannels->Associate (this);
         fChannels->SetMaxLength (kMaxChnLength);
         AddFrame (fChannels, fL[1]);
         fChnSel = new TGTextButton (this, new TGHotString 
                              (" &Select... "), kDfmChnSel);
         fChnSel->Associate (this);
         AddFrame (fChnSel, fL[1]);
      }
      else {
         fLabel[2] = 0; fChannels = 0; fChnSel = 0;
      }
      // fourth line (time)
      if (fSourceSel && fTimeFormatsel) {
         // 4A
         fLabel[3] = new TGLabel (this, "Start GPS: ");
         AddFrame (fLabel[3], fL[1]);
         fF[0] = new TGHorizontalFrame (this, 10, 10);
         AddFrame (fF[0], fL[1]);
         fTimeGPS = new TLGNumericControlBox (fF[0], 0., 12, kDfmTimeGPS,
                              kNESInteger, kNEANonNegative);
         fTimeGPS->Associate (this);
         fF[0]->AddFrame (fTimeGPS, fL[2]);
         fLabel[9] = new TGLabel (fF[0], "sec        ");
         fF[0]->AddFrame (fLabel[9], fL[2]);
         fTimeGPSN = new TLGNumericControlBox (fF[0], 0., 12, kDfmTimeGPSN,
                              kNESInteger, kNEANonNegative, kNELLimitMinMax,
                              0, 999999999);
         fTimeGPSN->Associate (this);
         fF[0]->AddFrame (fTimeGPSN, fL[2]);
         fLabel[10] = new TGLabel (fF[0], "nsec");
         fF[0]->AddFrame (fLabel[10], fL[2]);
         fTimeLookup = new TGTextButton (this, new TGHotString 
                              (" &Lookup... "), kDfmTimeLookup);
         fTimeLookup->Associate (this);
         AddFrame (fTimeLookup, fL[1]);
         // 4B
         fLabel[6] = new TGLabel (this, "       UTC: ");
         AddFrame (fLabel[6], fL[1]);
         fF[1] = new TGHorizontalFrame (this, 10, 10);
         AddFrame (fF[1], fL[1]);
         fTimeDate = new TLGNumericControlBox (fF[1], 0., 12, kDfmTimeDate,
                              kNESDayMYear);
         fTimeDate->Associate (this);
         fF[1]->AddFrame (fTimeDate, fL[2]);
         fLabel[11] = new TGLabel (fF[1], "dd/mm/yy   ");
         fF[1]->AddFrame (fLabel[11], fL[2]);
         fTimeTime = new TLGNumericControlBox (fF[1], 0., 10, kDfmTimeTime,
                              kNESHourMinSec, kNEANonNegative);
         fTimeTime->Associate (this);
         fF[1]->AddFrame (fTimeTime, fL[2]);
         fLabel[12] = new TGLabel (fF[1], "hh:mm:ss");
         fF[1]->AddFrame (fLabel[12], fL[2]);
         fTimeNow = new TGTextButton (this, new TGHotString
                              (" &Now "), kDfmTimeNow);
         fTimeNow->Associate (this);
         AddFrame (fTimeNow, fL[1]);
      }
      // fourth line (format)
      if (!fSourceSel && fTimeFormatsel) {
         fLabel[3] = new TGLabel (this, "Format: ");
         AddFrame (fLabel[3], fL[1]);
         fF[0] = new TGHorizontalFrame (this, 10, 10);
         AddFrame (fF[0], fL[1]);
         fLabel[9] = new TGLabel (fF[0], "Len: ");
         fF[0]->AddFrame (fLabel[9], fL[2]);
         fFLen = new TLGNumericControlBox (fF[0], 0., 5, kDfmFormatLen,
                              kNESInteger, kNEAPositive);
         fFLen->Associate (this);
         fF[0]->AddFrame (fFLen, fL[2]);
         fLabel[10] = new TGLabel (fF[0], "   Num: ");
         fF[0]->AddFrame (fLabel[10], fL[2]);
         fFNum = new TLGNumericControlBox (fF[0], 0., 3, kDfmFormatNum,
                              kNESInteger, kNEAPositive);
         fFNum->Associate (this);
         fF[0]->AddFrame (fFNum, fL[2]);
         fLabel[6] = new TGLabel (fF[0], "   Compr.: ");
         fF[0]->AddFrame (fLabel[6], fL[2]);
         fCType = new TGComboBox (fF[0], kDfmFormatCompr);
         fCType->Associate (this);
         fCType->Resize (80, 23);
         fCType->AddEntry ("None", 0);
         fCType->AddEntry ("gzip", 1);
         fCType->AddEntry ("diff", 2);
         fCType->AddEntry ("gzip/diff", 3);
         fCType->AddEntry ("zero supp", 5);
         fCType->AddEntry ("zero/gzip", 6);
         fF[0]->AddFrame (fCType, fL[2]);
         fFVers = new TGComboBox (this, kDfmFormatType);
         fFVers->Associate (this);
         fFVers->Resize (50, 23);
         fFVers->AddEntry ("Vers. 4", 4);
         fFVers->AddEntry ("Vers. 6", 6);
         fFVers->Select (framefast::kDefaultFrameVersion);
         // CFType->AddEntry ("binary", 3);
         // CFType->AddEntry ("text", 4);
         AddFrame (fFVers, fL[1]); // Was fL[6] which is an error, there's only 4 of them.
      }
      // fifth line
      if (fSourceSel && fTimeFormatsel) {
         fLabel[4] = new TGLabel (this, "Duration: ");
         AddFrame (fLabel[4], fL[1]);
         fF[2] = new TGHorizontalFrame (this, 10, 10);
         AddFrame (fF[2], fL[1]);
         fDuration = new TLGNumericControlBox (fF[2], 0., 12, kDfmDuration,
                              kNESInteger, kNEANonNegative);
         fDuration->Associate (this);
         fF[2]->AddFrame (fDuration, fL[2]);
         fLabel[13] = new TGLabel (fF[2], "sec        ");
         fF[2]->AddFrame (fLabel[13], fL[2]);
         fDurationN = new TLGNumericControlBox (fF[2], 0., 12, kDfmDurationN,
                              kNESInteger, kNEANonNegative, kNELLimitMinMax,
                              0, 999999999);
         fDurationN->Associate (this);
         fF[2]->AddFrame (fDurationN, fL[2]);
         fLabel[8] = new TGLabel (fF[2], "nsec");
         fF[2]->AddFrame (fLabel[8], fL[2]);
         fLabel[7] = new TGLabel (this, "");
         AddFrame (fLabel[7], fL[1]);
      }
      // sixth line
      if (fStagingsel) {
         fLabel[5] = new TGLabel (this, "Keep: ");
         AddFrame (fLabel[5], fL[1]);
         fF[3] = new TGHorizontalFrame (this, 10, 10);
         AddFrame (fF[3], fL[1]);
         fKeep = new TLGNumericControlBox (fF[3], 0., 12, kDfmKeep,
                              kNESHourMin, kNEANonNegative);
         fKeep->Associate (this);
         fF[3]->AddFrame (fKeep, fL[2]);
         fLabel[14] = new TGLabel (fF[3], "hh:mm");
         fF[3]->AddFrame (fLabel[14], fL[2]);
         fStaging = new TGTextButton (this, new TGHotString
                              (" S&taging... "), kDfmStaging);
         fStaging->Associate (this);
         AddFrame (fStaging, fL[1]);
      }
      // set values
      fSel = fSourceSel ? fDacc->sel() : fDacc->dest();
      Build (-1);
   }

//______________________________________________________________________________
   TLGDfmSelection::~TLGDfmSelection ()
   {
      if (fMultiServer) delete fMultiServer;
      if (fServer) delete fServer;
      if (fSearchServer) delete fSearchServer;
      if (fUDN) delete fUDN;
      if (fUDNMore) delete fUDNMore;
      if (fChannels) delete fChannels;
      if (fChnSel) delete fChnSel;
      if (fTimeGPS) delete fTimeGPS;
      if (fTimeGPSN) delete fTimeGPSN;
      if (fTimeDate) delete fTimeDate;
      if (fTimeTime) delete fTimeTime;
      if (fTimeLookup) delete fTimeLookup;
      if (fTimeNow) delete fTimeNow;
      if (fDuration) delete fDuration;
      if (fDuration) delete fDurationN;
      if (fFLen) delete fFLen;
      if (fFNum) delete fFNum;
      if (fCType) delete fCType;
      if (fFVers) delete fFVers;
      if (fKeep) delete fKeep;
      if (fStaging) delete fStaging;
      for (int i = 0; i < 15; i++) 
         if (fLabel[i]) delete fLabel[i];
      for (int i = 0; i < 5; i++) 
         if (fF[i]) delete fF[i];
      for (int i = 0; i < 4; i++) 
         if (fL[i]) delete fL[i];
   }

//______________________________________________________________________________
   void TLGDfmSelection::SetSel (const selservers& sel)
   {
      fSel = sel;
   }

//______________________________________________________________________________
   void TLGDfmSelection::SetStartTime (const Time& T0)
   {
      time_t t = getUTC (T0);
      tm utc;
      gmtime_r (&t, &utc);
      utc.tm_year += 1900;
      utc.tm_mon++;
      fTimeGPS->SetIntNumber (T0.getS());
      fTimeGPSN->SetIntNumber (T0.getN());
      fTimeDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
      fTimeTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
      fUTCDirty = kFALSE;
   }

//______________________________________________________________________________
   void TLGDfmSelection::UpdateGPS ()
   {
      tm utc;
      memset (&utc, 0, sizeof (tm));
      fTimeDate->GetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
      fTimeTime->GetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
      utc.tm_year -= 1900;
      utc.tm_mon--;
      time_t t = mktime (&utc);
      Time start = fromUTC (t);
      t = 0;
      start -= mktime (gmtime_r (&t, &utc));
      fTimeGPS->SetIntNumber (start.getS());
      fUTCDirty = kFALSE;
   }

//______________________________________________________________________________
   void TLGDfmSelection::SetDuration (const Interval& Dt)
   {
      fDuration->SetIntNumber (Dt.GetS());
      fDurationN->SetIntNumber (Dt.GetN());
   }

//______________________________________________________________________________
   Bool_t TLGDfmSelection::UpdateChannels (bool read) 
   {
      if (my_debug) cerr << "TLGDfmSelection::UpdateChannels(read = " << (read ? "true" : "false") << ")" << endl ;
      if (!fChannelsel) {
         return kFALSE;
      }
      // get server entry
      selserverentry* entry = fSel.selectedEntry();
      if (entry == 0) {
         if (!read) {
            fChannels->SetText ("");
            fChannels->SetState (kTRUE);
         }
         return kFALSE;
      }
      else {
         // read if field is active
         if (read) {
            if (fChannels->IsEnabled()) {
               string chn = fChannels->GetText();
	       if (my_debug) cerr << "  fChannels->GetText() = " << chn << endl ;
               return entry->setChannels (chn.c_str());
            }
            else {
               return kTRUE;
            }
         }
         // write if not multiple UDN & not more than 30
         else {
            if ((entry->getUDN().size() > 1) ||
               (entry->channels().size() > 30)) {
               fChannels->SetText ("");
               fChannels->SetState (kFALSE);
            }
            else {
               fChannels->SetText (entry->getChannels().c_str());
	       if (my_debug) cerr << "  entry->getChannels() = " << entry->getChannels() << endl ;
               fChannels->SetState (kTRUE);
            }
         }
         return kTRUE;
      }
   }

//______________________________________________________________________________
   Bool_t TLGDfmSelection::UpdateFormat (bool read) 
   {
      if (fSourceSel || !fTimeFormatsel) {
         return kFALSE;
      }
      // get server entry
      selserverentry* entry = fSel.selectedEntry();
      if (entry == 0) {
         if (!read) {
            fFLen->SetIntNumber (1);
            fFNum->SetIntNumber (1);
            fCType->Select (0);
            fFVers->Select (framefast::kDefaultFrameVersion);
         }
         return kFALSE;
      }
      else {
         if (read) {
            int flen = fFLen->GetIntNumber();
            int fnum = fFNum->GetIntNumber();
            int compr = fCType->GetSelected();
            int vers = fFVers->GetSelected();
            entry->selectFormat (flen, fnum, compr, vers);
         }
         else {
            int flen, fnum, compr, vers;
            entry->selectedFormat (flen, fnum, compr, vers);
            fFLen->SetIntNumber (flen);
            fFNum->SetIntNumber (fnum);
            fCType->Select (compr);
            fFVers->Select (vers);
         }
         return kTRUE;
      }
   }

//______________________________________________________________________________
   void TLGDfmSelection::SetWait (bool set)
   {
      if (set) {
         gVirtualX->SetCursor (fId, fWaitCursor);
         gVirtualX->Update();
      }
      else {
         gVirtualX->SetCursor (fId, kNone);
      }
   }

//______________________________________________________________________________
   Bool_t TLGDfmSelection::ReadData (bool quite) 
   {
      // check server selection
      selserverlist servers;
      if (fSel.isMultiple()) {
         servers = fSel.selectedM();
      }
      else {
         if (!fSel.selectedServer().empty()) {
            servers.push_back (fSel.selectedS());
         }
      }
      if (servers.empty()) {
         if (!quite) {
            int retval;
            char msg[1024];
            sprintf (msg, "No %s selected", 
                    fSourceSel ? "server" : "client");
            new TGMsgBox (fClient->GetRoot(), this, "Error", msg, 
                         kMBIconStop, kMBOk, &retval);
         }
         return kFALSE;
      }
      bool serexist = true;
      bool udnexist = true;
      for (selserveriter i = servers.begin(); i != servers.end(); ++i) {
         dataserver* ds = fDacc->get ((const char*)i->getName());
         if (!ds) {
            serexist = false;
         }
         else if (i->getUDN().empty()) {
            udnexist = false;
         }
         else {
            for (const_UDNiter j = i->getUDN().begin(); 
                j != i->getUDN().end(); ++j) {
               if (!ds->get (j->first)) {
                  udnexist = false;
               }
            }
         }
      }
      if (!serexist) {
         if (!quite) {
            int retval;
            char msg[1024];
            sprintf (msg, "Invalid %s selected", 
                    fSourceSel ? "server" : "client");
            new TGMsgBox (fClient->GetRoot(), this, "Error", msg, 
                         kMBIconStop, kMBOk, &retval);
         }
         return kFALSE;
      }
      // check UDN selection
      if (!udnexist) {
         if (!quite) {
            int retval;
            char msg[1024];
            sprintf (msg, "Invalid or no UDN selected");
            new TGMsgBox (fClient->GetRoot(), this, "Error", msg, 
                         kMBIconStop, kMBOk, &retval);
         }
         return kFALSE;
      }
   
      // read channels
      if (fChannelsel) {
         if (!UpdateChannels()) {
            if (!quite) {
               int retval;
               new TGMsgBox (fClient->GetRoot(), this, "Error", 
                            "Invalid channel selection", 
                            kMBIconStop, kMBOk, &retval);
            }
            return kFALSE;
         }
      }
      // read times
      if (fSourceSel && fTimeFormatsel) {
         if (fUTCDirty) {
            UpdateGPS();
         }
         Time start = 
            Time (fTimeGPS->GetIntNumber(), fTimeGPSN->GetIntNumber());
         Interval duration (fDuration->GetIntNumber(),
                           fDurationN->GetIntNumber());
         if (!fSel.selectTime (start, duration, &fDacc->list())) {
            if (!quite) {
               int retval;
               new TGMsgBox (fClient->GetRoot(), this, "Error", 
                            "Selected time interval unavailable", 
                            kMBIconStop, kMBOk, &retval);
            }
            return kFALSE;
         }
      }
      // read format 
      if (!fSourceSel && fTimeFormatsel) {
         if (!UpdateFormat()) {
            if (!quite) {
               int retval;
               new TGMsgBox (fClient->GetRoot(), this, "Error", 
                            "Invalid format selection", 
                            kMBIconStop, kMBOk, &retval);
            }
            return kFALSE;
         }
      }
      // read staging
      if (fStagingsel) {
         Interval keep = fKeep->GetIntNumber() * 60;
         if (!fSel.selectStaging (keep)) {
            if (!quite) {
               int retval;
               new TGMsgBox (fClient->GetRoot(), this, "Error", 
                            "Invalid staging parameters", 
                            kMBIconStop, kMBOk, &retval);
            }
            return kFALSE;
         }
      }
      // set values in return
      if (fSourceSel) {
         fDacc->setSel (fSel);
      }
      else {
         fDacc->setDest (fSel);
      }
      return kTRUE;
   }

//______________________________________________________________________________
   void TLGDfmSelection::Build (Int_t level)
   {
      // levels:
      // -3 : clear and rebuild 
      // -2 : initialize completely 
      // -1 : initialize the first time around
      //  0 : rebuild server and UDN selection and adjust channels/format
      //  1 : rebuild UDN selection and adjust channels/format
      if (level == -3) {
         fSel.clear();
         fSel.setMultiple (false);
      }
      // init from scratch
      if (level <= 0) {
         if (level != -1) {
            fServer->RemoveEntries (0, 10000);
         }
         int selid = -1;
         int id = -1;
         // multi server
         if (fSel.isMultiple()) {
            fMultiServer->Select (1);
            for (selserveriter i = fSel.begin(); i != fSel.end(); ++i) {
               char buf[1024];
               sprintf (buf, "%s (%i)", (const char*)i->getName(), ++id);
               fServer->AddEntry (buf, id);
               if (i == fSel.selectedMServer()) selid = id;
            }
         
         }
         // single server
         else {
            fMultiServer->Select (0);
            dataservername sel = fSel.selectedS().getName();
            for (serveriter i = fDacc->begin(); i != fDacc->end(); ++i) {
               if ((fSourceSel && !i->second.supportInput()) ||
                  (!fSourceSel && !i->second.supportOutput())) {
                  continue;
               }
               fServer->AddEntry ((const char*)i->first, ++id);
               if (i->first == sel) selid = id;
            }
         }
         if (selid >= 0) {
            fServer->Select (selid);
         }
         else {
            fSel.selectServer ("");
            // clean top entry
            fServer->SetTopEntry 
               (new TGTextLBEntry (fServer, new TGString (""), 0), 
               new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                 kLHintsExpandX));
            fServer->MapSubwindows();
         }
         if (level == 0) {
            fServer->MapSubwindows();
            fServer->Layout();
         }
      }
      // init UDN selection
      if (level <= 1) {
         if (level != -1) {
            fUDN->RemoveEntries (0, 10000);
         }
         dataserver* ds = fDacc->get (fSel.selectedServer().c_str());
         if (ds) {
            ds->updateUDNs (false);
         }
         if (ds &&
            ((ds->getType() == st_File) || 
            (ds->getType() == st_Tape) ||
            (ds->getType() == st_SM))) {
            fUDN->AddEntry ("--new--", 0);
         }
         if (ds && ds->supportMultiUDN()) {
            fUDN->AddEntry ("--multiple--", 1);
            fUDNMore->SetState (kButtonUp);
         }
         else {
            fUDNMore->SetState (kButtonDisabled);
         }
         if (fStagingsel) {
            bool supp = (ds && ds->supportStaging());
            fStaging->SetState (supp ? kButtonUp : kButtonDisabled);
            fKeep->SetState (supp);
         }
         // get server entry
         selserverentry* entry = fSel.selectedEntry();
         // no sel
         if ((entry == 0) || (ds == 0)) {
            // clean top entry
            fUDN->SetTopEntry 
               (new TGTextLBEntry (fUDN, new TGString (""), 0), 
               new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                 kLHintsExpandX));
            fUDN->MapSubwindows();
         }
         // multiple sel
         else if (entry->getUDN().size() > 1) {
            fUDN->Select (1);
            // make sure all UDNs are up to date
            if (fSourceSel) {
               for (const_UDNiter i = entry->getUDN().begin(); 
                   i != entry->getUDN().end(); ++i) {
                  ds->lookupUDN (i->first);
               }
            }
         }
         // single sel
         else {
            int selid = -1;
            int id = 1;
            UDN cur = entry->getUDN().empty() ? 
               UDN ("") : entry->getUDN().begin()->first;
            for (UDNiter i = ds->begin(); i != ds->end(); ++i) {
               namerecord nr ((const char*)i->first);
               fUDN->AddEntry (nr.getName(), ++id);
               if (i->first == cur) selid = id;
            }
            if (selid >= 0) {
               fUDN->Select (selid);
            }
            else  {
               entry->setUDN (UDNList());
               // clean top entry
               fUDN->SetTopEntry 
                  (new TGTextLBEntry (fUDN, new TGString (""), 0), 
                  new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                    kLHintsExpandX));
               fUDN->MapSubwindows();
            }
            // make sure UDN is up to date
            if ((selid >= 0) && (strlen ((const char*)cur) > 0)) {
               ds->lookupUDN (cur);
            }
         }
         if (level >= 0) {
            fUDN->MapSubwindows();
            fUDN->Layout();
         }
      }
      // init channel selection
      if (level <= 0) {
         UpdateChannels (false);
         UpdateFormat (false);
      }
      // init rest
      if (level < 0) {
         if (fSourceSel) {
            // Time & duration
            if (fTimeFormatsel) {
               SetStartTime (fSel.selectedTime());
               SetDuration (fSel.selectedDuration());
            }
         }
         // keep staged
         if (fStagingsel) {
            fKeep->SetNumber ((double)fSel.selectedStaging()/60);
         }
      }
   }

//______________________________________________________________________________
   bool compUDNs (const UDNList::value_type& u1, 
                 const UDNList::value_type& u2) 
   {
      return (u1.first == u2.first);
   }

//______________________________________________________________________________
   Bool_t TLGDfmSelection::SelectServer (const TString& newserver)
   {
      if (fSel.isMultiple()) {
         return kFALSE;
      }
      selserverentry* entry = fSel.selectedEntry();
      if ((entry != 0) &&
         (dataservername ((const char*)newserver) == entry->getName())) {
         return kFALSE;
      }
      fSel.selectServer ((const char*)newserver, &fDacc->list());
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGDfmSelection::SelectServer (Int_t newserver)
   {
      if (!fSel.isMultiple() || (newserver < 0) || 
         (newserver >= (int)fSel.selectedM().size())) {
         return kFALSE;
      }
      selserveriter i = fSel.begin();
      advance (i, newserver);
      if (i == fSel.selectedMServer()) {
         return kFALSE;
      }
      fSel.selectMServer (i);
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGDfmSelection::SelectUDN (const UDNList& udn)
   {
      // get selected server
      selserverentry* entry = fSel.selectedEntry();
      dataserver* ds = 0;
      if (entry) ds = fDacc->get ((const char*)entry->getName());
      if (!ds) {
         // clean top entry
         fUDN->Select(-1);
         fUDN->SetTopEntry 
            (new TGTextLBEntry (fUDN, new TGString (""), 0), 
            new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                              kLHintsExpandX));
         fUDN->MapSubwindows();
         return kFALSE;
      }
      // check if 'new' UDN(s) are the same as previously
      if ((udn.size() == entry->getUDN().size()) &&
         equal (udn.begin(), udn.end(), entry->getUDN().begin(), 
               compUDNs)) {
         return kFALSE;
      }
      // lookup UDN(s) and check login
      SetWait();
      if (fSourceSel) {
         // check username/password
         bool login;
         string username;
         string password;
         const char* uname = 0;
         const char* pword = 0;
         do {
            login = true;
            for (const_UDNiter i = udn.begin(); i != udn.end(); ++i) {
               // Prompt for username/password if failed
               if (!ds->login (i->first, uname, pword)) {
                  Bool_t ret = kFALSE;
                  string loginprompt = "Login (";
                  loginprompt += ds->getName();
                  loginprompt += ")";
                  new TLGDfmLogin (gClient->GetRoot(), GetParent(),
                                  loginprompt.c_str(), 
                                  username, password, ret);
                  if (!ret) {
                     // clean top entry
                     fUDN->Select(-1);
                     fUDN->SetTopEntry 
                        (new TGTextLBEntry (fUDN, new TGString (""), 0), 
                        new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                          kLHintsExpandX));
                     fUDN->MapSubwindows();
                     return kFALSE;  // cancel
                  }
                  uname = username.c_str();
                  pword = password.c_str();
                  login = false;
                  break;
               }
            } 
         } while (!login);
         // lookup UDN(s)
         for (const_UDNiter i = udn.begin(); i != udn.end(); ++i) {
            ds->lookupUDN (i->first);
         }
      }
      // set new UDN(s)
      entry->setUDN (udn);
      SetWait (false);
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGDfmSelection::SelectUDN (const UDN& udn)
   {
      UDNList ulist;
      ulist.insert (UDNList::value_type (udn, UDNInfo()));
      return SelectUDN (ulist);
   }

//______________________________________________________________________________
   Bool_t TLGDfmSelection::AddServer ()
   {
      // server list
      UpdateChannels ();
      UpdateFormat ();
      TString name = fSel.selectedServer().c_str();
      selserverlist servers;
      if (fSel.isMultiple()) {
         servers = fSel.selectedM();
      }
      else {
         if (name.Length() != 0) {
            servers.push_back (fSel.selectedS());
         }
      }
      // call dialog box
      Bool_t ret;
      new TLGDfmServerDlg (gClient->GetRoot(), GetParent(),
                          *fDacc, fSourceSel, servers, name, ret);
      // add new server
      if (ret) {
         if (servers.size() > 1) {
            fSel.setMultiple (true);
            fSel.selectM (servers);
            SelectServer (0);
         }
         else {
            fSel.setMultiple (false);
            if (!servers.empty()) {
               fSel.selectS (servers[0]);
            }
         }
      }
      return ret;
   }

//______________________________________________________________________________
   Bool_t TLGDfmSelection::MultipleUDN ()
   {
      selserverentry* entry = fSel.selectedEntry();
      if (!entry) {
         return kFALSE;
      }
      dataserver* ds = fDacc->get ((const char*)entry->getName());
      if (!ds) {
         return kFALSE;
      }
      Bool_t ret;
      TLGDfmUDNDlg (gClient->GetRoot(), GetParent(), 
                   *ds, fSourceSel, entry->getUDN(), ret);
      return ret;
   }

//______________________________________________________________________________
   Bool_t TLGDfmSelection::SelectChannels ()
   {
      if (!fChannelsel) {
         return kFALSE;
      }
      selserverentry* entry = fSel.selectedEntry();
      if (!entry) {
         return kFALSE;
      }
      dataserver* ds = fDacc->get ((const char*)entry->getName());
      if (!ds) {
         return kFALSE;
      }
      Bool_t ret;
      UpdateChannels();
      // selected servers for obtaining input channels
      selservers* inpsel = &fSel;  // ok for  fSourceSel = true
      if (!fSourceSel) {
         if (fOther) {
            fOther->UpdateChannels();
            inpsel = &fOther->fSel;
         }
         else {
            inpsel = &fDacc->sel();
         }
      }
      // list of available channels
      channellist chnavail;
      if (fSourceSel || !inpsel->isMultiple()) {
         // available input channels = channels of all UDNs in sel. entry
         // available output channels = channels of all UDNs in sel. 
         //   input entry (if single selection)
         selserverentry* entry = inpsel->selectedEntry();
         dataserver* ds = 0;
         if (entry) {
            ds = fDacc->get ((const char*)entry->getName());
         }
         // go through UDN list
         if (ds) {
            channelquerylist q;
            if (!fSourceSel) {
               q = inpsel->selectedS().channels();
            }
            for (UDNiter i = entry->getUDN().begin(); 
                i != entry->getUDN().end(); ++i) {
               UDNInfo* f = ds->get (i->first);
               if (f != 0) {
                  FilterChannels (f->channels(), chnavail, 
                                 q.empty() ? 0 : &q);
               }
            }
         }
      }
      else {
         // available output channels = channels of all UDNs in 
         // all selected inputs (if multiple selection)
         for (selserveriter sel = inpsel->begin(); 
             sel != inpsel->end(); ++sel) {
            dataserver* ds = fDacc->get ((const char*)sel->getName());
            if (!ds) {
               continue;
            }
            // go through UDN list
            channelquerylist q (sel->channels());
            for (UDNiter i = sel->getUDN().begin();
                i != sel->getUDN().end(); ++i) {
               UDNInfo* f = ds->get (i->first);
               if (f != 0) {
                  FilterChannels (f->channels(), chnavail, 
                                 q.empty() ? 0 : &q);
               }
            }
         }
      }
      SortChannels (chnavail);
      new TLGDfmChnSelDlg (gClient->GetRoot(), GetParent(), *ds, 
                          *entry, chnavail, ret);
      if (ret) {
         UpdateChannels (false);
      }
      return ret;
   }

//______________________________________________________________________________     
   Bool_t TLGDfmSelection::GetChannelList (channellist& chnavail)     
   {
      if (my_debug) std::cerr << "TLGDfmSelection::GetChannelList()" << std::endl ;
      chnavail.clear();
      UpdateChannels();
      if (fSel.isMultiple()) {
	 if (my_debug) std::cerr << "  GetChannelList() - multiple selected dataservers" << std::endl ;
         for (selserveriter sel = fSel.begin(); 
             sel != fSel.end(); ++sel) {
            dataserver* ds = fDacc->get ((const char*)sel->getName());
            if (!ds) {
               continue;
	       if (my_debug) std::cerr << "   dataserever name = " << sel->getName() << std::endl ;
            }
            // go through UDN list
            channelquerylist q (sel->channels());
            for (UDNiter i = sel->getUDN().begin();
                i != sel->getUDN().end(); ++i) {
               UDNInfo* f = ds->get (i->first);
               if (f != 0) {
                  FilterChannels (f->channels(), chnavail, 
                                 q.empty() ? 0 : &q);
               }
            }
         }
      }
      else {
         // input entry (if single selection)
	 if (my_debug) std::cerr << "  GetChannelList() - single selected dataserver" << std::endl ;
         selserverentry* entry = fSel.selectedEntry();
         dataserver* ds = 0;
         if (entry) {
            ds = fDacc->get ((const char*)entry->getName());
	    if (my_debug) std::cerr << "   dataserever name = " << entry->getName() << std::endl ;
         }
         // go through UDN list
         if (ds) {
	    // fSel is a selservers class.  selectedS() returns a 
	    // selserverentry& which is fSel.fActiveS,
	    // selserverentry::channels() returns a channellist& which is 
	    // selserverentry.fChannels.channel
	    // A channellist is a vector<channelentry>
            channelquerylist q = fSel.selectedS().channels();
	    if (my_debug) cerr << "     The channelquerylist is constructed from " << fSel.selectedS().channels().size() << " channel entries" << endl ;
            for (UDNiter i = entry->getUDN().begin(); 
                i != entry->getUDN().end(); ++i) {
               UDNInfo* f = ds->get (i->first);
               if (f != 0) {
                  FilterChannels (f->channels(), chnavail, 
                                 q.empty() ? 0 : &q);
               }
            }
         }
      }
      if (my_debug) std::cerr << "TLGDfmSelection::GetChannelList() - return" << std::endl ;
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGDfmSelection::SelectTimes ()
   {
      if (!fSourceSel || !fTimeFormatsel) {
         return kFALSE;
      }
      selserverentry* entry = fSel.selectedEntry();
      if (!entry) {
         return kFALSE;
      }
      dataserver* ds = fDacc->get ((const char*)entry->getName());
      if (!ds) {
         return kFALSE;
      }
      Bool_t ret;
      if (fUTCDirty) {
         UpdateGPS();
      }
      Time start (fTimeGPS->GetIntNumber(), 
                 fTimeGPSN->GetIntNumber());
      Interval duration (fDuration->GetIntNumber(), 
                        fDurationN->GetIntNumber());
      new TLGDfmTimeSelDlg (gClient->GetRoot(), GetParent(), *ds, 
                           entry->getUDN(), start, duration, ret);
      if (ret) {
         SetStartTime (start);
         SetDuration (duration);
      }
      return ret;
   }

//______________________________________________________________________________
   Bool_t TLGDfmSelection::SetStaging ()
   {
      return kFALSE;
   }

//______________________________________________________________________________
   Bool_t TLGDfmSelection::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // Add server
            case kDfmServerSearch:
               {
                  // Add a new server
                  if (AddServer ()) {
                     SetWait ();
                     Build (0);
                     SetWait (false);
                  }
                  break;
               }
            // UDN More
            case kDfmUDNMore:
               {
                  // Select multiple UDNs
                  if (MultipleUDN ()) {
                     selserverentry* entry = fSel.selectedEntry();
                     if (entry) {
                        SelectUDN (entry->getUDN());
                     }
                     Build (1);
                  }
                  break;
               }
            // Channel select
            case kDfmChnSel:
               {
                  SetWait ();
                  SelectChannels ();
                  SetWait (false);
                  break;
               }
            // Time lookup
            case kDfmTimeLookup:
               {
                  SetWait ();
                  SelectTimes ();
                  SetWait (false);
                  break;
               }
            // Time now
            case kDfmTimeNow:
               {
                  Time now = Now();
                  now.setN (0);
                  SetStartTime (now);
                  break;
               }
            // Staging
            case kDfmStaging:
               {
                  SetStaging();
                  break;
               }
         }
      }
      // Combobox
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_COMBOBOX)) {
         switch (parm1) {
            // server single/multiple change
            case kDfmMultiServer:
               {
                  Bool_t mul = fMultiServer->GetSelected() != 0;
                  if (mul != fSel.isMultiple()) {
                     UpdateChannels ();
                     UpdateFormat ();
                     fSel.setMultiple (mul);
                     Build (0);
                  }
                  break;
               }
            // server change
            case kDfmServer:
               {
                  UpdateChannels ();
                  UpdateFormat ();
                  if (fSel.isMultiple()) {
                     if (SelectServer (fServer->GetSelected())) {
                        Build (1);
                     }
                  }
                  else {
                     TString newserver = 
                        ((TGTextLBEntry*)fServer->GetSelectedEntry())->
                        GetText()->GetString();
                     if (SelectServer (newserver)) {
                        Build (1);
                     }
                  }
                  UpdateChannels (false);
                  UpdateFormat (false);
                  break;
               }
            // UDN change
            case kDfmUDN:
               {
                  SetWait ();
                  int id = (int)fUDN->GetSelected();
                  // Select new UDNs
                  if (id == 0) { //(newudn == "--new--") {
                     UDN u;
                     selserverentry* entry = fSel.selectedEntry();
                     dataserver* ds = 0;
                     if (entry) {
                        ds = fDacc->get ((const char*)entry->getName());;
                     }
                     if (ds && TLGDfmUDNDlg::addUDN (this, *ds, 
                                          fSourceSel, &u)) {
                        SelectUDN (u);
                     }
                     Build (1);
                  }
                  // Select multiple UDNs
                  else if (id == 1) { // (newudn == "--multiple--") {
                     MultipleUDN ();
                     selserverentry* entry = fSel.selectedEntry();
                     if (entry) {
                        SelectUDN (entry->getUDN());
                     }
                     Build (1);
                  }
                  // Select another UDN
                  else {
                     id -= 2;
                     dataserver* ds = 
                        fDacc->get (fSel.selectedServer().c_str());
                     UDNiter i = ds->begin();
                     if ((id >= 0) && (id < ds->size())) {
                        advance (i, id);
                        if (SelectUDN (i->first)) {
                           // nothing
                        }
                     }
                  }
                  SetWait (false);
                  break;
               }
            // // frame type
            // case kDfmFormatType:
               // {
                  // switch (parm2) {
                     // case 0:
                     // default:
                        // {
                           // fFLen->SetIntNumber (32);
                           // fFNum->SetIntNumber (1);
                           // break;
                        // }
                     // case 1:
                        // {
                           // fFLen->SetIntNumber (3600);
                           // fFNum->SetIntNumber (1);
                           // break;
                        // }
                     // case 2:
                        // {
                           // fFLen->SetIntNumber (24*3600);
                           // fFNum->SetIntNumber (1);
                           // break;
                        // }
                  // }
                  // break;
               // }
         }
      }
      // Text entry updated
      if ((GET_MSG (msg) == kC_TEXTENTRY) &&
         (GET_SUBMSG (msg) == kTE_TEXTUPDATED)) {
         switch (parm1) {
            // GPS seconds have changed
            case kDfmTimeGPS:
               {
                  // set UTC start time
                  Time start = Time (fTimeGPS->GetIntNumber(), 0);
                  time_t t = getUTC (start);
                  tm utc;
                  gmtime_r (&t, &utc);
                  utc.tm_year += 1900;
                  utc.tm_mon++;
                  fTimeDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
                  fTimeTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
                  fUTCDirty = kFALSE;
                  break;
               }
            // GPS nano-seconds have changed
            case kDfmTimeGPSN:
               {
                  break;
               }
            // UTC time has changed
            case kDfmTimeDate:
            case kDfmTimeTime:
               {
                  // set GPS seconds
                  UpdateGPS ();
                  break;
               }
         }
      }
      // Text entry changed
      if ((GET_MSG (msg) == kC_TEXTENTRY) &&
         (GET_SUBMSG (msg) == kTE_TEXTCHANGED)) {
         switch (parm1) {
            // UTC time has changed
            case kDfmTimeDate:
            case kDfmTimeTime:
               {
                  fUTCDirty = kTRUE;
                  break;
               }
         }
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmDialog							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGDfmDialog::TLGDfmDialog (const TGWindow* p, const TGWindow* main,
                     dataaccess& dacc, Bool_t& ret, Bool_t channelsel,
                     Bool_t timesel, Bool_t stagingsel)
   : TLGTransientFrame (p, main, 10, 10), fOk (&ret)
   {
   
      // layout hints
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 8, 8, 8, 8);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 70, 70, 0, 0);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 8, 8, 8, 8);
      // Dfm selection
      fDfm = new TLGDfmSelection (this, dacc, true, "", 100, channelsel, 
                           timesel, stagingsel);
      fDfm->Associate (this);
      AddFrame (fDfm, new TGLayoutHints (kLHintsLeft | 
                                 kLHintsTop, 8, 8, 8, 8));
      // Buttons
      fButtonFrame = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fButtonFrame, fL[0]);
      fOkButton = new TGTextButton (fButtonFrame, 
                           new TGHotString ("     &Ok     "), 1);
      fOkButton->Associate (this);
      fButtonFrame->AddFrame (fOkButton, fL[1]);
      fCancelButton = new TGTextButton (fButtonFrame, 
                           new TGHotString ("   &Cancel   "), 0);
      fCancelButton->Associate (this);
      fButtonFrame->AddFrame (fCancelButton, fL[1]);
   
      // set dialog box title
      SetWindowName ("Data Selection");
      SetIconName ("Data Selection");
      SetClassHints ("DfmSelDlg", "DfmSelDlg");
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGDfmDialog::~TLGDfmDialog ()
   {
      delete fOkButton;
      delete fCancelButton;
      delete fButtonFrame;
      delete fDfm;
      for (int i = 0; i < 3; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   void TLGDfmDialog::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGDfmDialog::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // ok
            case 1:
               {
                  if (fDfm->ReadData()) {
                     // set return value and quit
                     if (fOk) *fOk = kTRUE;
                     DeleteWindow();
                  }
                  break;
               }
            // cancel
            case 0:
               {
                  // set return value and quit
		  if (fOk) *fOk = kFALSE;
                  DeleteWindow();
                  break;
               }
         }
      }
      return kTRUE;
   }

}
