
#include "dfmgui.hh"
#include "TLGDfmSel.hh"


namespace dfm {
   using namespace std;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dfmDlg							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool dfmDlg (const TGWindow* p, const TGWindow* main,
               dataaccess& dacc, bool channelsel,
               bool timesel, bool stagingsel)
   {
      Bool_t 		ret;
   
      // Show export dialog box
      new TLGDfmDialog (p, main, dacc, ret, channelsel, timesel, 
                       stagingsel);
      return ret;
   }


}
