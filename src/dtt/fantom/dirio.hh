/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: dirio							*/
/*                                                         		*/
/* Module Description: directory support for smartio			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 22Nov00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dirio.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_DIRIO_H
#define _LIGO_DIRIO_H

#include "iosupport.hh"
#include "framedir.hh"
#include <string>

namespace fantom {


/** @name Directory support
    This header defines support methods for reading from and writing to
    directories.
   
    @memo Directory support
    @author Written November 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Directory IO class. This is a support class for the smart dir input
    and smart dir output device classes.
    
    @memo Directory IO support class.
 ************************************************************************/
   class dir_support : public iosupport {
   public:
       /** Create directory support.
           \begin{verbatim}
           Directory format:
           dir://'dirname'[@startdir[.startfile]:stop[.stopfile]][#filenum]. 
           \end{verbatim}
           If any of the options are appended to the directory,
           auto-increment support is enabled. The start argument 
           describes the first directory number and optionally the 
           first file number. Similarly, the stop argument describes 
           the last directory number and optionally the last file 
           number. The file number argument is used to determine
           how many files should be created per directory.
   
           Example 1: "test.@3:5" represents the files in "test.3/", 
           "test.4/" and "test.5/".
   
           Example 2: "test.@4#3600" represents the directories
           "test.4/", "test.5/", etc., assuming no more than 3600
           files per directory.
   
           @param dirname Directory name
           @param conf Option argument (currently ignored)
         */
   
      explicit dir_support (const char* dirname = 0, 
                        const char* conf = 0) 
      : fCreateIfNeeded (true) {
         setDirname (dirname); }
      /// Set dir name
      void setDirname (const char* dirname = 0, const char* conf = 0);
      /// Get dir name
      const char* getDirname () const {
         return (fDirname.empty() ? 0 : fDirname.c_str()); }
      /// Get current directory name
      const char* getCurDir ();
   
      /// set create if needed flag
      void setCINFlag (bool cin = true) {
         fCreateIfNeeded = cin; }
      /// get the next existing filename
      bool getNextFilename (std::string& fname);
      /// set the next filename (for output)
      bool setNextFilename (std::string& fname, 
                        const std::string& guess);
   
      /// Read next frame into buffer (not supported)
      virtual framefast::basic_frame_storage* readFrame () {
         return 0; }
      /// Get frame writer (not supported)
      virtual framefast::basic_frameout* getWriter (const char* fname) {
         return 0; }
      /// End of file?
      virtual bool eof() const {
         return fDirInit ? fDirPos == fDir.end() : false; }
   
      /// Auto-increment?
      bool autoIncrement() const {
         return fAutoInc; }
      /// Directory stem
      std::string dirStem() const {
         return fDirStem; }
      /// Current dir number
      int curDir() const {
         return fCurDir; }
      /// Current file number
      int curFile() const {
         return fCurFile; }
      /// Last dir number
      int lastDir() const {
         return fLastDir; }
      /// Last file number
      int lastFile() const {
         return fLastFile; }
      /// Number of files per dir
      int fileNum() const {
         return fFileNum; }
   
      /// Clear dirio cache
      static void ClearCache();
   protected:
      /// initialize fdir
      void init (FrameDir& fdir) const;
   
      /// Create directory if needed
      bool		fCreateIfNeeded;
      /// Directory name (as specified)
      std::string	fDirname;
      /// Directory stem (for auto-increment)
      std::string	fDirStem;
      /// temporary directory name
      char		fTempdir[1024];
      /// Auto-increment enabled?
      bool		fAutoInc;
      /// Number of files per directory
      int		fFileNum;
      /// Current directory number
      int		fCurDir;
      /// Current auto-increment counter (file number)
      int		fCurFile;
      /// last directory number
      int		fLastDir;
      /// last file number
      int		fLastFile;
      /// List of frame files
      FrameDir 		fDir;
      /// frame file directory read in
      bool		fDirInit;
      /// Current position in frame dir
      FrameDir::file_iterator fDirPos;
      /// Last position in frame dir
      FrameDir::file_iterator fDirLast;
   };


//@}

}

#endif // _LIGO_DIRIO_H
