#!/bin/sh
#
# init parameters
#
device=0
mt=
load=0
unload=0
devnum=0
drive=0
#
# parse command line arguments
#
if [ $# -gt 0 ]; then
   while [ "$1" != "" ]; do
      case "$1" in
      -*) optarg=`echo "$2" | sed 's/-*//'` ;;
      *)  optarg= ;;
      esac
      if [ "${optarg}" != "" ]; then
         case "$1" in
         -d) device=${optarg} ;;
         -l) load=${optarg} ;;
         -u) unload=${optarg} ;;
	 -f) mt="${optarg}" ;;
         esac
         shift
      fi
      shift
   done
fi
#
# check arguments
#
if [ ${device} -ge 0 ]; then
   :
else 
   exit 1
fi
if [ ${load} -ge 0 ]; then
   :
else
   exit 1
fi
if [ ${unload} -ge 0 ]; then
   :
else
   exit 1
fi
if [ ${unload} -le 0 -a ${load} -le 0 ]; then
   exit 1
fi
if [ ${unload} -gt 0 -a -z "${mt}" ]; then
   echo "exit here"
   exit 1
fi
#
# set device 
#
devnum=`expr ${device} / 10`
drive=`expr ${device} % 10 + 1`
#
# set environment
#
CYRCS=${CYRCS:-"/usr/bin/cyrcs"}
export CYRCS
CYSTACKER="/dev/cychs${devnum}"
export CYSTACKER
#
# unload stacker command
#
if [ ${unload} -gt 0 ]; then
   echo "${CYRCS} -j ${CYSTACKER} -c eject ${mt}"
   if ${CYRCS} -j ${CYSTACKER} -c "eject ${mt}" >/dev/null 2>&1 ; then
      :
   else
      exit 1
   fi
   echo "${CYRCS} -j ${CYSTACKER} -c pos slot ${unload}"
   if ${CYRCS} -j ${CYSTACKER} -c "pos slot ${unload}" >/dev/null 2>&1 ; then
      :
   else
      exit 1
   fi
   echo "${CYRCS} -j ${CYSTACKER} -c pos drive ${drive}"
   if ${CYRCS} -j ${CYSTACKER} -c "pos drive ${drive}" >/dev/null 2>&1 ; then
      :
   else
      exit 1
   fi
   echo "${CYRCS} -j ${CYSTACKER} -c move drive ${drive} slot ${unload}"
   if ${CYRCS} -j ${CYSTACKER} -c "move drive ${drive} slot ${unload}" >/dev/null 2>&1 ; then
      :
   else
      exit 1
   fi
fi
#
# load stacker command
#
if [ ${load} -gt 0 ]; then
   echo "${CYRCS} -j ${CYSTACKER} -c pos slot ${load}"
   if ${CYRCS} -j ${CYSTACKER} -c "pos slot ${load}" >/dev/null 2>&1 ; then
      :
   else
      exit 1
   fi
   echo "${CYRCS} -j ${CYSTACKER} -c pos drive ${drive}"
   if ${CYRCS} -j ${CYSTACKER} -c "pos drive ${drive}" >/dev/null 2>&1 ; then
      :
   else
      exit 1
   fi
   echo "${CYRCS} -j ${CYSTACKER} -c move slot ${load} drive ${drive}"
   if ${CYRCS} -j ${CYSTACKER} -c "move slot ${load} drive ${drive}" >/dev/null 2>&1 ; then
      :
   else
      exit 1
   fi
fi
#
# done
#
exit 0
