SET(DIAG_SRC
    diagnames.c
    diagorg.cc
    diagtest.cc
    ffttools.cc
    repeat.cc
    sineresponse.cc
    stdsuper.cc
    stdtest.cc
    supervisory.cc
    sweptsine.cc
    testenv.cc
    testiter.cc
    testsync.cc
    timeseries.cc
    time_override.cpp
)


add_library(diag_lib OBJECT
    ${DIAG_SRC}
)

target_include_directories(diag_lib PRIVATE
    ${DTT_INCLUDES}
    ../../algo
)