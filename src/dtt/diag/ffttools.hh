/* version $Id: ffttools.hh 7690 2016-08-17 00:07:17Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: ffttools.h						*/
/*                                                         		*/
/* Module Description: FFT based diagnostics tests			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 11Jun99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: ffttools.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 5.0				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_FFTTOOLS_H
#define _GDS_FFTTOOLS_H

/* Header File List: */
#include "stdtest.hh"
#include "fftmodule.h"

namespace diag {


/** FFT Test
    This object implements the Fourier tests.
   
    @memo Object for implementing a FFT test
    @author Written June 1999 by Daniel Sigg
    @see Diagnostics test manual for used algorithms
    @version 0.1
 ************************************************************************/
   class ffttest : public stdtest {  
   public:
   
      /** Constructs a test object for measuring the fourier components.
          @memo Default constructor.
          @return void
       ******************************************************************/
      ffttest ();
   
      /** Destructs the fourier test object.
          @memo Destructor.
          @return void
       ******************************************************************/
      virtual ~ffttest ();
   
      /** Returns a new diagnostics test object describing a fourier
          test measurement.
          @memo Diagnostics test object creation function.
          @return new diagnotsics test object
       ******************************************************************/
      virtual diagtest* self () const;
   
      /** End of test. This function cleans up temporary storage.
          @memo Cleanup method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool end (std::ostringstream& errmsg);
   
   protected:
   
      /// frequency span stretch factor: 1.1
      static const double fftSpanFactor;
   
      /// temporary storage for intermediates results
      class tmpresult {
      public:
      	 /// static storage for a zero parameter object
         static const fftparam fftparamZero;
      
         /** Constructs a temporary object to store intermediate results.
             @memo Default constructor.
             @param Name channel name
             @param Size size of storage arrays
             @param Cmplx set true if time series was down-converted
             @return void
          ***************************************************************/
         explicit tmpresult (std::string Name, int Size = 0, 
                           bool Cmplx = false, 
                           const fftparam& param = fftparamZero);
      
         /** Destructs the temporary object.
             @memo Destructor.
             @return void
          ***************************************************************/
         ~tmpresult();
      
         /** Constructs a temporary object from another one.
             @memo Copy constructor.
             @param tmp temporary object
             @return void
          ***************************************************************/
         tmpresult (const tmpresult& tmp);
      
         /** Copies a temporary object from another one. Moves the 
             pointers rather than copy. It will set the pointers in
             the original object to zero.
             @memo Copy operator.
             @param tmp temporary object
             @return reference to object
          ***************************************************************/
         tmpresult&  operator= (const tmpresult& tmp);
      
         /** Allocates new storage. Deletes the old arrays first.
             @memo Allocate memory.
             @param Size size of storage arrays
             @return true if successful
          ***************************************************************/
         bool allocate (int Size = 0);
      
         /** True if valid temporary object.
             @memo Valid method.
             @return true if valid
          ***************************************************************/
         bool valid () const;
      
         /// read/write lock to protect object
         mutable thread::readwritelock	lock;
         /// channel name
         std::string	name;
         /// true if complex time series
         bool		cmplx;
         /// size of arrays, i.e. number of fft points
         int		size;
         /// fft result
         float*		fft;
         /// temporary array
         float*		x;
         /// fft parameter object
         fftparam	prm;
      };
      typedef std::vector<tmpresult> tmpresults;
   
      /// start frequency
      double		fStart;
      /// stop frequency
      double		fStop;
      /// bandwidth
      double		BW;
      /// Ramp down time
      double		rampDown ;
      /// Ramp up time
      double		rampUp ;
      /// overlap
      double		overlap;
      /// window type
      int		window;
      /// remove DC part?
      bool		removeDC;
      /// number of A channels
      int		AChannels;
      /// measurement time
      double		measTime;
      /// settling time
      double		settlingTime;
      // time to continue measureing after an excitation is stopped.
      // this allows the excitation to ring down, but for the measurement to capture the ringing.
      double    burstNoiseQuietTime_s;
      /// number of skipped measurement steps
      int		skipMeas;
   
      /// resolution bandwidth (window dependent)
      double 		windowBW;
      /// frequency span
      double		fSpan;
      /// Number of points used in the FFT
      int		fftPoints;
      /// Number of points in result
      int		points;
      /// highest frequency of interest
      double		fMaxMeas;
      /// lowest sampling frequency
      double		fMinSample;
      /// sampling frequency after 1st decimation
      double		fSample;
      /// zoom frequency
      double		fZoom;
      /// zoom frequency start time
      tainsec_t		fZoomStart;
      /// second decimation stage
      int		decimate2;
      /// total number of A channels (including stimuli)
      int		numA;
      /// measurement start time
      double 		mStart;
      /// t = 0 for excitation signals
      double 		exct0;
      /// time to add to mTime to set it to the next time grid
      double 		mTimeAdd;
      /// fft plan
      fftparam		fftPlan;
   
      /// temporary storage
      tmpresults	tmps;
   
      /** Read parameters. This function reads the parameters from the 
          storage object.
          @memo Read parameter method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool readParam (std::ostringstream& errmsg);
   
      /** Calcluate measurement time. This function calculates start 
   	  time and duration of each measuremenmt period.
          @memo Calculate measurement time method.
          @param errmsg error message stream
          @param t0 start time
          @return true if successful
       ******************************************************************/
      virtual bool calcTimes (std::ostringstream& errmsg,
                        tainsec_t& t0);

       /**
           *  Calculate the various durations values needed to schedule excitations and measurements for a burst signal
           *  Any argument can be passed as NULL if it's not needed.
           * @param time_meas_ns  the total time to take data.  Should be 1/BW.
           * @param time_pitch_ns  the time from the start of one measurement to the start of the next measurment
           * this time_meas_ns + skip-time, where skip-time is probably time_rampup_ns
           * @param time_rampup_ns rampup time for excitation before a measurement starts
           * @param time_rampdown_ns rampdown time
           * @param time_excitation_ns time of steady excitation from the end of ramp up to the start of ramp down.
           * @param time_quiet_ns  quiet time with no excitation, but measurement.
           * @return true if successful.  Fails if any calculated value is negative or if overlap is > 0. You
           * can't combine burst mode and overlap.
           * Output arguments thare are NULL are still calculated, so they can still generate an error even if you
           * don't see the reason.
           */
       bool calcBurstDurations (std::ostringstream& errmsg, tainsec_t *time_meas_ns, tainsec_t *time_pitch_ns, tainsec_t *time_rampup_ns,
                                tainsec_t *time_rampdown_ns, tainsec_t *time_excitation_ns,
                                tainsec_t *time_quiet_ns);

       /** Generates a  signal list which is described by the stimulus
        *  Use this function to generate a burst noise signal instead of calcSignal.
           object.
           @memo Generates signal list.
           @param t0 start time of excitation
           @param duration duration of excitation
       @param rampUp duration of excitation ramp up
       @param rampDown duration of excitation ramp down
           @return true if successful
        ***************************************************************/
       bool calcBurstSignal (tainsec_t t0, tainsec_t duration, tainsec_t rampUp=0, tainsec_t rampDown=0);

       /**
        * Calculate a new measurement when in burst noise mode.
        *
        * @param i measurement point index
        * @param measPoint  measurement index (channel?)
        * @param measure_ns length of measurement window
        * @param pitch_ns time delay from one point to the next
        * @param rampUp_ns ramp up time for excitations
        * @return true when successful.
        */
       bool newBurstMeasPoint(int i, int measPoint, tainsec_t measure_ns, tainsec_t pitch_ns, tainsec_t rampUp_ns);
   
      /** Calcluate a new measurement point. This function calculates 
          a new measurement interval, synchronization point and
          measurement partitions.
          @memo Calculate measurement point method.
          @param t0 start time (t = 0)
          @param t1 earliest time measurement can start
          @param i measurement point index
          @param measPoint measurement index
          @return true if successful
       ******************************************************************/
      virtual bool newMeasPoint (int i, int measPoint = 0);
   
      /** Calculates measurement parameters. This function determines 
          excitation signals, partitions for the rtdd and synchronization 
          points.
          @memo Add measurements method.
          @param errmsg error message stream
          @param t0 start time
          @param measPoint measurement point number
          @return true if successful
       ******************************************************************/
      virtual bool calcMeasurements (std::ostringstream& errmsg,
                        tainsec_t t0 = 0, int measPoint = 0);
   
      /** Stops the measurements. This function deletes the temporary
          storage space, then calls its base class function.
          @memo Stop measurements method. 
          @param firstIndex first data index to delete
          @return true if successful
       ******************************************************************/
      virtual bool stopMeasurements (int firstIndex = -1);
   
      /** Analysis routine which performs the analysis. This function 
          must be overwritten by descendents.
          @memo Analysis method.
          @param id callback argument describing the sync event
          @param measnum measurement number
          @param notify if true upon return sends a notification message
          @return true if successful
       ******************************************************************/
      virtual bool analyze (const callbackarg& id, int measnum, 
                        bool& notify);
   
      /** Calculates an FFT of the given channel.
          @memo FFT method
          @param resultnum number of result record
          @param measnum measurement number
          @param chnname channel name (including array indices)
          @param stim true if stimulus readback channel
          @param id callback argument describing the sync event
          @return true if successful
       ******************************************************************/
      bool fft (int resultnum, int measnum, std::string chnname, bool stim, 
               const callbackarg& id); 
   
      /** Calculates cross-correlation and coherence for a given channel.
          @memo Cross-correlation and coherence method
          @param resultnum number of result record
          @param measnum measurement number
          @param chnname channel name (including array indices)
          @param stim true if stimulus readback channel
          @param id callback argument describing the sync event
          @return true if successful
       ******************************************************************/
      bool cross (int resultnum, int measnum, std::string chnname, bool stim,
                 const callbackarg& id);   
   };

}
#endif // _GDS_FFTTOOLS_H
