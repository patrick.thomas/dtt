/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: timeseries						*/
/*                                                         		*/
/* Module Description: triggered time series measurement		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */

#include <time.h>
#include <unistd.h>
#include <cmath>
#include  <iostream>
#include "diagnames.h"
#include "timeseries.hh"
#include "awgfunc.h"
#include "gdsalgorithm.h"
#include "diagdatum.hh"
#include "IIRFilter.hh"
#include "FilterDesign.hh"
#include "time_override.h"

// JCB For debugging, print process and thread id.
char *printids()
{
   pid_t	pid ;
   pthread_t	tid ;
   static char 	str[256] ;

   pid = getpid() ;
   tid = pthread_self() ;
   sprintf(str, "pid = %u, tid = %lu", (unsigned int) pid, (unsigned long) tid) ;
   return str ;
}
   

namespace diag {
   using namespace std;
   using namespace thread;

   static const int my_debug = 0 ;

   // JCB - For debugging.
   // Initialize the static variable in the class to keep track of class instances.
   int timeseries::instance_count = 0 ;
   int timeseries::tmpresult::instance_count = 0 ;

   // maximum number of sampling points per (excitation) period
   const double _MAX_OVERSAMPLING = 4.0;
   // Time ahead buffers are allocated
   const double _TIME_AHEAD = 3.0; 

   timeseries::tmpresult::tmpresult (string Name, int Size, 
                     double Dx, bool Cmplx, string Filter) 
   : name (Name), cmplx (Cmplx), size (Size), x (0), xsqr (0), dx (Dx),
   filterspec (Filter), filter (0)
   {
      // JCB - for debugging
      myinstance = instance_count ; // JCB
      instance_count++ ; // JCB
      if (my_debug)
      { // JCB
	 char str[256] ;
	 sprintf(str, "construct tmpresult(%s, Size = %d, Dx = %f) instance %d, %s\n", Name.c_str(), Size, Dx, myinstance, printids()) ;
	 cerr << str ;
      }
      allocate (Size);
   }


   timeseries::tmpresult::~tmpresult()
   {
      allocate ();
      if (filter) delete filter;
      if (my_debug)
      { // JCB
	 char str[128] ;
	 sprintf(str, "timeseries::tmpresult %d deleted, %s\n", myinstance, printids()) ;
	 cerr << str ;
      }
   }


   bool timeseries::tmpresult::allocate (int Size)
   {
      if (my_debug)
      { // JCB
	 char str[128] ;
	 sprintf(str, "timeseries::tmpresult::allocate(%d) instance %d, %s\n", Size, myinstance, printids()) ; // JCB 
	 cerr << str ;
      }
      // delete old arrays
      if (xsqr != 0) {
         delete [] xsqr;
         xsqr = 0;
      }
      if (x != 0) {
         delete [] x;
         x = 0;
      }
      // alocate new ones
      size = Size;
      if (size == 0) {
         return true;
      }
      else {
         x = new (nothrow) float[cmplx ? 2 * size : size];
         xsqr = new (nothrow) float[cmplx ? 2 * size : size];
         return valid();
      }
   }


   bool timeseries::tmpresult::valid () const
   {
      return ((x != 0) && (xsqr != 0) && (dx > 0));
   }


   timeseries::tmpresult::tmpresult (const tmpresult& tmp) 
   : name (""), cmplx (false), size (0), x (0), xsqr (0), dx (1),
   filter (0) {
      *this = tmp;
      // JCB - for debugging
      myinstance = instance_count ; // JCB
      instance_count++ ; // JCB
      if (my_debug)
      { // JCB
	 char str[128] ;
	 sprintf(str, "tmpresult copy instance %d to instance %d, %s\n", tmp.myinstance, myinstance, printids()) ; 
	 cerr << str ;
      }
   }


   timeseries::tmpresult& timeseries::tmpresult::operator= (
                     const tmpresult& tmp)
   {
      if (this != &tmp) {
         name = tmp.name;
         size = tmp.size;
         cmplx = tmp.cmplx;
         x = tmp.x;
         xsqr = tmp.xsqr;
         dx = tmp.dx;
         const_cast <tmpresult&>(tmp).x = 0;
         const_cast <tmpresult&>(tmp).xsqr = 0;
         filterspec = tmp.filterspec;
         if (filter) delete filter;
         filter = 0;
      }
      return *this;
   }


   // timeseries constructor. 
   // The timeseriesname is a const char array "TimeSeries" defined in diagnames.h
   timeseries::timeseries () 
   : stdtest (timeseriesname) , measTime (0), preTriggerTime (0),
   settlingTime (0), sigBW (0)
   {
      // JCB - initialize the rest of the variables for the class.
      // These values don't necessarily make sense.
      deadTime = 0.0 ;
      includeStatistics = 0 ;
      fMaxMeas = 0.0 ;
      fMinSample = 0.0 ;
      fMaxSample = 0.0 ;
      fZoom = 0.0 ;
      mStart = 0.0 ;
      exct0 = 0.0 ;
      mTimeAdd = 0.0 ;
      dTime = 0.0 ;
      skipMeas = 0 ;
      myinstance = instance_count ; // JCB
      instance_count++ ; // JCB
      if (my_debug)
      { // JCB
	 char str[128] ;
	 sprintf(str, "timeseries %d created, %s\n", myinstance, printids()) ;
	 cerr << str ;
      }
   }

   // JCB
   timeseries::~timeseries(void)
   {
      if (my_debug)
      { // JCB
	 char str[128] ;
	 sprintf(str, "timeseries %d destroyed, %s\n", myinstance, printids()) ;
	 cerr << str ;
      }
      myinstance = -1 ;
   }


   diagtest* timeseries::self () const 
   {
      // JCB - is this correct?
      // return new (nothrow) timeseries ();
      return new (nothrow) timeseries ;
   }


   bool timeseries::end (ostringstream& errmsg)
   
   {
      semlock		lockit (mux);
      // delete temporary storage
      tmps.clear();
   
      return stdtest::end (errmsg);
   }


   bool timeseries::readParam (ostringstream& errmsg)
   {
      // call parent method
      if (!stdtest::readParam (errmsg)) {
         return false;
      }
   
      semlock		lockit (mux);
      bool		err = false;
   
      // read measurement time
      if (!test->getParam (*storage->Test,
                          tsMeasurementTime, measTime)) {
         errmsg << "Unable to load values from Test." <<
            tsMeasurementTime << endl;
         err = true;
      }
      // read pre trigger time
      if (!test->getParam (*storage->Test,
                          tsPreTriggerTime, preTriggerTime)) {
         errmsg << "Unable to load value from Test." <<
            tsPreTriggerTime << endl;
         err = true;
      }
      // read settling time
      if (!test->getParam (*storage->Test,
                          tsSettlingTime, settlingTime)) {
         errmsg << "Unable to load values from Test." <<
            tsSettlingTime << endl;
         err = true;
      }
      // Read ramp down time
      if (!test->getParam (*storage->Test, tsRampDown, rampDown)) {
         errmsg << "Unable to load value from Test." << tsRampDown << endl ;
         err = true ;
      }
      if (my_debug) cerr << "  rampDown parameter = " << rampDown << endl ;
      // Read ramp up time
      if (!test->getParam (*storage->Test, tsRampUp, rampUp)) {
         errmsg << "Unable to load value from Test." << tsRampUp << endl ;
         err = true ;
      }
      if (my_debug) cerr << "  rampUp parameter = " << rampUp << endl ;
      // read dead time
      if (!test->getParam (*storage->Test, tsDeadTime, deadTime)) {
         errmsg << "Unable to load values from Test." <<
            tsDeadTime << endl;
         err = true;
      }
      // read signal bandwidth
      if (!test->getParam (*storage->Test, tsBW, sigBW)) {
         errmsg << "Unable to load value from Test." << tsBW << endl;
         err = true;
      }
      // read whether to include statistics
      if (!test->getParam (*storage->Test, tsIncludeStatistics, 
                          includeStatistics)) {
         errmsg << "Unable to load value from Test." << 
            tsIncludeStatistics << endl;
         err = true;
      }
      // read filter design string
      if (!test->getParam (*storage->Test, tsFilter, filterSpec)) {
         filterSpec = "";
      }
   
      // read stimuli channel
      if (!readStimuliParam (errmsg, false, allWaveforms)) {
         err = true;
      }
   
      // read measurement channels
      if (!readMeasParam (errmsg)) {
         err = true;
      }
   
      // check heterodyne frequency
      fZoom = 0.0;
      if (!heterodyneFrequency (fZoom)) {
         errmsg << "Channels have inconsistent heterodyne frequency." << endl;
         err = true;
      }
      //cout << "ZOOM FREQUENCY IN TIME SERIES MEASUREMENT " << fZoom << endl;
   
      return !err;
   }


   bool timeseries::calcTimes (std::ostringstream& errmsg,
                     tainsec_t& t0)
   {
      semlock		lockit (mux);
      bool		err = false;
   
      if (my_debug) cerr << "imeseries::calcTimes(..., t0 = " << t0 << ")" << endl ;
      // check measurement time
      if (measTime <= 0) {
         errmsg << "Measurement time must be positive" << endl;
         return false;
      }
      // check settling time
      if (settlingTime < 0) {
         errmsg << "Settling time must be positive or zero" << endl;
         return false;
      }
      // check dead time
      if (deadTime >= 1) {
         errmsg << "Dead time must be smaller than one" << endl;
         return false;
      }
      // check pre-trigger time
      if (preTriggerTime >= 1) {
         errmsg << "Pre-trigger time must be smaller than one" << endl;
         return false;
      }
      // check averages
      if (averages < 1) {
         errmsg << "Number of averages must be at least one" << endl;
         err = true;
      }
      // check averages
      if (averages > 100000) {
         errmsg << "Number of averages must be smaller than 100000" << 
            endl;
         err = true;
      }
      // check average type
      if ((averageType < 0) || (averageType > 2)) {
         errmsg << "Illegal average type" << endl;
         err = true;
      }
      // check bandwidth
      if (sigBW <= 0) {
         errmsg << "Bandwidth must be positive" << endl;
         err = true;
      }
      // check filter
      FilterDesign ds (16384.);
      if (!ds.filter (filterSpec.c_str())) {
         errmsg << "Invalid filter" << endl;
         err = true;
      }
      if (err) {
         return false;
      }
      // Number of result records
      rnumber = 0;
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         if (iter->isReadback) {
            rnumber++;
         }
      }
      rnumber += meas.size();
   
      // highest frequency of interest
      fMaxMeas = sigBW;
      // determine sampling rates
      if (my_debug)
      {
	 cerr << "timeseries::calcTimes() calling samplingFrequencies()" << endl ;
         cerr << "   fMaxMeas   = " << fMaxMeas << endl ;
	 cerr << "   fMinSample = " << fMinSample << endl ;
	 cerr << "   fMaxSample = " << fMaxSample << endl ;
      }

      samplingFrequencies (fMaxMeas, fMinSample, fMaxSample);
      if (my_debug)
      {
	 cerr << "   LINE " << __LINE__ << endl ;
	 cerr << "   fMaxMeas   = " << fMaxMeas << endl ;
	 cerr << "   fMinSample = " << fMinSample << endl ;
	 cerr << "   fMaxSample = " << fMaxSample << endl ;
      }
   
      // calculate time grid
      timeGrid = calcTimeGrid (fMaxSample / 2.0, &t0);
   
      if (my_debug)
      {
	 cerr << "   fMaxMeas = " << fMaxMeas << endl ;
	 cerr << "   fMinSample = " << fMinSample << endl ;
	 cerr << "   fMaxSample = " << fMaxSample << endl ;
	 cerr << "   timeGrid = " << timeGrid << endl ;
      }

      // calculate measurement start time

      if (preTriggerTime >= 0) {
	 if (rampUp > (settlingTime * measTime)) {
	    // Use rampUp instead of settlingTime.
	    if (my_debug) cerr << "  use rampUp (" << rampUp << ") instead of settlingTime" << endl ;
	    mStart = adjustForSampling (rampUp, timeGrid) ;
	 }
	 else
	    // trigger is applied after measurement starts
	    mStart = adjustForSampling (settlingTime * measTime, timeGrid);
      }
      else {
	 if (rampUp > (settlingTime * measTime)) {
	    // Use rampUp instead of settlingTime.
	    if (my_debug) cerr << "  use rampUp (" << rampUp << ") instead of settlingTime (trigger)" << endl ;
	    mStart = adjustForSampling (rampUp - preTriggerTime * measTime, timeGrid);
	 }
	 else
	    // trigger is applied before measurement starts
	    mStart = adjustForSampling (settlingTime * measTime - 
                              preTriggerTime * measTime, timeGrid);
      }
      if (my_debug) cerr << "timeseries::calcTimes() - mStart = " << mStart << endl ;

      //exct0 = mStart + preTriggerTime * measTime;
      exct0 = 0 ;
      mTimeAdd = adjustForSampling (measTime, timeGrid) - measTime;
      if (my_debug) cerr << "timeseries::calcTimes() -  mTimeAdd = " << mTimeAdd << endl ;

      if (dTime >= 0) {
         dTime = adjustForSampling (deadTime, timeGrid);
      }
      else {
         dTime = - adjustForSampling (-deadTime, timeGrid);
      }
   
      // Use a value from a widget to set the ramp down time.
      testExc->setRampDown((tainsec_t)(rampDown * 1E9 + 0.5)) ;
      if (my_debug) cerr << "  rampDown time is " << rampDown * 1E9 << endl ;
      testExc->setRampUp((tainsec_t)(rampUp * 1E9 + 0.5)) ;
      if (my_debug) cerr << "  rampUp time is " << rampUp * 1E9 << endl ;

      // handle averaging
      avrgsize = averages;
      if (avrgsize * (measTime + mTimeAdd) < _TIME_AHEAD) {
         // need more buffers
         avrgsize = (int)(_TIME_AHEAD / (measTime + mTimeAdd));
      }
      // make sure we account for filter delay
      int onum = (int)(ceil ((measTime + mTimeAdd + 21./fMinSample) / 
                            (measTime + mTimeAdd + 1E-12)) + 0.1);
      if (onum > 1) avrgsize += onum;
      avrgsize += 1;
      // if (avrgsize < onum) {
         // avrgsize = onum;
      // }
      cout << "AVRGSIZE IS _________________" << avrgsize << endl;
   
      return true;
   }


   bool timeseries::newMeasPoint (int i, int measPoint)
   {
      semlock		lockit (mux);
   
      if (my_debug)
	 cerr << "timeseries::newMeasPoint(i = " << i << ", measPoint = " << measPoint << ")" << endl ;
      // calulate start time
      tainsec_t	start = T0 + (tainsec_t) ((mStart + (double) (i + skipMeas) * (measTime + mTimeAdd)) * 1E9 + 0.5);
   
      if (my_debug)
      {
	 cerr << "  start = " << start << endl ;
	 cerr << "    T0       = "<< T0 << endl ;
	 cerr << "    mStart   = " << mStart << endl ;
	 cerr << "    skipMeas = " << skipMeas << endl ;
	 cerr << "    i        = " << i << endl ;
	 cerr << "    measTime = " << measTime << endl ;
	 cerr << "    mTimeAdd = " << mTimeAdd << endl ;
      }
      // check if too far behind
      if (RTmode) 
      {
	 if (my_debug) cerr << "  RTmode = " << RTmode << endl ;
         tainsec_t now = gps_now_ns();
         if (start < now + _EPOCH) 
	 {
	    if (my_debug)
	       cerr << "    now = " << now << ", _EPOCH = " << _EPOCH << endl ;
            skipMeas = (int) (((double) (now + _EPOCH - T0) / 1E9 - mStart) /
                     (measTime + mTimeAdd) + 0.99) - i;
	    if (my_debug)
	       cerr << "    skipMeas = " << skipMeas << endl ;
            if (skipMeas < 0) {
               skipMeas = 0;
            }
            start = T0 + (tainsec_t)
               ((mStart + (double) (i + skipMeas) * 
                (measTime + mTimeAdd)) * 1E9 + 0.5);
	    if (my_debug)
	       cerr << "    start recalculate, = " << start << endl ;
         }
      }
      //cerr << "start " << start << " " << mStart << " " <<
       //  (measTime + mTimeAdd) << endl;
   
      // fine adjust to time grid & calc. duration
      start = fineAdjustForSampling (start, timeGrid);
      tainsec_t	duration = (tainsec_t) ((measTime + mTimeAdd - dTime) * 1E9 + 0.5);

      if (my_debug)
      {
	 cerr << "  start, duration after fineAdjustForSampling()" << endl ;
	 cerr << "    start = " << start << ", duration = " << duration << ", timeGrid = " << timeGrid << endl ;
      }
   
      // add interval
      intervals.push_back (interval (start, duration));
   
      // add new partitions
      if (!addMeasPartitions (intervals.back(), measPoint * averages + i,
                           fMaxSample, 0, fZoom)) {
         return false;
      }
   
      // add synchronization point
      if (!addSyncPoint (intervals.back(), i, measPoint)) {
         return false;
      }
   
      if (my_debug) cerr << "timeseries::newMeasPoint() return true" << endl ;
      return true;
   }


   bool timeseries::calcMeasurements (std::ostringstream& errmsg,
                     tainsec_t t0, int measPoint)
   {
      semlock		lockit (mux);
      tainsec_t		eStart;		// excitation start	
      tainsec_t		dur;		// excitation duration	
   
      // determine excitation signals
      if (my_debug) cerr << "timeseries::calcMeasurements(..., t0 = " << t0 << "< measPoint = " << measPoint << ")" << endl ;
      eStart = t0 + (tainsec_t) (exct0 * 1E9);
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) 
      {
         if ((iter->waveform == (AWG_WaveType) 10) ||
            (iter->waveform == (AWG_WaveType) 11)) 
	 {
            dur = (tainsec_t) ((measTime + mTimeAdd) * 1E9 + 0.5);
         }
         else {
            dur = -1;
         }
         double fold = iter->freq;
         if (iter->freq <= 0) {
            iter->freq = 1.0 / (measTime + mTimeAdd);
         }
         if (!iter->calcSignal (eStart, dur, rampUp*1E9, rampDown*1E9)) 
	 {
            errmsg << "Unable to calculate excitation signal" << endl;
            iter->freq = fold;
            return false;
         }
         iter->freq = fold;
      }
   
      // create initial measurement points
      skipMeas = 0;
      for (int i = 0; i < avrgsize; i++) {
         if (!newMeasPoint (i)) {
            errmsg << "Unable to create measurement points" << endl;
            return false;
         }
      }
      if (my_debug) cerr << "timeseries::calcMeasurements() return true" << endl ;
      return true;
   }


   bool timeseries::stopMeasurements (int firstIndex)
   {
      semlock		lockit (mux);
      // delete temporary storage
      tmps.clear();
   
      return stdtest::stopMeasurements (firstIndex);
   }


   bool timeseries::analyze (const callbackarg& id, int measnum, 
                     bool& note)
   {
      semlock		lockit (mux);
   
      /////////////////////////////////////////////////////////
      // Init analysis first time around		     //
      /////////////////////////////////////////////////////////
   
      // init analysis first time around
      if (measnum == 0) {
      
      /////////////////////////// Allocate temporary storage
	 if (my_debug)
	 { // JCB
	    char str[128] ;
	    sprintf(str, "timeseries(%d)::analyze - clearing tmps, %s\n", myinstance, printids()) ;
	    cerr << str ;
	 }
         tmps.clear();
         const diagResult& aChn = diagChn::self();
         int num = id.measPoint * averages + id.measPeriod;
         for (stimuluslist::iterator iter = stimuli.begin();
             iter != stimuli.end(); iter++) {
            if (iter->duplicate) {
               continue;
            }
            if (iter->isReadback) {
               // get time series
               string pname = diagObjectName::makeName (
                                    iter->readbackInfo.chName, step, num);
               gdsDataObject* chndat = storage->findData (pname);
               if (chndat == 0) {
                  return false;
               }
               int N;
               if (!aChn.getParam (*chndat, stTimeSeriesN, N)) {
                  return false;
               }
               double dt;
               if (!aChn.getParam (*chndat, stTimeSeriesdt, dt)) {
                  return false;
               }
               double f0 = 0.0;
               aChn.getParam (*chndat, stTimeSeriesf0, f0);
               // create temp object
	       if (my_debug)
	       { // JCB
		  char str[256] ;
		  sprintf(str, "timeseries(%d)::analyze - adding tmpresult to tmps %s, %d, %f..., %s\n", myinstance, iter->readback.c_str(), N, dt, printids()) ;
		  cerr << str ;
	       }
               tmps.push_back (tmpresult (iter->readback, N, dt, f0 != 0,
                                         filterSpec));
               if (!tmps.back().valid()) {
                  return false;
               }
            }
         }
         for (measlist::iterator iter = meas.begin();
             iter != meas.end(); iter++) 
	 {
            if (iter->duplicate) {
               continue;
            }
            // get time series
            string pname = diagObjectName::makeName (
                                 iter->info.chName, step, num);
            gdsDataObject* chndat = storage->findData (pname);
            if (chndat == 0) {
               return false;
            }
            int N;
            if (!aChn.getParam (*chndat, stTimeSeriesN, N)) {
               return false;
            }
            double dt;
            if (!aChn.getParam (*chndat, stTimeSeriesdt, dt)) {
               return false;
            }
            double f0 = 0.0;
            aChn.getParam (*chndat, stTimeSeriesf0, f0);
            // create temp object
	    if (my_debug)
	    { // JCB
	       char str[256] ;
	       sprintf(str, "timeseries(%d)::analyze - adding tmpresult to tmps %s, %d, %f... (2), %s\n", myinstance, iter->name.c_str(), N, dt, printids()) ;
	       cerr << str ;
	    }
            tmps.push_back (tmpresult (iter->name, N, dt, f0 != 0,
                                      filterSpec));
            if (!tmps.back().valid()) {
               return false;
            }
         }
      
	 /////////////////////////// Create index
         // get index access class
         const diagIndex& 	indx = diagIndex::self();
         // find index / create new index if necessary
         gdsDataObject* 	iobj = storage->findData (stIndex);
         if (iobj == 0) {
            iobj = indx.newObject (0);
            if (iobj == 0) {
               return false;
            }
            storage->addData (*iobj, false);
         }
      
         // write index 
         ostringstream		entry1;
         cerr << "ANALYZE TS 1" << endl;
         // time series entry
         int b = 0;
         for (tmpresults::iterator iter = tmps.begin();
             iter != tmps.end(); iter++, b++) {
               // set index entry
            diagIndex::channelEntry (entry1, b, iter->name);
            diagIndex::resultEntry (entry1, b, 0, iter->size, b);
         }
         indx.setEntry (*iobj, icTimeseries, step, entry1.str());
         cerr << "ANALYZE TS 2" << endl;
      }
   
      /////////////////////////////////////////////////////////
      // Calculate averages of all channels		     //
      /////////////////////////////////////////////////////////
   
      // analyze all measurement channels
      if (!callChannelAnalysis (id, measnum, 
                           (channelAnalysis) &timeseries::sum)) {
         return false;
      }
   
      note = true;
      cerr << "ANALYZE TS 3" << endl;
      return true;
   }


   bool timeseries::sum (int resultnum, int measnum, 
                     string chnname, bool stim, const callbackarg& id)
   {
      if (my_debug)
      {
	 char str[512];
	 sprintf(str, "timeseries::sum - analyize %s from %d into %d, %s\n", chnname.c_str(), measnum, resultnum, printids()) ;
	 cerr << str ;
      }
      //cerr << "analyze " << chnname << " from " << measnum << " into " << resultnum << endl;
   
      // get time series access class
      //const diagResult& aChn = diagChn::self();
      // get time series
      gdsDataObject* chndat = storage->findData (chnname);
      if (chndat == 0 && my_debug) 
      {
	 char str[128] ; // JCB
	 sprintf(str, "sum: analyze 1.5, %s\n", printids()) ; // JCB
	 cerr << str ; // JCB
// JCB         cerr << "analyze 1.5" << endl;
         return false;
      }
      // number of float values
      int N = tmps[resultnum].size;
      int num = tmps[resultnum].cmplx ? 2 * N : N;

      if (my_debug)
      { // JCB
	 char str[256] ;
	 sprintf(str, "timeseries(%d)::sum tmps[%d].size = %d, num = %d, %s\n", myinstance, resultnum, N, num, printids()) ;
	 cerr << str ; // JCB
      }
   
      // get result access class
      const diagResult* aRes = diagTimeSeries::self (stObjectTypeTimeSeries);
      if (aRes == 0 && my_debug) 
      {
	 char str[128] ; // JCB
	 sprintf(str, "analyze 1.6, %s\n", printids()) ; // JCB
	 cerr << str ; // JCB
// JCB         cerr << "analyze 1.6" << endl;
         return false;
      }
   
      // get result object
      string		resname = 
         diagObjectName::makeName (stResult, rindex + resultnum);
      gdsDataObject* 	res = storage->findData (resname);
      if (my_debug)
      { // JCB
	 char str[128] ;
	 sprintf(str, "analyze 1, %s\n", printids()) ;
	 cerr << str ;
      }
// JCB      cerr << "analyze 1" << endl;
   
      if (measnum == 0) {
         // init analysis result first time around
         if (res != 0) {
            storage->erase (resname);
         }
         int dim1 = includeStatistics ? 5 : N;
         int dim2 = includeStatistics ? N : 0;
         res = aRes->newObject (0, dim1, dim2, rindex + resultnum, -1,
                              tmps[resultnum].cmplx  ? gds_complex32: gds_float32);
         if (res != 0) {
            // set parameters of result
            string		name;
            int			i1, i2;
            if (!diagStorage::analyzeName (chnname, name, i1, i2)) {
               name = "";
            }
            aRes->clone (*res, chndat, false);
            res->setFlag (gdsDataObject::resultObj);
            aRes->setParam (*res, stTimeSeriesSubtype, 
                           tmps[resultnum].cmplx ? 1 : (includeStatistics ? 3 : 2));
            aRes->setParam (*res, stTimeSeriesAverageType, 
                           averageType);
            aRes->setParam (*res, stTimeSeriesAverages, averages);
            aRes->setParam (*res, stTimeSeriesf0, fZoom);
                           //1.0 / (measTime + mTimeAdd));
            aRes->setParam (*res, stTimeSeriesChannel, name);
            storage->addData (*res, false);
         }
      }
      if (res == 0) {
         cerr << "analyze 1.7" << endl;
         return false;
      }
      if (my_debug)
      { // JCB
	 char str[128] ;
	 sprintf(str, "analyze 2, %s\n", printids()) ;
	 cerr << str ;
      }
// JCB      cerr << "analyze 2" << endl;

      if (my_debug)
      { // JCB
	 char str[128] ;
	 sprintf(str, "sum: num = %d, %s\n", num, printids()) ;
	 cerr << str ;
      }
      // JCB - If num is 0, there's no memory allocated for the arrays, so leave.
      if (num == 0) {
	 if (my_debug)
	 {
	    char str[256] ;
	    sprintf(str, "timeseries(%d)::sum - no storage allocated for data. sum() returning false, %s.\n", myinstance, printids()) ;
	    cerr << str ;
	 }
	 return false ;
      }
   
      if (my_debug)
      { // JCB
	 char str[256] ;
	 sprintf(str, "About to copy data to tmps[%d] instance %d, %s.\n", resultnum, tmps[resultnum].instance(), printids()) ;
	 cerr << str ;
      }
      // get time series
      memcpy (tmps[resultnum].x, chndat->value, num * sizeof (float));
   
      // apply filter if non-complex
      if (!tmps[resultnum].cmplx) {
         if (!tmps[resultnum].filter && 
            !tmps[resultnum].filterspec.empty() &&
            (tmps[resultnum].dx > 0)) {
            FilterDesign ds (1. / tmps[resultnum].dx);
            if (ds.filter (tmps[resultnum].filterspec.c_str())) {
               tmps[resultnum].filter = ds.release();
               cerr << "analyze 3: filter with " << filterSpec << endl;
            }
         }
         if (tmps[resultnum].filter) {
            try {
               tmps[resultnum].filter->reset();
               TSeries inp (Time (0), tmps[resultnum].dx, 
                           tmps[resultnum].size, tmps[resultnum].x);
               TSeries out = tmps[resultnum].filter->apply (inp);
               if ((int)out.getNSample() != tmps[resultnum].size) {
                  throw std::runtime_error("filter failed");
               }
               out.getData (tmps[resultnum].size, tmps[resultnum].x);
            }
               catch (...) {
                  cerr << "Filtering of time series failed" << endl;
               }
         }
      }
   
      // calculate mean and store result
      avg_specs		avgprm;
      avgprm.avg_type = (averageType != 1) ? 
         AVG_LINEAR_VECTOR : AVG_EXPON_VECTOR;
      avgprm.dataset_length = num;
      avgprm.data_type = DATA_REAL;
      avgprm.number_of_averages = averages;
      int num_so_far = measnum;
      if (avg (&avgprm, 1, tmps[resultnum].x, &num_so_far, 
              (float*) res->value) < 0) {
         return false;
      }
   
      // compute statistical properties
      if (!tmps[resultnum].cmplx && includeStatistics) {
      
         // minimum
         float* x = tmps[resultnum].x;
         float* y = (float*) res->value + 2 * num;
         for (int i = 0; i < num; i++) {
            if ((measnum == 0) || (x[i] < y[i])) {
               y[i] = x[i];
            }
         }
         // maximum
         y = (float*) res->value + 3 * num;
         for (int i = 0; i < num; i++) {
            if ((measnum == 0) || (x[i] > y[i])) {
               y[i] = x[i];
            }
         }
         // rms
         float* xsqr = tmps[resultnum].xsqr;
         y = (float*) res->value + 4 * num;
         for (int i = 0; i < num; i++) {
            x[i] *= x[i];
         }
         num_so_far = measnum;
         if (avg (&avgprm, 1, x, &num_so_far, xsqr) < 0) {
            return false;
         }
         for (int i = 0; i < num; i++) {
            y[i] = sqrt (xsqr[i]);
         }
      
         // std. dev.
         y = (float*) res->value + 1 * num;
         x = (float*) res->value;
         for (int i = 0; i < num; i++) {
            if (measnum == 0) {
               y[i] = 0;
            }
            else {
               y[i] = sqrt ((measnum + 1) / measnum * 
                           (xsqr[i] - x[i] * x[i]));
            }
         }
      }
      aRes->setParam (*res, stTimeSeriesAverages, 
                     (averageType != 1) ? measnum + 1 : 
                     min (measnum + 1, averages));
   
      if (my_debug) cerr << "analyzed " << chnname << endl;
      return true;
   }


}
