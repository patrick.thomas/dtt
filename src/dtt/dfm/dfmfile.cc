#include "dfmfile.hh"
#include "framedir.hh"
#include "dirio.hh"
#include "fname.hh"
#include "fchannel.hh"
#include "framefast/framefast.hh"
#include "memmap/mmap.hh"
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <strings.h>
#include <iostream>
#include <cstring>
#include <string>
#include <vector>

namespace dfm {
   using namespace std;
   using namespace fantom;


   // scan stride for reading channel names from frames
   const int kChannelScan = 1000;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }

//______________________________________________________________________________
   static string findExt (const char* File)
   {
      const char* pdot = strrchr (File, '.');
      const char* pslash = strrchr (File, '/');
      if (pdot && (!pslash || pdot > pslash) && pdot[1] && 
         (strlen (pdot+1) < 5)) {
         return pdot + 1;
      }
      else {
         return "";
      }
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// isFrame (quick and dirty!) / readChannels			        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static bool isFrame (const char* File)
   {
      if (!File) {
         return false;
      }
      // Find number > 1E6 in the file name
      Time::ulong_t time=0;
      for (const char* ptr=File ; *ptr ; ) {
         if (*ptr >= '0' && *ptr <= '9') {
            time = strtol(ptr, (char**)&ptr, 0);
            if (time > 1000000) {
               return true;
            }
         } 
         else {
            ptr++;
         }
      }
      return false;
   }

//______________________________________________________________________________
   static bool readChannels (const string& fname, UDNInfo& uinfo)
   {
      // load the frame
      framefast::framereader fr;
      if (!fr.loadFile (fname.c_str())) {
         return false;
      }
      // get TOC
      const framefast::toc_t* toc = fr.getTOC();
      if (!toc) {
         return false;
      }
      double dur = (double)fr.duration();
      // loop over TOC
      for (int j = 0; j < 5; ++j) {
         for (int i = 0; i < (int)toc->fNData[j]; i++) {
            // check if already in list
            chniter chn = uinfo.findChn (toc->fData[j][i].fName);
            if (chn == uinfo.endChn()) {
               // if not add
               std::pair <chniter, bool> ins = 
                  uinfo.insertChn (toc->fData[j][i].fName);
               // get rate from ADC structure
               if (ins.second) {
                  framefast::data_t adcinfo;
                  if (fr.getData (adcinfo, toc->fData[j][i].fPosition[0], 
                                 (framefast::datatype_t)j,
                                 framefast::frvect_t::fv_nocopy)) {
                     if (adcinfo.fADC.fSampleRate > 0) {
                        ins.first->SetRate (adcinfo.fADC.fSampleRate);
                     }
                     else { // infer rate from array length
                        ins.first->SetRate (adcinfo.fVect.fNData / dur);
                     }
                  }
               }
               //cerr << "Channel = " << toc->fData[j][i].fName << endl;
            }
         }
      }
      return true;
   }

//______________________________________________________________________________
   static bool readChannelsTxt (const char* fname, UDNInfo& uinfo)
   {
      if (!fname) {
         return false;
      }
      gdsbase::mmap mm (fname, ios_base::in);
      if (!mm) {
         return false;
      }
      const char* p = (const char*) mm.get();
      // read file
      string n;
      n.reserve (1024);
      int errnum = 0;
      channellist chnlist;
      for (int i = 0; i < (int)mm.size() && errnum < 10; i++) {
         if (p[i] == '\n') {
            n = trim (n.c_str());
            if (!n.empty() && (n[0] != '#')) {
               // get channel name and rate
               if (fantom::String2Channels (chnlist, n.c_str()) && 
                  !chnlist.empty()) {
                  // check if already in list
                  chniter chn = uinfo.findChn (chnlist[0].Name());
                  if (chn == uinfo.endChn()) {
                     // if not add
                     uinfo.insertChn (chnlist[0].Name(), (int)chnlist[0].Rate());
                  }
               }
            }
            n = "";
         }
         else if (isprint (p[i])) {
            n += p[i];
         }
         else if ((p[i] & 0x7F) != 0) {
            errnum++;
         }
      }
      return true;
   }

//______________________________________________________________________________
   static bool checkChnFile (const char* fname, string& chnname)
   {
      if (!fname) {
         return false;
      }
      string s = fname;
      string::size_type pos = s.find (".udn");
      if (pos != string::npos) {
         s.replace (pos, 4, ".chn");
      }
      else {
         string ext = findExt (fname);
         if (!ext.empty()) {
            pos = s.find (ext);
         }
         if (pos != string::npos) {
            s.erase (pos);
            s += "chn";
         }
         else {
            s += ".chn";
         }
      }
      struct stat finfo;
      if (stat (s.c_str(), &finfo) == 0) {
         chnname = s;
         return true;
      }
      else {
         return false;
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dfmfile							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   dfmfile::dfmfile () 
   {
   }

//______________________________________________________________________________
   bool dfmfile::open (const std::string& addr, bool read)
   {
      fAddr = addr;
      return true;
   }

//______________________________________________________________________________
   void dfmfile::close()
   {
      fAddr = "";
   }

//______________________________________________________________________________
   bool dfmfile::requestUDNs (UDNList& udn)
   {
      //udn.clear();
      // get environment variable
      const char* udnfile = getenv (kFileUDN);
      if (!udnfile || !*udnfile) {
         return true;
      }
      // scan environment variable for config file names
      char* udef = new (nothrow) char [strlen (udnfile) + 10];
      strcpy (udef, udnfile);
      char* last;
      char* p = strtok_r (udef, ":", &last);
      while (p) {
         string fname = trim (p);
         p = strtok_r (0, ":", &last);
         gdsbase::mmap mm (fname.c_str(), ios_base::in);
         if (!mm) {
            continue;
         }
         // read config files
         string n;
         n.reserve (1024);
         int errnum = 0;
         const char* mp = (const char*)mm.get();
         for (int i = 0; i < (int)mm.size() && errnum < 10; i++) {
            if (mp[i] == '\n') {
               n = trim (n.c_str());
               if (!n.empty() && (n[0] != '#')) {
                  udn.insert (UDNList::value_type 
                             (UDN ((const char*) n.c_str()), UDNInfo()));
               }
               n = "";
            }
            else if (isprint (mp[i])) {
               n += mp[i];
            }
            else if ((mp[i] & 0x7F) != 0) {
               errnum++;
            }
         }
      }
      delete [] udef;
      return true;
   }

//______________________________________________________________________________
   bool dfmfile::requestUDNInfo (const UDN& udn, UDNInfo& info)
   {
      // add udn to name list
      namelist list;
      list.addName ((const char*)udn);
      FrameDir 		fdir;
      //fdir.setDebug (10);
      frametype utype = NONE;
      vector<string> chnnamelist;
      bool err = false;
      // name lookup
      cerr << "requestUDNInfo: begin" << endl;
      while (!list.empty()) {
         namerecord* cur = list.removeName();
         if (!cur) {
            continue;
         }
         string name = cur->getName();
         bool wildcard = (name.find_first_of ("*[?") != string::npos);
         // file without wildcard
         if ((cur->getDevType() == dev_file) && !wildcard) {
           // check if frame file
            if (isFrame (cur->getName())) {
               filenamerecord* frec = dynamic_cast<filenamerecord*>(cur);
               int cont = frec ? frec->getCont() : 0;
               fdir.addFile (cur->getName(), cont);
               if (utype == NONE) {
                  utype = FF;
                  // string ext = findExt (cur->getName());
                  // if (strcasecmp (ext.c_str(), "T") == 0) {
                     // utype = STF;
                  // }
                  // else if (strcasecmp (ext.c_str(), "mT") == 0) {
                     // utype = MTF;
                  // }
                  // else {
                     // utype = FF;
                  // }
               }
            }
            // otherwise read as text file
            else {
               cerr << "requestUDNInfo: add list " << cur->getName() << endl;
               gdsbase::mmap mm (cur->getName(), ios_base::in);
               if (mm) {
                  list.addNameList ((const char*)mm.get(), mm.size(), false);
               }
               // framefast::mmap_frame_storage mm;
               // if (mm.map (cur->getName())) {
                  // list.addNameList (mm.data(), mm.size(), false);
               // }
               // check for channel list file
               string chnname;
               if (checkChnFile (cur->getName(), chnname)) {
                  chnnamelist.push_back (chnname);
               }
            }
         }
         // file with wildcard
         else if (((cur->getDevType() == dev_file) && wildcard)) {
            list.addFiles (cur->getName(), false);
         }
         // directory
         else if (cur->getDevType() == dev_dir) {
            // add all files from dir or file wildcard
            fantom::dir_support d (cur->getName(), cur->getConf());
            std::string fname;
            do {
               if (d.getNextFilename (fname)) {
                  fdir.addFile (fname.c_str());
               }
            } while (!d.eof());
            if (utype == NONE) {
               utype = FF;
            }
         // 	    vector<string> l;
         //             do {
         //                std::string fname;
         //                if (d.getNextFilename (fname)) {
         //                   l.push_back (fname);
         //                }
         //             } while (!d.eof());
         //             for (vector<string>::reverse_iterator i = l.rbegin();
         //                 i != l.rend(); ++i) {
         //                string s ("file://");
         //                s += *i;
         //                list.addName (s.c_str(), false);
         //             }
         }
         else {
            err = true;
         }
         delete cur;
      }
   
      // build UDN info (time segments)
      cerr << "requestUDNInfo: time list" << endl;
      UDNInfo uinfo;
      uinfo.setType (utype);
      FrameDir::gps_t time = 0;
      FrameDir::gps_t timebeg = 0;
      for (FrameDir::series_iterator i = fdir.beginSeries(); 
          i != fdir.endSeries(); ++i) {
         if (time == 0) {
            time = i->first;
            timebeg = time;
         }
         // check for gap
         if (time != i->second.getStartGPS()) {
            Interval Delta = Time(time) - Time(timebeg);
            uinfo.insertDSeg (Time(timebeg), Delta);
            time = i->first;
            timebeg = time;
         }
         time += i->second.getLength().GetS();
      }
      // don't forget last segment
      if (time != 0) {
         Interval Delta = Time(time) - Time(timebeg);
         uinfo.insertDSeg (Time(timebeg), Delta);
      }
      // build UDN info (channel list);
      if (chnnamelist.empty()) {
         cerr << "requestUDNInfo: channel names from frame" << endl;
         // derived from 1st, last and every 1000 frame
         if (!err && (fdir.begin() != fdir.end())) {
            FrameDir::file_iterator i = fdir.end();
            --i;
            readChannels (i->getFile(), uinfo);
            i = fdir.begin();
            readChannels (i->getFile(), uinfo);
            int num = fdir.size();
            while (num > kChannelScan) {
               advance (i, kChannelScan);
               readChannels (i->getFile(), uinfo);
               num -= kChannelScan;
            }
         }
      }
      else {
         cerr << "requestUDNInfo: channel names from list" << endl;
         // read channels from file
         for (vector<string>::iterator i = chnnamelist.begin(); 
             i != chnnamelist.end(); ++i) {
            readChannelsTxt (i->c_str(), uinfo);
         }
      }
      // return
      if (!err) info = uinfo;
      cerr << "requestUDNInfo: end" << endl;
      return !err;
   }

//______________________________________________________________________________
   void dfmfile::ClearCache()
   {
      dir_support::ClearCache();
   }

}
