/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "dfmsends.hh"
#include "fchannel.hh"
#include "sendsio.hh"
#include <iostream>
#include <sstream>
#include <cstring>
#include <cstdlib>

#define SENDS_UDNPFX_STR "nds2://"
#define SENDS_UDNPFX_LEN 7

namespace dfm {
   using namespace std;
   using namespace fantom;


   // 5s timeout
   static const int _TIMEOUT = 5000000;	
   // Default NDS port
   static const int kSENDSPORT = 31200;

   static int my_debug = 0 ;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dfmsends							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   dfmsends::dfmsends () : fPort (0)
   {
      fChnPreselFull = true;
   }

//______________________________________________________________________________
   bool dfmsends::open (const std::string& addr, bool read)
   {
      fAddr = addr;
      if (my_debug) cerr << "dfmsends::open(" << addr << ", ...)" << endl ;
      string::size_type pos = fAddr.find (":");
      if (pos != string::npos) {
         fServer = fAddr.substr (0, pos);
         fPort = atoi (fAddr.c_str() + pos + 1);
      }
      else {
         fServer = fAddr;
         fPort = kSENDSPORT;
      }
      for (string::iterator i = fServer.begin(); 
          i != fServer.end(); ++i) {
         *i = tolower (*i);
      }
      while (!fServer.empty() && isspace (fServer[0])) {
         fServer.erase (0, 1);
      }
      while (!fServer.empty() && 
            isspace (fServer[fServer.size()-1])) {
         fServer.erase (fServer.size() - 1);
      }
      return true;
   }

//______________________________________________________________________________
   void dfmsends::close()
   {
      fAddr = "";
   }

//______________________________________________________________________________
   bool dfmsends::requestUDNs (UDNList& udn)
   {
      ostringstream buf;
      buf << SENDS_UDNPFX_STR << fServer;
      if (fPort != kSENDSPORT) {
         buf << ":" << fPort;
      }
      //udn.clear();
      string s = buf.str() + "/frames";
      udn.insert (UDNList::value_type (UDN (s.c_str()), UDNInfo()));
      s = buf.str() + "/trend";
      udn.insert (UDNList::value_type (UDN (s.c_str()), UDNInfo()));
      s = buf.str() + "/minute-trend";
      udn.insert (UDNList::value_type (UDN (s.c_str()), UDNInfo()));
      return true;
   }

//______________________________________________________________________________
   bool dfmsends::requestUDNInfo (const UDN& udn, UDNInfo& info)
   {
      if (my_debug) cerr << "dfmsends::requestUDNInfo()" << endl ;
      // build UDN info
      //cerr << "ask for UDN info" << endl;
      frametype utype = NONE;
      // The UDN contains several substrings we need, such as the server name,
      // the server port, the type, possibly epoch start and stop. Extract these.
      unsigned long epoch_start=0, epoch_stop=0;
      string s(udn) ;
      string es("epoch_start=") ;
      string ee("epoch_end=") ;
      string si("nds2://") ;      // This string identifes this as a SENDS.
      string ff("/frames") ;
      string stf("/trend") ;
      string mtf("/minute-trend") ;
      if (s.find(ff) != string::npos) 
      {
	 utype = FF ;
	 if (my_debug) cerr << "  utype = FF" << endl ;
      }
      else if (s.find(stf) != string::npos)
      {
	 utype = STF ;
	 if (my_debug) cerr << "  utype = STF" << endl ;
      }
      else if (s.find(mtf) != string::npos)
      {
	 utype = MTF ;
	 if (my_debug) cerr << "  utype = MTF" << endl ;
      }
      else
      {
	 if (my_debug) cerr << "dfmsends::requestUDNInfo() - Can't determine utype" << endl ;
	 return false ;
      }

      // Look for epoch start, end.  If present, set the epoch
      string::size_type es_pos = s.find(es) ;
      string::size_type ee_pos = s.find(ee) ;
      // Look for the address and port.
      string::size_type si_pos = s.find(si) ;
      string::size_type addr_end = s.find_first_of(":/&?", si_pos+ si.length()) ;
      fServer = s.substr(si_pos+si.length(), addr_end - (si_pos+si.length())) ;
      if (my_debug) cerr << "dfmsends::requestUDNInfo() - fServer = " << fServer << endl ;
      fPort = atoi(s.substr(addr_end + 1, string::npos).c_str()) ;
      if (my_debug) cerr << "dfmsends::requestUDNInfo() - fPort = " << fPort << endl ;
      
      UDNInfo uinfo;
      uinfo.setType (utype);
      // get channel names
      // Use the epoch_start, stop times, or neither.
      channellist chns;
      if (es_pos != string::npos && ee_pos != string::npos)
      {
	 epoch_start = atoi(s.c_str() + es_pos + es.length()) ;
	 epoch_stop = atoi(s.c_str() + ee_pos + ee.length()) ;
	 // Both start and stop were found, set the epoch.
	 if (my_debug) cerr << "Setting epoch to " << epoch_start << ", " << epoch_stop << endl;
	 if (!sends_support::getChannels (fServer.c_str(), fPort, chns, utype, epoch_start, epoch_stop)) {
	    cerr << "Can't get channels for " << fServer << ":" << fPort << endl;
	    return false;
	 }
      }
      else
      {
	 if (!sends_support::getChannels (fServer.c_str(), fPort, chns, utype)) {
	    cerr << "Can't get channels for " << fServer << ":" << fPort << endl;
	    return false;
	 }
      }
      uinfo.setChannels (chns);
      // get times
      Time start;
      Time stop;
      //cerr << "ask for time" << endl;
      if (!sends_support::getTimes (fServer.c_str(), fPort, start, 
                           stop, utype)) {
	 cerr << "Can't get times for " << fServer << ":" << fPort << endl;
         return false;
      }
      //cerr << "ask for time done" << endl;
      if (stop > start) {
         uinfo.insertDSeg (start, stop - start);
      }
      info = uinfo;
      if (my_debug) cerr << "dfmsends::requestUDNInfo() return TRUE" << endl ;
      return true;
   }


}
