#include "gmutex.hh"
#include "dfmapi.hh"
#include "dfm.hh"
#include "dfmlars.hh"
#include "dfmnds.hh"
#include "dfmsends.hh"
#include "dfmsm.hh"
#include "dfmfile.hh"
#include "dfmtape.hh"
#include "dfmfunc.hh"
#include "fchannel.hh"
#include "smartio.hh"
#include "framemux.hh"
#include <string>
#include <iostream>
#include <map>

namespace dfm {
   using namespace std;

   typedef std::map <std::string, UDNList> ServerList;

   static UDNList udncache;
   static ServerList servercache;
   static thread::mutex cachemux;

   static int my_debug = 0 ;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dfmapi                                                               //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool dfmapi::requestData (fantom::framemux& mux, const Time& start, 
                     const Interval& duration, const UDNList& udn, 
                     stagingtype staging, const Interval& keep,
                     fantom::channelquerylist* select)
   {
      bool success = true;
      mux.setClock (start);
      mux.setStop (start + duration);
      for (const_UDNiter i = udn.begin(); i != udn.end(); ++i) {
         string name = (const char*) i->first;
         //cerr << "Request " << name << endl;
      	 // create new smart input
         fantom::smart_input* inp = 
            new (std::nothrow) fantom::smart_input (name.c_str());
         if (!inp) {
            success = false;
            break;
         }
      	 // set parameters
         inp->setTimeLimits (start, duration);
         inp->setState (fantom::smartio_basic::io_active);
         fantom::channelquerylist chnq = 
            preselectChannels (i->first, i->second.channels(), select);
         inp->setChannelList (chnq.empty() ? 0 : &chnq);
      	 // Add to input list
         int num = mux.inp().size();
         if (!mux.inp().Add (num, inp)) {
            success = false;
            break;
         }
      }
      return success;
   }

//______________________________________________________________________________
   bool dfmapi::sendData (fantom::framemux& mux, const UDN& udn, 
                     const char* format, 
                     const fantom::channelquerylist* channels)
   {
      string name = (const char*) udn;
      //cerr << "Send " << name << endl;
      // create new smart output
      fantom::smart_output* out = 
         new (std::nothrow) fantom::smart_output (name.c_str());
      if (!out) {
         return false;
      }
      // set parameters
      out->setType (format);
      out->setState (fantom::smartio_basic::io_active);
      out->setChannelList (channels);
      // add to output list
      int num = mux.out().size();
      if (!mux.out().Add (num, out)) {
         return false;
      }
      return true;
   }

//______________________________________________________________________________
   bool dfmapi::cachedUDNs (const char* server, UDNList& udn, bool force)
   {
      if (!server) {
         return false;
      }
      string dname (server);
      thread::semlock lockit (cachemux);
      // check in cache
      if (!force) {
         ServerList::iterator i = servercache.find (dname);
         if (i != servercache.end()) {
            udn = i->second;
            return true;
         }
      }
      // lookup
      cachemux.unlock();
      bool succ = requestUDNs (udn);
      cachemux.lock();
      if (!succ) {
         return false;
      }
      // add to cache
      servercache[dname] = udn;
      return true;
   }

//______________________________________________________________________________
   bool dfmapi::cachedUDNInfo (const UDN& udn, UDNInfo& info, bool force)
   {
      if (my_debug) {
	 cerr << "dfmapi::cachedUDNInfo()" << endl ;
	 cerr << "  Cache contents:" << endl ;
	 for (UDNiter i = udncache.begin(); i != udncache.end(); i++)
	    cerr << "   " << i->first << endl ;
      }

      thread::semlock lockit (cachemux);
      // check if in cache
      if (!force) {
         UDNiter i = udncache.find (udn);
         if (i != udncache.end()) {
            info = i->second;
	    if (my_debug) {
	       cerr << "dfmapi::cachedUDNInfo return TRUE, udncache.find() succeeded." << endl ;
	       // See what info has in it... info is a UDNInfo...
	       fantom::channellist channels = info.channels() ;
	       cerr << "Number of channels is " << channels.size() << endl ;
	    }
            return true;
         }
      }
      // lookup
      cachemux.unlock();
      if (my_debug) cerr << "dfmapi::cachedUDNInfo() - calling requestUDNInfo(" << udn << ", <info>)" << endl ;
      bool succ = requestUDNInfo (udn, info);
      cachemux.lock();
      if (!succ) {
	 if (my_debug) cerr << "dfmapi::cachedUDNInfo - requestUDNInfo() failed, return FALSE" << endl ;
         return false;
      }
      // add to cache
      udncache[udn] = info;
      if (my_debug) {
	 cerr << "dfmapi::cachedUDNInfo() - requestUDNInfo successful, return TRUE" << endl ;
	 fantom::channellist channels = info.channels() ;
	 cerr << "Number of channels is " << channels.size() << endl ;
      }
      return true;
   }

//______________________________________________________________________________
   void dfmapi::ClearCache()
   {
      dfmfile::ClearCache();
      thread::semlock lockit (cachemux);
      udncache.clear();
      servercache.clear();
   }

//______________________________________________________________________________
   fantom::channelquerylist dfmapi::preselectChannels (
                     const UDN& udn,
                     const fantom::channelquerylist& insel,
                     const fantom::channelquerylist* outsel)
   {
      fantom::channelquerylist chnsel;
      // check if simple algorithm is sufficient
      if (!fChnPreselFull) {
         chnsel = insel;
         return chnsel;
      }
      cerr << "FULL CHANNEL PRESELECT REQUEST" << endl;
      // Get UDN info
      UDNInfo uinfo;
      if (!cachedUDNInfo (udn, uinfo, false)) {
         chnsel = insel;
         return chnsel;
      }
      // Get channel list
      fantom::channellist chnlist = uinfo.channels();
      if (chnlist.empty()) {
         chnsel = insel;
         return chnsel;
      }
      // Filter input selection
      if (!insel.empty()) {
         if (!fantom::FilterChannels (chnlist, &insel)) {
            chnsel = insel;
            return chnsel;
         }
      }
      // Filter output selection
      if (outsel && !outsel->empty()) {
         if (!fantom::FilterChannels (chnlist, outsel)) {
            chnsel = insel;
            return chnsel;
         }
      }
      cerr << "LENGTH OF REQUESTED CHANNEL LIST IS " << chnlist.size() << endl;
      chnsel = fantom::channelquerylist (chnlist);
      return chnsel;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// createDFMapi                                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   dfmapi* createDFMapi (dataservicetype type)
   {
      switch (type) {
         case st_LARS: 
            {
               return new (nothrow) dfmlars;
            }
         case st_NDS: 
            {
               return new (nothrow) dfmnds;
            }
         case st_SENDS: 
            {
               return new (nothrow) dfmsends;
            }
         case st_SM: 
            {
               return new (nothrow) dfmsm;
            }
         case st_File: 
            {
               return new (nothrow) dfmfile;
            }
         case st_Tape: 
            {
               return new (nothrow) dfmtape;
            }
         case st_Func: 
            {
               return new (nothrow) dfmfunc;
            }
         case st_Invalid:
         default:
            {
               return 0;
            }
      }
   }


}
