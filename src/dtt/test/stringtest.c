/* version $Id: stringtest.c 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include "gdsutil.h"

/* main program */

   #ifdef OS_VXWORKS
   int stringtest ()
   #else
   int main(int argc, char *argv[])
   #endif
   {
      int	n;
      char	s1[256];
      char	s2[256];
   
      strcpy (s1, "this is a test to");
      strcpy (s2, "this is a test too");
      n = 16;
      printf ("Compare '%s' with '%s': %i\n", 
             s1, s2, strcmp (s1, s2));
      printf ("Compare '%s' with '%s': %i\n", 
             s1, s2, gds_strcasecmp (s1, s2));
      printf ("Compare up to %i chars of '%s' with '%s': %i\n", 
             n, s1, s2, gds_strncasecmp (s1, s2, n));
   
      printf ("\n\n");
      strcpy (s1, "H1:LSC-lalala_2");
      chnMakeName (s1, "L", "PM", "ASC", "dadada_3");
      if (chnIsValid (s1) == 0) {
         printf ("%s is invalid\n", s1);
      }
      else {
         printf ("%s is valid\n", s1);
      }
      if (chnSitePrefix (s1, s2) != NULL) {
         printf ("site prefix = %s\n", s2);
      }
      if (chnIfoPrefix (s1, s2) != NULL) {
         printf ("ifo prefix = %s\n", s2);
      }
      if (chnSubsystemName (s1, s2) != NULL) {
         printf ("subsystem = %s\n", s2);
      }
      if (chnRemName (s1, s2) != NULL) {
         printf ("remaining = %s\n", s2);
      }
      if (chnShortName (s1, s2) != NULL) {
         printf ("short name = %s\n", s2);
      }
   
      return 0;
   }
