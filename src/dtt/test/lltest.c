/* version $Id: lltest.c 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */

#include <stdio.h>
#include <math.h>
#include <limits.h>

#if 0
   typedef int SItype	__attribute__ ((mode (SI)));
   typedef int DItype	__attribute__ ((mode (DI)));

   struct DIstruct {SItype high, low;};
   typedef union
   {
      struct DIstruct 	s;
      DItype 		ll;
   } DIunion;

   DItype _divdi3 (DItype x1, DItype x2)
   {
      DIunion	w;
   
      w.ll = x1 + x2;
      printf ("result = %x | %x\n", w.s.high, w.s.low);
      /* w.ll = x1 + x2;*/
      return w.ll;
   }

   void printLL (DItype x) 
   {
      DIunion	w;
   
      w.ll = x;
      printf ("result = %x | %x\n", w.s.high, w.s.low);
   }
#endif

   void	main (void)
   {
      /* long long	x;
      long long	y;
      long long	z; */
      short s;
      float t;
   
      t = floor (-1.1);
      s = (short) t;
      printf ("f = %f, s = %i\n", t, s);
   
      /* printf ("size of DItype = %i\n", sizeof (DItype));
   
      x = 0x034100000045LL;
      y = 0x000200006645LL;
      y = 0x000300006745LL;
      z = x + y;
      printLL (z);
      z = x / y;
      printLL (z);
      z = x % y;
      printLL (z);
   
      z = __divdi3 (x, y);
      printLL (z); */
   }
