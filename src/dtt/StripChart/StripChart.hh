#ifndef STRIPCHART_HH
#define STRIPCHART_HH
//
//    Strip chart class
//
#include "circbuf.hh"

class StripPen;
class TCanvas;
class TPolyLine;
class TPad;

/**  The strip-chart class displays N series of data points that vary with 
  *  the same x (time) spacing. An arbitrary number of data series (pens) 
  *  may be displayed over an arbitrary interval. The pens are numbered from 
  *  0 to nPen-1. Each strip chart object is displayed on a separate root 
  *  TCanvas created and controlled by the object.
  *  @memo Strip-chart display class.
  *  @author J. Zweizig
  *  @version 1.3; Modified: January 12, 2001
  */
class StripChart {
public:
  //------------------------------------  SPecial data types
  /**  X-Axis display format.
    *  AxisTYpe enumerates the x-axis display formats.
    */
  enum AxisType {

      /**  No horizontal axis.
        */
      kNone,

      /**  Pure number.
        */
      kNumber,

      /**  Grenwich Mean Time.
        */
      kGMTime,

      /**  Local Time.
        */
      kLocalTime,
  };

  typedef double XAxis_t;

  //------------------------------------  Constructors, destructors
  /**  A strip chart is created with 'nPen' pens. A new canvas is created
    *  with title 'Title'.
    *  @memo Create a new strip chart.
    *  @param nPen Number of pens on the strip chart.
    *  @param Title optional title to be given to the created canvas.
    */
  StripChart(int nPen=0, const char* Title=0);

  /**  A strip chart is created with 'nPen' pens on the specified Pad.
    *  @memo Create a new strip chart.
    */
  StripChart(int nPen, TPad* canvas);

  /**  The strip chart is destroyed and its display is removed.
    *  @memo Destroy a strip chart.
    */
  ~StripChart();

  /**  getLogY() returns true if the vertical scale of pen 'p' is logarithmic
    *  and false if it is linear.
    *  @memo Get vertical scale mode.
    */
  bool getLogY(int p) const;

  /**  getMaxX() returns the current upper limit of the x axis.
    *  @memo X axis maximum.
    */
  XAxis_t getMaxX(void) const {return mXmin;}

  /**  getMinX() returns the current lower limit of the x axis.
    *  @memo X axis minimum.
    */
  XAxis_t getMinX(void) const {return mXmin;}

  /**  getN() returns the number of entries per pen in the strip chart 
    *  history buffer.
    *  @memo History length.
    */
  int getN(void) const {return mNPosn;}

  //------------------------------------  Mutators
  /**  If auto-scroll is set, the x-axis of the chart will be pushed forward
    *  whenever a point with x > x-Max is plotted. The minimum scroll distance
    *  is set by set-Scroll().
    *  @memo Set auto-scroll mode.
    */
  void setAxisType(enum AxisType mode) {mAxisType = mode;}

  /**  If auto-scroll is set, the x-axis of the chart will be pushed forward
    *  whenever a point with x > x-Max is plotted. The minimum scroll distance
    *  is set by set-Scroll().
    *  @memo Set auto-scroll mode.
    */
  void setAutoScroll(bool smode=true) {mAutoScroll = smode;}

  /**  Before they are plotted, the pen data are normalized to fit within a 
    *  horizontal stripe. A linear normalization transformation is used i.e. 
    *  Ynorm[i] = Scale * Y[i] + Bias. By default the scale is set to 
    *  0.5/nPen and the bias is set to (p + 0.5)/nPen so that nPen signals 
    *  with -1.0 < Y[i] <1.0 will fit on a Plot with Ymin=0 and Ymax=1.0.
    *  @memo Set the normalization bias for pen 'p'.
    */
  void setBias(int p, float bias);

  /**  Pen 'p' is set to the named color.
    *  @memo Set pen color.
    */
  void setColor(int p, const char* color);

  /**  Pen 'p' is set to the specified color index.
    *  @memo Set pen color.
    */
  void setColor(int p, int color);

  /**  The vertical scale mode of pen 'p' is set to logarithmic (if log=true)
    *  or linear (if log=false).
    *  @memo Set pen vertical mode.
    */
  void setLogY(int p, bool logy);

  /**  The vertical mode for all defined pens is set to logarithmic (if 
    *  logy=true) or linear (if logy=false). All pens created after this
    *  function is called will initially have the same mode.
    *  @memo Set vertical mode for all pens.
    */
  void setLogY(bool logy);

  /**  The x-axis upper limit (mXmax) is set to the specified value. mXmax
    *  defaults to 1000.
    *  @memo Set the initial x-axis maximum.
    */
  void setMaxX(XAxis_t xmax);

  /**  The y-axis upper limit (mYmax) is set to the specified value. mYmax
    *  defaults to 1.
    *  @memo Set the y-axis maximum.
    */
  void setMaxY(float ymax);

  /**  The x-axis lower limit (mXmin) is set to the specified value. mXmin
    *  defaults to 0.
    *  @memo Set the initial x-axis minimum.
    */
  void setMinX(XAxis_t xmin);

  /**  The y-axis lower limit (mYmin) is set to the specified value. mYmin
    *  defaults to 0.
    *  @memo Set the initial y-axis minimum.
    */
  void setMinY(float ymin);

  /*  All pens are reallocated with the specified number of entries in 
    *  the history list. All pen information including the current history, 
    *  the specified color, etc. are lost.
    *  @memo Set the number of history elements.
    **/
  void setN(int N);

  /**  The number of pens is set to 'N'. If N is larger than the current 
    *  number of pens, new pens are added. If N is less than the current 
    *  number, the high-numbered pens are deleted.
    *  @memo Set the number of pens.
    */
  void setNPen(int N);

  /**  The normalization scale for pen 'p' is set to 'scale'. See the 
    *  description setBias() for a discussion of the normalization transform.
    *  @memo Set the normalization scale.
    */
  void setScale(int p, float scale);

  /**  Specify the minimum scrolling distance as a fraction of the x-axis
    *  length. If auto-scroll is enabled, the x-axis will be scrolled by this 
    *  distance when a point is plotted beyond the current limits. This is
    *  also used as a minimum distance if scrolling is requested explicitly.
    *  @memo Set the minimum scroll distance.
    */
  void setScroll(float scrolmin) {mScroll = scrolmin;}

  /**  Select whether the pen names will be displayed. If the flag is set,
    *  any pen titles will be displated at the left hand side of the pen 
    *  trace position. If the flag is cleared the Chart is extended to the
    *  almost te edge of the canvas.
    *  @memo Show pen names.
    *  @param show if true the pen titles will be displayed 
    */
  void setShowNames(bool show) {mPenNames = show;}

  /**  The normalization transform for pen 'p' is set to map a symmetric
    *  band centered at 'Center' and with half-width 'hWidth' into the
    *  'p'th of nPen equal-sized horizontal stripes on the strip chart.
    *  See setBias() for a discussion of the normalization transform.
    *  @memo Set normalization transform for one pen.
    */
  void setStripe(int p, float Center, float hWidth);

  /**  Define a title for the specified pen.
    *  @memo Define a pen title.
    */
  void setTitle(int p, const char* title);

  /**  BY default no axis values are displayed.
    *  @memo Enable plotting of y-axis values.
    */
  void setYAxis(bool On) {mPlotYAxis = On;}

  /**  Scroll the strip-chart forward by 'dT' units in x. The width of the
    *  chart is maintained.
    *  @memo Scroll the chart.
    */
  void scroll(float dT);

  /**  The nPen values in y[] are plotted at 'x'.
    *  @memo Plot a time point.
    */
  void plot(XAxis_t x, const float* y);

  /**  Re-Draw the strip-chart.
    *  The strip chart is re-displayed with any new settings.
    */
  void redraw(void);

private:
  /**  initialize the strip char for the specified canvas and number of 
    *   pens.
    *  @memo Chart initialization.
    */
  void InitChart(int nPen, TPad* canvas);

  /**  The canvas is drawn with axis, pen titles, and the pad for the 
    *  strip chart.
    *  @memo Format the canvas.
    */
  void drawAxis();

  /**  The parameters of pen 'p' are set to the default values.
    *  @memo Set pen parameters to default values.
    */
  void resetPen(int p);

  /**  The polyline for all pens is updated from history index 'inx' through
    *  the most recent data. If exist beyond the current x upper limit, the 
    *  chart is automatically scrolled.
    *  @memo Update the chart.
    */
  void update(int inxmin=-1);

private:
  /**  Number of pens.
    *  mNPen contains the number of pens allocate.
    */
  int mNPen;

  /**  If set, the strip chart canvas is delted when the StripChart 
    *  is destroyed.
    *  @memo Delete canvas flag.
    */
  bool mDelCanvas;

  /**  mCanv points to a canvas allocated by the StripChart.
    *  @memo Canvas pointer.
    */
  TPad* mCanv;

  /**  mPlot points to the Pad containing the StripChart.
    *  @memo Plot pad.
    */
  TPad* mPlot;

  /**  mPen is a vector of nPen pointers to the StripPen objects storing
    *  the status of each pen.
    *  @memo Pen pointers.
    */
  StripPen** mPen;

  /**  If set the pen names will be displayed to the left of the strip chart.
    *  @memo Display pen names.
    */
  bool mPenNames;

  /**  PolyLine high water mark.
    *  mHighWater points to the first unused Polyline point.
    */
  int mHighWater;

  /**  Default scale mode.
    *  mLogy defines the default vertical scale mode. 
    */
  bool mLogY;

  ///  x-axis minimum (external units)
  XAxis_t mXmin;

  ///  x-axis maximum (external units)
  XAxis_t mXmax;

  ///  y-axis minimum (external units)
  float mYmin;

  ///  y-axis maximum  (external units)
  float mYmax;

  XAxis_t mX0;

  /**  Enable autoscrolling.
    *  If set, the chart will be scrolled automatically to include the 
    *  largest x point.
    */
  bool mAutoScroll;

  /**  Scroll distance.
    *  Minimum distance to scroll the x axis, specified as a fraction of 
    *  the x-axis width.
    */
  float mScroll;

  /**  History length.
    *  Number of entries in the history list.
    */
  int mNPosn;

  /**  Circular buffer iterator.
    *  Iterator used to manage the circular history buffers.
    */
  circbuf mInx;

  /**  X-axis history buffer.
    *  pointer to a circular buffer containing the historic x-axis values.
    */
  XAxis_t* mXData;

  /**  X-Axis display selection.
    *  display mode for thetime axis.
    */
  enum AxisType mAxisType;

  /**  Axes are plotted.
    */
  bool mAxisOK;

  /**  Axes are plotted.
    */
  bool mPlotYAxis;

};

#endif
