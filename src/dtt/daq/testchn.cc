/* version $Id: testchn.cc 7990 2018-01-13 01:32:22Z john.zweizig@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: testchn							*/
/*                                                         		*/
/* Module Description: name handling for diagnostics test channels	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */

#include <time.h>
#include "testchn.hh"

namespace diag {
   using namespace std;


   bool channelHandler::setSiteIfo (char SiteDefault, char SiteForce, 
                     char IfoDefault, char IfoForce)
   {
      siteDefault = SiteDefault;
      siteForce = SiteForce;
      ifoDefault = IfoDefault;
      ifoForce = IfoForce;
      return true;
   }


   bool channelHandler::channelInfo (const string& name, 
                     gdsChnInfo_t& info, int rate) const
   {
      return (gdsChannelInfo (channelName (name).c_str(), 
                             &info) == 0);
   }

   string channelHandler::channelName (const string& name) const
   {
      return name;
   }

}
