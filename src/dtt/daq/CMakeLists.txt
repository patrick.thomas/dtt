SET(DAQ_SRC
    gdschannel.c
    testchn.cc)

add_library(daq_lib OBJECT
    ${DAQ_SRC}
    )

target_include_directories(daq_lib PRIVATE
    ${DTT_INCLUDES}
    ${RPC_OUTPUT_DIR})

target_include_directories(daq_lib PUBLIC SYSTEM BEFORE ${RPC_INCLUDE_DIR})

add_dependencies(daq_lib
    rchannel_rpc
)

set_target_properties(daq_lib PROPERTIES
        PRIVATE_HEADER
            "gdschannel.h;testchn.hh")

INSTALL_HEADERS(daq_lib)