/* version $Id: FilterSection.hh 8040 2018-07-21 17:14:42Z john.zweizig@LIGO.ORG
 * $ */
#ifndef _LIGO_FILTERSECTION_H
#define _LIGO_FILTERSECTION_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: FilterSection						*/
/*                                                         		*/
/* Module Description: Filter section as defined by online system	*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 1.0	 4Aug02   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: FilterSection.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "FilterDesign.hh"
#include <string>
#include <vector>

namespace filterwiz
{

    /// Input Switching type
    enum input_switching
    {
        /// Always On
        kAlwaysOn = 1,
        /// Zero History
        kZeroHistory = 2
    };

    /// Input Switching type
    enum output_switching
    {
        /// Immediately
        kImmediately = 1,
        /// Ramp
        kRamp = 2,
        /// Input Crossing
        kInputCrossing = 3,
        /// Zero Crossing
        kZeroCrossing = 4
    };

    /** Filter Section Class. Represents a single filter section.

        @memo Filter Section
        @author Written August 2002 by Daniel Sigg
        @version 1.0
     ************************************************************************/
    class FilterSection
    {
    public:
        /// Default constructor
        FilterSection( );

        /// Constructor
        FilterSection( double fsample, int index );

        /// order by index
        bool
        operator<( const FilterSection& fs ) const
        {
            return fIndex < fs.fIndex;
        }

        /// empty() returns true if filter is unity gain.
        bool empty( ) const;

        /// designEmpty() returns true if filter has no design.
        bool designEmpty( ) const;

        ///  check filter (compare design and fFilter)
        bool check( ) const;

        ///  check validity of design string
        bool valid( ) const;

        ///  update filter from design string
        bool update( );

        ///  add a filter to both design and filter
        bool add( const char* cmd );

        /// Get index
        int
        getIndex( ) const
        {
            return fIndex;
        }

        /// Set index
        void
        setIndex( int index )
        {
            fIndex = index;
        }

        /// Get name
        const char*
        getName( ) const
        {
            return fName.c_str( );
        }

        /// Set name
        void
        setName( const char* p )
        {
            fName = p;
        }

        /// Get design string
        const char*
        getDesign( ) const
        {
            return fDesign.c_str( );
        }

        /// Get design string
        const std::string&
        refDesign( ) const
        {
            return fDesign;
        }

        /// Set design string
        void setDesign( const char* p, bool splitcmd = true, int maxline = 0 );

        void
        setDesign( const std::string s )
        {
            fDesign = s;
        };

        /// Filter access
        FilterDesign&
        filter( )
        {
            return fFilter;
        }

        /// Filter access
        const FilterDesign&
        filter( ) const
        {
            return fFilter;
        }

        /// Get input switching
        input_switching
        getInputSwitch( ) const
        {
            return fInpSwitch;
        }

        /// Set input switching
        void
        setInputSwitch( input_switching sw )
        {
            fInpSwitch = sw;
        }

        /// Get output switching
        output_switching
        getOutputSwitch( ) const
        {
            return fOutSwitch;
        }

        /// Set output switching
        void
        setOutputSwitch( output_switching sw )
        {
            fOutSwitch = sw;
        }

        /// Get ramp time
        double
        getRamp( ) const
        {
            return fRamp;
        }

        /// Set ramp time
        void
        setRamp( double ramp )
        {
            fRamp = ramp;
        }

        /// Get tolerance
        double
        getTolerance( ) const
        {
            return fTolerance;
        }

        /// Set tolerance
        void
        setTolerance( double tol )
        {
            fTolerance = tol;
        }

        /// Get timeout
        double
        getTimeout( ) const
        {
            return fTimeout;
        }

        /// Set timeout
        void
        setTimeout( double timeout )
        {
            fTimeout = timeout;
        }

        /// Get header
        const char*
        getHeader( ) const
        {
            return fHeader.c_str( );
        }

        /// Set header
        void
        setHeader( const char* p )
        {
            fHeader = p;
        }

        /// Split a command argument into multiple lines
        static std::string splitCmd( const char* cmd, int maxlen = 0 );

        /// Set the fGainOnly flag
        void
        setGainOnly( int gain_only )
        {
            fGainOnly = gain_only;
        }
        /// Access to the fGainOnly flag
        bool
        getGainOnly( void ) const
        {
            return fGainOnly;
        }

        /// Access to the gain-only gain.
        void
        setGainOnlyGain( std::string gain )
        {
            fGainOnlyGain = gain;
        }
        std::string
        getGainOnlyGain( )
        {
            return fGainOnlyGain;
        }

    protected:
        /// index - value 0-9, filter section number
        int fIndex;

        /// Name - Just a label for the filter section.
        std::string fName;

        /// Design string - Defines the filter design parameters
        std::string fDesign;

        /// Filter - Implementation of the design.
        FilterDesign fFilter;

        /// Input switching
        input_switching fInpSwitch;

        /// Output switching
        output_switching fOutSwitch;

        /// Ramp time - relative to sampling frequency.
        double fRamp;

        /// Tolerance
        double fTolerance;

        /// Timeout
        double fTimeout;

        /// Header
        std::string fHeader;

        /// GainOnly flag - set true if filter is a gain-only filter.
        int fGainOnly;

        /// Gain only gain - Meaningful if fGainOnly is true, this
        /// should be set to the gain of the filter.  Needed because the
        /// filter creation functions tend to do away with gain-only filters
        /// especially if the gain == 1.
        std::string fGainOnlyGain;
    };

    /// List of filter sections
    typedef std::vector< FilterSection > FilterSectionList;

} // namespace filterwiz

#endif // _LIGO_FILTERSECTION_H
