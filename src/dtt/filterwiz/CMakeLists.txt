
# libfilterfile: control filter files.
add_library(filterfile SHARED
        FilterFile.cc
        FilterModule.cc
        FilterSection.cc
        FilterMessage.cc
        )

target_include_directories(filterfile PUBLIC  ${EXTERNAL_INCLUDE_DIRECTORIES})

set_target_properties(filterfile PROPERTIES
        VERSION
            0.0.1
        PRIVATE_HEADER
            "FilterFile.hh;FilterModule.hh;FilterSection.hh;FilterMessage.hh"
        )

target_link_libraries(filterfile PUBLIC
        dmtsigp
        gdsbase
        )

INSTALL_LIB(filterfile ${INCLUDE_INSTALL_DIR}/foton)



if (NOT ONLY_FOTON)

    # libRfilterfile: ROOT dictionary used by foton.py to access filter files
    SET(dict_headers
            FilterSection.hh
            FilterModule.hh
            FilterFile.hh
            FilterMessage.hh
            )

    create_cint_dict(filterfile filterfile 0.0.1
            -c
            -I${CMAKE_CURRENT_SOURCE_DIR}
            -I${CMAKE_SOURCE_DIR}/src/config
            -I${GDS_INCLUDE_DIR}
            ${dict_headers}
            filterfile_linkdef.h
            )

    target_link_libraries(
            Rfilterfile PUBLIC
            # project libs
            filterfile

            # gds libs
            dmtsigp

            # root libs
            Core
    )

    # filterwiz library used by foton and other gui programs to load filter editor.
    add_library(filterwiz SHARED
            TLGFilterDlg.cc
            TLGFilterWizard.cc)

    target_include_directories(filterwiz PUBLIC
            ../gui/dttgui
            #${EXTERNAL_INCLUDE_DIRECTORIES}
            )

    set_target_properties(filterwiz PROPERTIES
            VERSION
                0.0.1
            PRIVATE_HEADER
                "TLGFilterDlg.hh;TLGFilterWizard.hh"
            )

    target_link_libraries(filterwiz
            # project libs
            filterfile
            dttgui

            # gds libs
            gdsbase
            dmtsigp

            # root libs
            Gui
            Core

            )

    INSTALL_LIB(filterwiz ${INCLUDE_INSTALL_DIR}/foton)

endif()
