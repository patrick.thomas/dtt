
/* GDS test point rpc interface */

/* fix include problems with VxWorks */
#ifdef RPC_HDR
%#define		_RPC_HDR
#endif
#ifdef RPC_XDR
%#define		_RPC_XDR
#endif
#ifdef RPC_SVC		
%#define		_RPC_SVC
#endif
#ifdef RPC_CLNT		
%#define		_RPC_CLNT
#endif
%#include "rpcinc.h"
%#include "rgdsmsgtype.h"


/* rpc interface */
program RGDSMSGCB {
   version RGDSMSGCBVERS {

      reply_r GDSMSGNOTIFY (int id, message_r msg) = 1;

   } = 1;
} = 0x40000001;
