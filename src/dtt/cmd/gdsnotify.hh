/* -*- mode: c++; c-basic-offset: 3; -*- */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: gdsnotify.h						*/
/*                                                         		*/
/* Module Description: notification class				*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 12Apr99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: testiter.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 5.0				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS NW17-161				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_NOTIFY_H
#define _GDS_NOTIFY_H

/* Header File List: */
#include <string>

namespace diag {


/** Command notification object
    This object send notification messages back to the user interface.
    In order to work it has to be initialized with a valid callback
    function and the user interface must have registered a callback
    handler.
   
    @memo Object for sending command notifications
    @author Written April 1999 by Daniel Sigg
    @version 0.1
 ************************************************************************/
   class cmdnotify {
   public:
      /// Flag for specifying the notification message
      enum flag {
      /// invalid message
      invalid = 0,
      /// begin of test message
      begin = 1,
      /// end of test message
      end = 2,
      /// begin of measurement message
      testBegin = 3,
      /// end of measurement message
      testEnd = 4,
      /// new analysis result
      testAnalysis = 5,
      /// new iterator result
      iterEvaluation = 6,
      /// data receiving error
      dataError = 7,
      /// number of messages
      lNotify = 8};
      typedef enum flag flag;
   
      /// prototype for callback function
      typedef int (*diagNotification) (
                        const char* msg, const char* prm, int pLen, 
                        char** res, int* rLen);
   
      /// contains the notification messages
      static const char* const msgs[];
   
      /** Constructs a command notification object.
          @param NotifyFunc notification callback function
          @memo Default constructor.
       ******************************************************************/
      explicit cmdnotify (diagNotification NotifyFunc = 0) :
      notifyFunc (NotifyFunc) {
      }
   
      /** Sends a notification message.
          @memo Send method.
          @return true if successful
       ******************************************************************/
      bool send (flag id) const;
   
      /** Sends an errro notification message.
          @memo Send error method.
          @return true if successful
       ******************************************************************/
      bool sendError (const std::string& errmsg) const;
   
   private:
      /// callback function
      diagNotification		notifyFunc;
   };

}

#endif /* _GDS_NOTIFY_H */
