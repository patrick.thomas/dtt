CREATE_RPC_TARGET(rgdsmsg)
CREATE_RPC_TARGET(rgdsmsgcb)
CREATE_RPC_TARGET(rgdsmsgtype)

add_dependencies(rgdsmsgcb_rpc
        rgdsmsgtype_rpc)

add_dependencies(rgdsmsg_rpc
        rgdsmsgtype_rpc)

set(CMD_SRC
    cmdapi.cc
    cmdline.cc
    gdscmd_d.c
    gdscmd.cc
    gdsmsg_server.c
    gdsmsg.c
    gdsnotify.cc
    )

add_library(cmd_lib OBJECT
    ${CMD_SRC}
)

target_include_directories(cmd_lib PRIVATE
    ${DTT_INCLUDES}
    ${RPC_OUTPUT_DIR}
    )
target_include_directories(cmd_lib PUBLIC SYSTEM BEFORE ${RPC_INCLUDE_DIR})

add_dependencies(cmd_lib
    rgdsmsg_rpc
    rgdsmsgcb_rpc
    rgdsmsgtype_rpc)

target_link_libraries(cmd_lib PUBLIC
        ${READLINE_LIBRARIES}
        gdsbase
        pthread
        util_lib)