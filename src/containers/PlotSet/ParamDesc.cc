#include "ParamDesc.hh"
#include "tconv.h"
#include <cstdio>
#include <cstring>
#include <time.h>
#include <cmath>
#include <cstdlib>
#include <iostream>


   using namespace std;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// ParameterDescriptor                                                  //
//                                                                      //
// Descriptor for parameters                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   const int ParameterDescriptor::kPrmStartTime = 1;
   const int ParameterDescriptor::kPrmAverages = 2;
   const int ParameterDescriptor::kPrmThird = 4;
   const int ParameterDescriptor::kPrmUser = 8;

//______________________________________________________________________________
   ParameterDescriptor::ParameterDescriptor ()
   : fParamSet (0), fT0Sec (0), fT0NSec (0), fAverages (0),
   fThird (""), fUser (0)
   {
   }

//______________________________________________________________________________
   ParameterDescriptor::ParameterDescriptor (
                     const ParameterDescriptor& prmd)
   : fParamSet (0), fT0Sec (0), fT0NSec (0), fAverages (0),
   fThird (""), fUser (0)
   {
      *this = prmd;
   }

//______________________________________________________________________________
   ParameterDescriptor::ParameterDescriptor (
                     const ParameterDescriptor* prmd)
   : fParamSet (0), fT0Sec (0), fT0NSec (0), fAverages (0),
   fThird (""), fUser (0)
   {
      if (prmd) {
         *this = *prmd;
      }
   }

//______________________________________________________________________________
   ParameterDescriptor::~ParameterDescriptor()
   {
      if (fUser != 0) {
         delete [] fUser;
      }
   }

//______________________________________________________________________________
   ParameterDescriptor& ParameterDescriptor::operator= (
                     const ParameterDescriptor& prmd)
   {
      if (this != &prmd) {
         fParamSet = prmd.fParamSet;
         fT0Sec = prmd.fT0Sec;
         fT0NSec = prmd.fT0NSec;
         fAverages = prmd.fAverages;
         fThird = prmd.fThird;
         if (fUser != 0) {
            delete [] fUser;
            fUser = 0;
         }
         if (prmd.fUser != 0) {
            fUser = new char [strlen (prmd.fUser) + 1];
            if (fUser) {
               strcpy (fUser, prmd.fUser);
            }
         }
      }
      return *this;
   }

//______________________________________________________________________________
   void ParameterDescriptor::SetStartTime (unsigned int sec, unsigned int nsec)
   {
      fParamSet |= kPrmStartTime;
      fT0Sec = sec;
      fT0NSec = nsec;
   }

//______________________________________________________________________________
   bool ParameterDescriptor::GetStartTime (unsigned int& sec, 
                     unsigned int& nsec) const
   {
      if (fParamSet & kPrmStartTime) {
         sec = fT0Sec;
         nsec = fT0NSec;
         return true;
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   bool ParameterDescriptor::GetStartTime (string& s, bool utc,
                     double timeshift) const
   {
      if (fParamSet & kPrmStartTime) {
         tainsec_t t0 = 
            fT0Sec * _ONESEC + fT0NSec + (tainsec_t)(timeshift * 1E9);
         char buf[1024];
         if (utc) {
            utc_t mtime;
            TAItoUTC (t0 / _ONESEC, &mtime);
            strftime (buf, 100, "T0=%d/%m/%Y %H:%M:%S", &mtime);
            int usec = (t0 % _ONESEC) / 1000;
            if (usec != 0) {
               sprintf (buf + strlen (buf), ".%06i", usec);
               int i = strlen (buf) - 1;
               while ((i >= 0) && (buf[i] == '0')) {
                  buf[i] = 0;
                  i--;
               }
            }
         }
         else if (t0 % _ONESEC == 0) {
            sprintf (buf, "T0=%i", fT0Sec);
         }
         else {
            sprintf (buf, "T0=%i.%06i", 
                    (unsigned int)(t0 / _ONESEC), (int)((t0 % _ONESEC) / 1000));
            int i = strlen (buf) - 1;
            while ((i >= 0) && (buf[i] == '0')) {
               buf[i] = 0;
               i--;
            }
         }
         s = buf;
         return true;
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   void ParameterDescriptor::SetAverages (int averages)
   {
      fParamSet |= kPrmAverages;
      fAverages = averages;
   }

//______________________________________________________________________________
   bool ParameterDescriptor::GetAverages (int& averages) const
   {
      if (fParamSet & kPrmAverages) {
         averages = fAverages;
         return true;
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   bool ParameterDescriptor::GetAverages (string& s) const
   {
      if (fParamSet & kPrmAverages) {
         char	buf[128];
         sprintf (buf, "Avg=%i", fAverages);
         s = buf;
         return true;
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   void ParameterDescriptor::SetThird (const char* name)
   {
      if (name != 0) {
         fParamSet |= kPrmThird;
         fThird = name;
      }
      else {
         fParamSet &= ~kPrmThird;
         fThird = "";
      }
   }

//______________________________________________________________________________
   const char* ParameterDescriptor::GetThird () const
   {
      if (fParamSet & kPrmThird) {
         return fThird.c_str();
      }
      else {
         return 0;
      }
   }

//______________________________________________________________________________
   bool ParameterDescriptor::GetThird (string& s) const
   {
      if (fParamSet & kPrmThird) {
         s = fThird;
         return true;
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   void ParameterDescriptor::SetUser (const char* xml)
   {
      if (fUser) delete [] fUser;
      fUser = 0;
      if (xml) {
         fUser = new char [strlen (xml) + 1];
         if (fUser) {
            strcpy (fUser, xml);
         }
      }
      if (fUser) {
         fParamSet |= kPrmUser;
      }
      else {
         fParamSet &= !kPrmUser;
      }
   }

//______________________________________________________________________________
   const char* ParameterDescriptor::GetUser () const
   {
      return (fParamSet & kPrmUser) ? fUser : 0;
   }



