/* version $Id: PlotSet.hh 7747 2016-09-23 18:11:08Z james.batch@LIGO.ORG $ */
#ifndef _LIGO_PLOTSET_H
#define _LIGO_PLOTSET_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: PlotSet							*/
/*                                                         		*/
/* Module Description: Plot set description		:		*/
/*		       plot and unit descriptor, plot descriptor pool,	*/
/*		       parameter descriptor				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 29Oct99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGPlotSet.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <string.h>
#include <list>
#include "VirtualPlotPad.hh"
#include "DataDesc.hh"
#include "PlotDesc.hh"
#include "Descriptor.hh"
#include "Table.hh"


/** Describes a link element in the plot descriptor list which is
    maintained by the plot set. This object is for internal use only.
   
    @brief Link element of a plot descriptor list
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class PlotListLink {
      /// PlotMap is a friend
      friend class PlotMap;
   
   private:
      /// disable copy constructor
      PlotListLink (const PlotListLink&); 
      /// Disable assignment oparator
      PlotListLink& operator= (const PlotListLink&);
   
   protected:
      /// Name of plot type link element
      std::string		fName;
      /// Plot descriptor
      PlotDescriptor*	fPlot;
      /// Pointer to next sibling
      PlotListLink*	fSibling;
      /// Pointer to first child;
      PlotListLink*	fChild;
   
   public:
      explicit PlotListLink (const char* name, PlotDescriptor* plot) : 
      fName (name), fPlot (plot), fSibling (0), fChild (0) {
      }
      ~PlotListLink () {
         if (fPlot) fPlot->fOwner = 0; delete fPlot; }
   
      const PlotDescriptor* GetPlot () const {
         return fPlot; }
      void SetPlot (PlotDescriptor* plot, bool deleteIt = true) {
         if (deleteIt) {
            if (fPlot) fPlot->fOwner = 0; delete fPlot; }
         fPlot = plot; }
      const char* GetName () const {
         return fName.c_str(); }
      const PlotListLink* Next () const {
         return fSibling; }
      const PlotListLink* Child () const {
         return fChild; }
   
      bool operator== (const char* s) const;
      bool operator== (const std::string& s) const {
         return (*this == s.c_str()); }
      bool operator< (const char* s) const;
   };



/** Describes a list of plot descriptors which is maintained by the 
    plot set. This object is for internal use only.
   
    @brief List of plot descriptors
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class PlotMap {
   protected :
      /// Pointer to first graph type
      PlotListLink		fRoot;
   
      PlotListLink* FindChild (const PlotListLink* first, 
                        const char* name) const;
      PlotListLink* FindSibling (const PlotListLink* first, 
                        const char* name) const;
      PlotListLink* AddChild (PlotListLink* first, 
                        const char* name, PlotDescriptor* plot);
      PlotListLink* AddSibling (PlotListLink* first, 
                        const char* name, PlotDescriptor* plot);
      void RemoveAllChilds (PlotListLink* first);
      int GetChildID (const PlotListLink* first, 
                     const char* name) const;
   
   public:
      PlotMap () : fRoot ("root", 0) {
      }
      ~PlotMap () {
         RemoveAllChilds (&fRoot); }
      /// Copy constructor
      PlotMap (PlotMap& map) : fRoot ("root", 0) {
         fRoot.fChild = map.fRoot.fChild; map.fRoot.fChild = 0; }
      /// Assignment oparator
      PlotMap& operator= (PlotMap& map) {
         if (&map != this) {
            Clear(); fRoot.fChild = map.fRoot.fChild; map.fRoot.fChild = 0; }
         return *this; }
   
      bool Add (PlotDescriptor* plot);
      bool Remove (const PlotDescriptor* plot, bool deleteIt = true);
      bool Remove (const char* graph, const char* Achn, 
                  const char* Bchn = 0, bool deleteIt = true);
      bool Empty () const;
      void Clear ();
   
      const PlotListLink* Get () const {
         return &fRoot; }
      const PlotListLink* Get (const char* graph) const {
         return FindChild (Get(), graph); }
      const PlotListLink* Get (const char* graph, 
                        const char* Achn) const {
         return FindChild (Get (graph), Achn); }
      const PlotListLink* Get (const char* graph, 
                        const char* Achn, const char* Bchn) const {
         return FindChild (Get (graph, Achn), Bchn); }
   
      int GetId (const char* graph) const {
         return GetChildID (Get(), graph); }
      int GetId (const char* graph, 
                const char* Achn) const {
         return GetChildID (Get (graph), Achn); }
      int GetId (const char* graph, 
                const char* Achn, const char* Bchn) const {
         return GetChildID (Get (graph, Achn), Bchn); }
   };



/** Describes a pool(set) of plot descriptors. A pool also maintains
    a list of plot pads currently using data from the pool and a list
    of main windows currently displaying a plot from the pool. 
    Typically, a user will create his/her own plot pool or use the 
    global instance (defined in TLGPlot). User data object which were
    made 'plot aware' by inheriting from AttDataDescriptor can be
    added to the pool directly, as well as data descriptors with an
    associated graph type and channel name(s). Using a plot pool
    has the advantage that multiple plots can be provided to the
    user through a single pad. The user may choose to display 
    any plot from the pool as he/she wishes.

    When the data of a user data object or data descriptor is changed,
    the user has to call the Update routines of either the changed
    objects or the one of the pool to display the new data. 

    When a plot pool is deleted all asscoiated plot windows are
    destroyed as well.

    Unless the data descriptor carries a copy of the data the 
    original data object (or arrays) must stay valid until  
    either the plot set is deleted or the associated plot decriptor
    removed from the pool.

    Plot descriptors are stored by their graph type and channel
    names. Hence, the combination of graph type, A channel name 
    and optionally B channel name must be unique.

    User code typically only uses the Add, Remove and Update
    methods.
   
    @brief Plot set or pool
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class PlotSet {
   public:
      /// Plot pad list
      typedef std::list <VirtualPlotPad*> padlist;
      /// Plot pad list
      typedef std::list <VirtualPlotWindow*> winlist;
   
   private:
      /// Disable copy constructor
      PlotSet (const PlotSet&);
      /// Disable assignment operator
      PlotSet& operator= (const PlotSet&);
   
   protected:
      /** Basic iterator for plot pool. Iterators become invalid if 
          the plot gets changed.
          @brief Plot pool basic iterator.
       ******************************************************************/
      class basic_iterator {
         /// PlotSet is a friend
         friend class PlotSet;
      protected:
         // Pointer to current graph type
         const PlotListLink*	fG;
         // Pointer to current A channel type
         const PlotListLink*	fA;
         // Pointer to current B channel type
         const PlotListLink*	fB;
         // Pointer to plot descriptor
         PlotDescriptor* 	fP;
      
      public:
      /** Constructs a plot pool iterator.
          @brief Default constructor.
          @return void
       ******************************************************************/
         explicit basic_iterator (const PlotListLink* g = 0) 
         : fG(g), fA(0), fB(0), fP(0) {
         }
      /** Compares two plot pool iterators.
          @brief Equal operator.
          @param iter Iterator to compare
          @return true if equal
       ******************************************************************/
         bool operator== (const basic_iterator& iter) const {
            return (fP == iter.fP); }
      /** Compares two plot pool iterators.
          @brief Unequal operator.
          @param iter Iterator to compare
          @return true if equal
       ******************************************************************/
         bool operator!= (const basic_iterator& iter) const {
            return (fP != iter.fP); }
      /** Increment a pool iterators.
          @brief Increment operator.
          @return original iterator
       ******************************************************************/
         basic_iterator& operator++();
      /** Increment a pool iterators.
          @brief Increment operator.
          @return incremented iterator
       ******************************************************************/
         basic_iterator operator++(int);
      };
      /// List tree of plot descriptors
      PlotMap		fPlotList;
      /** List of registered main windows (all registered windows
          are automatically closed when the plot set is deleted. */
      winlist		fMainWindows;
      /** List of registered graphics pads (all registered pads
          are automatically updated when update is called. */
      padlist		fPads;
   
   public:
      /** Plot filter object. Its function operator is used to apply the
          filter function.
          @brief Plot filter object.
       ******************************************************************/
      class PlotFilter {
      public:
      /** Constructs a plot filter.
          @brief Default constructor.
          @return void
       ******************************************************************/
         PlotFilter() {}
         virtual ~PlotFilter() {}
      /** Evaluates the filter function. The filter also passes
          graphics type and channel names; they can be changed by 
          the filter function to resolve naming conflicts between
          the two plot sets.
          @brief Evaluate filter.
          @param pd Plot descriptor under test
          @param g graph typeto be used (by default the same as pd)
          @param a A channel name to be used (by default the same as pd)
          @param b B channel name to be used (by default the same as pd)
          @return true if plot descriptor passed the filter
       ******************************************************************/
         virtual bool operator() (const PlotDescriptor& pd, std::string& g,
                           std::string& a, std::string& b) const {
            return true; }
      };
   
      /** Returns a reference to filter with passes everything.
       ******************************************************************/
      static const PlotFilter& NoFilter();
   
      /** Iterator for plot pool. Iterators become invalid if the plot
          gets changed.
          @brief Plot pool iterator.
       ******************************************************************/
      class iterator : public basic_iterator {
      public:
      /** Constructs a plot pool iterator.
          @brief Default constructor.
          @return void
       ******************************************************************/
         explicit iterator (const PlotListLink* g = 0) 
         : basic_iterator (g) {
         }
      
      /** Increment a pool iterators.
          @brief Increment operator.
          @return original iterator
       ******************************************************************/
         iterator& operator++() {
            basic_iterator::operator++(); 
            return *this; }
      /** Increment a pool iterators.
          @brief Increment operator.
          @return incremented iterator
       ******************************************************************/
         iterator operator++(int) {
            iterator i = *this; 
            basic_iterator::operator++(); 
            return i; }
      /** Dereference a pool iterators.
          @brief Dereference operator.
          @return incremented iterator
       ******************************************************************/
         PlotDescriptor& operator*() {
            return *fP; }
      /** Dereference a pool iterators.
          @brief Dereference operator.
          @return incremented iterator
       ******************************************************************/
         PlotDescriptor* operator->() {
            return fP; }
      };
   
      /** Const iterator for plot pool. Iterators become invalid if the 
          plot gets changed.
          @brief Plot pool const_iterator.
       ******************************************************************/
      class const_iterator : public basic_iterator {
      public:
      /** Constructs a plot pool iterator.
          @brief Default constructor.
          @return void
       ******************************************************************/
         explicit const_iterator (const PlotListLink* g = 0) 
         : basic_iterator (g) {
         }
      /** Increment a pool iterators.
          @brief Increment operator.
          @return original iterator
       ******************************************************************/
         const_iterator& operator++() {
            basic_iterator::operator++(); 
            return *this; }
      /** Increment a pool iterators.
          @brief Increment operator.
          @return incremented iterator
       ******************************************************************/
         const_iterator operator++(int) {
            const_iterator i = *this; 
            basic_iterator::operator++(); 
            return i; }
      /** Dereference a pool iterators.
          @brief Dereference operator.
          @return incremented iterator
       ******************************************************************/
         const PlotDescriptor& operator*() const {
            return *fP; }
      /** Dereference a pool iterators.
          @brief Dereference operator.
          @return incremented iterator
       ******************************************************************/
         const PlotDescriptor* operator->() const {
            return fP; }
      };
   
      /** Constructs a plot pool.
          @brief Default constructor.
          @return void
       ******************************************************************/
      PlotSet ();
      /** Destructs the plot pool. All associated graphics windows
          are destroyed as well.
          @brief Destructor.
          @return void
       ******************************************************************/
      virtual ~PlotSet ();
   
      /** Merges the specified plot set with the current one,
          transferring only plot descriptors approved by the filter
          function. By default all plot descriptors pass the filter.
          @brief Merge plot set method.
          @param pl Plot set to merge
          @param filter Plot descriptor filter
          @return void
       ******************************************************************/
      virtual void Merge (PlotSet& pl, 
                        const PlotFilter& filter = NoFilter());
      /** Registers a main window. This method is used by TLGPadMain.
          @brief Register window method.
          @param win Window handle
          @return void
       ******************************************************************/
      virtual void RegisterWindow (VirtualPlotWindow* win);
      /** Unregisters a main window. This method is used by TLGPadMain.
          @brief Unregister window method.
          @param win Window handle
          @return void
       ******************************************************************/
      virtual void UnregisterWindow (VirtualPlotWindow* win);
      /** Returns the list of registered windows.
          @brief Get window list method.
          @return Window list
       ******************************************************************/
      virtual const winlist* GetRegisteredWindows() {
         return &fMainWindows; }
      /** Registers a plot pad. This method is used by TLGPad.
          @brief Register pad method.
          @param pad Pad handle
          @return void
       ******************************************************************/
      virtual void RegisterPad (VirtualPlotPad* pad);
      /** Unregisters a plot pad. This method is used by TLGPad.
          @brief Unregister pad method.
          @param pad Pad handle
          @return void
       ******************************************************************/
      virtual void UnregisterPad (VirtualPlotPad* pad);
   
      /** Returns the list of plot descriptors.
          @brief Get plot list method.
          @return void
       ******************************************************************/
      const PlotMap& GetPlotMap() const {
         return fPlotList; }
      /** Returns the begin iterator.
          @brief Begin method.
          @return iterator
       ******************************************************************/
      virtual iterator begin() {
         return ++iterator (fPlotList.Get()->Child()); }
      /** Returns the begin const_iterator.
          @brief Begin method.
          @return iterator
       ******************************************************************/
      virtual const_iterator begin() const {
         return ++const_iterator (fPlotList.Get()->Child()); }
      /** Returns the end iterator.
          @brief Begin method.
          @return iterator
       ******************************************************************/
      virtual iterator end() {
         return iterator(); }
      /** Returns the end const_iterator.
          @brief Begin method.
          @return iterator
       ******************************************************************/
      virtual const_iterator end() const {
         return const_iterator(); }
   
      /** Finds a plot descriptor.
          @brief Get plot descriptor method.
          @param graphtype Graph type
          @param Achn A channel
          @param Bchn B channel
          @return Plot descriptor
       ******************************************************************/
      virtual const PlotDescriptor* Get (const char* graphtype, 
                        const char* Achn, const char* Bchn = 0) const;
   
      /** Adds a plot descriptors to the pool. The plot descriptor
          is adopted by the pool.
          @brief Add plot descriptor method.
          @param plotd Plot descriptor to be added
          @return Plot descriptor, or 0 if failed
       ******************************************************************/
      virtual PlotDescriptor* Add (PlotDescriptor* plotd);
   
      /** Adds a data descriptors to the pool. The data descriptor
          is adopted by the pool. The caller must specify graph type
          and A channel name. If necessary a B channel can be specified
          as well.
          @brief Add data descriptor method.
          @param desc Data descriptor to be added
          @param graphtype Graph type
          @param Achn A channel name
          @param Bchn B channel name (optional)
          @param prmd Set of parameters (optional)
          @param cald Set of calibration records (optional)
          @return Plot descriptor, or 0 if failed
       ******************************************************************/
      virtual PlotDescriptor* Add (BasicDataDescriptor* desc,
                        const char* graphtype, 
                        const char* Achn, const char* Bchn = 0,
                        const ParameterDescriptor* prmd = 0,
                        const calibration::Descriptor* cald = 0);
   
      /** Adds a user data object to the pool. The user data object 
          must be derived from AttDataDescriptor to make it work.
          The plot will use the specified graph type and channel names,
          if they are supplied. If not it will query the user data 
          object. The routine will fail if neither of them is specified.
          @brief Add user data method.
          @param data User data object to be added
          @param graphtype Graph type (optional)
          @param Achn A channel name (optional)
          @param Bchn B channel name (optional)
          @return Plot descriptor, or 0 if failed
       ******************************************************************/
      virtual PlotDescriptor* Add (const AttDataDescriptor& data, 
                        const char* graphtype = 0, 
                        const char* Achn = 0, const char* Bchn = 0);
   
      /** Removes a plot descriptors from the pool. The plot descriptor
          is deleted if deleteIt is true.
          @brief Remove plot descriptor method.
          @param plotd Plot descriptor to be removed
    	  @param deleteIt automatically deletes the plot 
                 descriptor if true (default)
          @return void
       ******************************************************************/
      virtual void Remove (const PlotDescriptor* plotd, 
                        bool deleteIt = true);
   
      /** Removes all plot descriptors from the pool. The plot 
          descriptors are deleted. If all is set to false,  
          plot descriptors with the persistent flag set are not
          deleted.
          @brief Clear plot descriptor method.
          @return void
       ******************************************************************/
      virtual void Clear (bool all = true);
   
      /** True if plot set is empty.
          @brief Is empty method.
          @return True if empty
       ******************************************************************/
      virtual bool Empty () const;
   
      /** Updates all plot pads which are displaying data from the
          pool. If a plot descriptor is specfied only pads which 
          are currently using this descriptor will be updated, otherwise
          all pads which have registered are updated. The option panels
          are not updated.
          @brief Update method.
          @param plotd Plot descriptor whose data has changed
          @return void
       ******************************************************************/
      virtual void Update (const PlotDescriptor* plotd) const;
   
      /** Updates all plot pads which are displaying data from the
          pool. The option pannels will be updated as well.
          @brief Update method.
          @return void
       ******************************************************************/
      virtual void Update () const;

      /**
       * Set whether to show "Disconnected from Data" message
       * @param show
       */
      virtual void ShowDisconnect(bool show);
   
   private:
      /// Filter with no action (pass all)
      static const PlotFilter kNoFilter;
   };


#endif // _LIGO_PLOTSET_H

