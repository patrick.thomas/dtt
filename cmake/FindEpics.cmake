# - Try to find Epics and Ezca
# Once done this will define
#  EPICS_FOUND - both epics and ezca found
#  EPICS_INCLUDE_DIRS - The readline include directories
#  EPICS_LIBRARIES - The libraries needed to use readline

FILE(GLOB lib64_epics /usr/lib64/epics*)
FILE(GLOB lib_epics /usr/lib/epics*)

SET(EPICS_FOUND TRUE)

set(header_search
        HINTS
        ${lib_epics}/include
        ${lib64_epics}/include
        ${lib_epics}/extensions/include
        ${lib64_epics}/extensions/include
        PATH_SUFFIXES
        /os/Linux
        /compiler/gcc
        )

find_path ( EPICS_BASE_INCLUDE_DIR cadef.h
        ${header_search}
        )

if (${EPICS_BASE_INCLUDE_DIR} MATCHES NOTFOUND$)
    SET(EPICS_FOUND FALSE)
endif()

find_path ( EZCA_INCLUDE_DIR ezca.h
        ${header_search}
        )

if (${EZCA_INCLUDE_DIR} MATCHES NOTFOUND$)
    SET(EPICS_FOUND FALSE)
endif()

find_path ( EPICSOS_INCLUDE_DIR osdEvent.h
        ${header_search}
        )

if (${EPICSOS_INCLUDE_DIR} MATCHES NOTFOUND$)
    SET(EPICS_FOUND FALSE)
endif()

find_path ( EPICS_COMP_INCLUDE_DIR compilerSpecific.h
        ${header_search}
        )

if (${EPICS_COMP_INCLUDE_DIR} MATCHES NOTFOUND$)
    SET(EPICS_FOUND FALSE)
endif()

SET (EPICS_INCLUDE_DIRS
        ${EPICS_BASE_INCLUDE_DIR}
        ${EZCA_INCLUDE_DIR}
        ${EPICSOS_INCLUDE_DIR}
        ${EPICS_COMP_INCLUDE_DIR}
        )

find_library(ezca_lib ezca
        HINTS
        ${lib_epics}/lib
        ${lib64_epics}/lib
        ${lib_epics}/extensions/lib
        ${lib64_epics}/extensions/lib
        PATH_SUFFIXES
        /linux-x86_64
        /x86_64-linux-gnu
        )

if (${ezca_lib} MATCHES NOTFOUND$)
    SET(EPICS_FOUND FALSE)
endif()

find_library(ca_lib ca
        HINTS
        ${lib_epics}/lib
        ${lib64_epics}/lib
        ${lib_epics}/extensions/lib
        ${lib64_epics}/extensions/lib
        PATH_SUFFIXES
        /linux-x86_64
        /x86_64-linux-gnu
        )

if (${ca_lib} MATCHES NOTFOUND$)
    SET(EPICS_FOUND FALSE)
endif()

find_library(Com_lib Com
        HINTS
        ${lib_epics}/lib
        ${lib64_epics}/lib
        ${lib_epics}/extensions/lib
        ${lib64_epics}/extensions/lib
        PATH_SUFFIXES
        /linux-x86_64
        /x86_64-linux-gnu
        )

if (${Com_lib} MATCHES NOTFOUND$)
    SET(EPICS_FOUND FALSE)
endif()

SET (EPICS_LIBRARIES ${ezca_lib} ${ca_lib} ${Com_lib})

#find_library ( READLINE_LIBRARY NAMES readline
#        HINTS ${PC_READLINE_LIBDIR} ${PC_READLINE_LIBRARY_DIRS} )

#set ( READLINE_LIBRARIES ${READLINE_LIBRARY} )
#set ( READLINE_INCLUDE_DIRS ${READLINE_INCLUDE_DIR} )

#include ( FindPackageHandleStandardArgs )
# handle the QUIETLY and REQUIRED arguments and set READLINE_FOUND to TRUE
# if all listed variables are TRUE
#find_package_handle_standard_args ( readline DEFAULT_MSG READLINE_LIBRARIES READLINE_INCLUDE_DIRS )

