#!/bin/bash

set -e

version=$1

if [[ -z $version ]]
then
  echo "ERROR: a tagged dtt version must be passed on the command line"
  exit
fi

deb_names="buster bullseye bookworm"
declare -A deb_versions
deb_versions=( ["buster"]=10 ["bullseye"]=11 ["bookworm"]=12 )

src_dir=$(pwd)
echo "Source directory is ${src_dr}"

# get version tag
version_tag=$(echo $version | sed 's/~/_/g')
echo "Releasing version ${version} from git tag '${version_tag}'"

# check if tag exists
git_tags=$(git tag)
found=false
for check_tag in $git_tags
do
  if [[ $check_tag == $version_tag ]]
  then
    found=true
    break
  fi
done
if ! $found
then
  echo "ERROR: Version tag not found.  Be sure to tag the version before releasing!"
  exit
fi

# check if tag is ancestor of master
if ! git merge-base --is-ancestor ${version_tag} master
then
  echo "ERROR: version tag must be an ancestor of the master branch"
  exit
fi

# check if tag already released
target_deb_tag="debian/${version_tag}-"
echo $target_deb_tag
for check_tag in $git_tags
do
  if [[ "${check_tag}" == *"${target_deb_tag}"* ]]
  then
    echo "ERROR: version already released in tag ${check_tag}"
    exit
  fi
done

# check out the tag
if ! git checkout ${version_tag}
then
  echo "Couldn't checkout tag '${version_tag}'"
  exit
fi

# make sure checkout is clean
num_changes=$(git diff-index HEAD | wc -l)
if [[ $num_changes -gt 0 ]]
then
  echo "ERROR: Source directory is not clean."
  exit
fi

# Check actual version number in source to see if it's correct.  Don't release packages with wrong version number.
tmp_dir=/tmp/__dtt_version_test
cmake_out=cmake_out.txt
if ! mkdir $tmp_dir
then
  echo "Error: Couldn't create temporary directory ${tmp_dir}.  Maybe it needs to be deleted."
  exit
fi
cd $tmp_dir
cmake $src_dir > ${cmake_out}

src_version=$(grep version: ${cmake_out} | cut -d ' ' -f 3)
if [[ $src_version != $version ]]
then
  echo "Error: Requested version was ${version} but source version at tag ${version_tag} was ${src_version}"
  cd $src_dir
  rm -rf $tmp_dir || echo "Failed to delete temporary directory ${tmp_dir}."
  exit
fi

cd $src_dir
rm -rf $tmp_dir || echo "Failed to delete temporary directory ${tmp_dir}."


# start the release
first_run=true
tmp_chng_entry='/tmp/_dtt_deb_changelog_entry.txt'
deb_chglog='debian/changelog'
tmp_chng_log='/tmp/_dtt_deb_changelog.txt'

for deb_name in $deb_names
do
  deb_ver=${deb_versions[$deb_name]}
  git switch "debian/${deb_name}"
  git pull
  gbp import-ref master -u $version --upstream-tag='%(version)s' --debian-branch=debian/$deb_name
  if $first_run
  then
    # manually edit first changelog
    old_line_count=$(cat ${deb_chglog} | wc -l)
    gbp dch -Rc -N ${version}-1+deb$deb_ver --debian-branch debian/$deb_name
    new_line_count=$(cat ${deb_chglog} | wc -l)
    new_entry_count=$(($new_line_count-$old_line_count))
    head -n $new_entry_count $deb_chglog > $tmp_chng_entry
    first_ver=$deb_ver
    first_run=false
  else
    # copy entry to subsequent changelogs
    # make sure to change deb number in first line
    head -n 1 $tmp_chng_entry | sed "s/deb${first_ver}/deb${deb_ver}/" > $tmp_chng_log
    tail_lc=$(($new_entry_count-1))
    tail -n $tail_lc $tmp_chng_entry >> $tmp_chng_log
    cat $deb_chglog >> $tmp_chng_log
    cp $tmp_chng_log $deb_chglog
    git add $deb_chglog
    git commit -m "updated change log from auto release script"
  fi
  git clean -f
  gbp tag --debian-branch=debian/$deb_name
  git push
done
git push --tags
git switch master
